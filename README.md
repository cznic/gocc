# gocc

Command gocc is an experimental C99 compiler targeting linux/amd64. Work in
progress. ATM its purpose is only to help verify the C99 frontend[[0]]
correctness.

Installation

    $ go get -u modernc.org/gocc

Documentation: [godoc.org/modernc.org/gocc](http://godoc.org/modernc.org/gocc)

  [0]: https://godoc.org/modernc.org/cc/v3
