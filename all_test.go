// Copyright 2019 The GOCC Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

//TODO gcc 4.7

package main // import "modernc.org/gocc"

import (
	"archive/tar"
	"archive/zip"
	"bufio"
	"bytes"
	"compress/bzip2"
	"compress/gzip"
	"context"
	"encoding/hex"
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"os/exec"
	"path"
	"path/filepath"
	"regexp"
	"runtime"
	"runtime/debug"
	"sort"
	"strings"
	"testing"
	"time"

	"github.com/dustin/go-humanize"
	"github.com/google/go-cmp/cmp"
	"modernc.org/cc/v3"
)

func caller(s string, va ...interface{}) {
	if s == "" {
		s = strings.Repeat("%v ", len(va))
	}
	_, fn, fl, _ := runtime.Caller(2)
	fmt.Fprintf(os.Stderr, "# caller: %s:%d: ", path.Base(fn), fl)
	fmt.Fprintf(os.Stderr, s, va...)
	fmt.Fprintln(os.Stderr)
	_, fn, fl, _ = runtime.Caller(1)
	fmt.Fprintf(os.Stderr, "# \tcallee: %s:%d: ", path.Base(fn), fl)
	fmt.Fprintln(os.Stderr)
	os.Stderr.Sync()
}

func dbg(s string, va ...interface{}) {
	if s == "" {
		s = strings.Repeat("%v ", len(va))
	}
	_, fn, fl, _ := runtime.Caller(1)
	fmt.Fprintf(os.Stderr, "# dbg %s:%d: ", path.Base(fn), fl)
	fmt.Fprintf(os.Stderr, s, va...)
	fmt.Fprintln(os.Stderr)
	os.Stderr.Sync()
}

func TODO(...interface{}) string { //TODOOK
	_, fn, fl, _ := runtime.Caller(1)
	return fmt.Sprintf("# TODO: %s:%d:\n", path.Base(fn), fl) //TODOOK
}

func stack() string { return string(debug.Stack()) }

func use(...interface{}) {}

func init() {
	use(caller, dbg, TODO, stack, dumpLayout) //TODOOK
}

// ----------------------------------------------------------------------------

var (
	oBlackBox   = flag.String("blackbox", "", "Record CSmith file to this file")
	oCSmith     = flag.Duration("csmith", time.Minute, "")
	oCertN      = flag.Int("certN", 5, "")
	oDev        = flag.Bool("dev", false, "Enable developer tests/downloads.")
	oDownload   = flag.Bool("download", false, "Download missing testdata. Add -dev to download also 100+ MB of developer resources.")
	oEdit       = flag.Bool("edit", false, "")
	oFF         = flag.Bool("ff", false, "Fail fast")
	oM32        = flag.Bool("m32", false, "")
	oRE         = flag.String("re", "", "")
	oStackTrace = flag.Bool("trcstack", false, "")
	oTrace      = flag.Bool("trc", false, "Print tested paths.")
	oTraceO     = flag.Bool("trco", false, "Print test output")

	csmithDefaultArgs = strings.Join([]string{
		"--bitfields",                     // --bitfields | --no-bitfields: enable | disable full-bitfields structs (disabled by default).
		"--max-nested-struct-level", "10", // --max-nested-struct-level <num>: limit maximum nested level of structs to <num>(default 0). Only works in the exhaustive mode.
		"--no-const-pointers", // --const-pointers | --no-const-pointers: enable | disable const pointers (enabled by default).
		"--no-consts",         // --consts | --no-consts: enable | disable const qualifier (enabled by default).
		"--no-packed-struct",  // --packed-struct | --no-packed-struct: enable | disable packed structs by adding #pragma pack(1) before struct definition (disabled by default).
		// "--no-safe-math",         // --safe-math | --no-safe-math: Emit safe math wrapper functions (enabled by default).
		"--no-volatile-pointers", // --volatile-pointers | --no-volatile-pointers: enable | disable volatile pointers (enabled by default).
		"--no-volatiles",         // --volatiles | --no-volatiles: enable | disable volatiles (enabled by default).
		"--paranoid",             // --paranoid | --no-paranoid: enable | disable pointer-related assertions (disabled by default).
	}, " ")

	downloads = []struct {
		dir, url string
		sz       int
		dev      bool
	}{
		{gccDir, "http://mirror.koddos.net/gcc/releases/gcc-9.1.0/gcc-9.1.0.tar.gz", 118000, true},
		{sqliteDir, "https://www.sqlite.org/2019/sqlite-amalgamation-3300100.zip", 2400, false},
		{tccDir, "http://download.savannah.gnu.org/releases/tinycc/tcc-0.9.27.tar.bz2", 620, false},
	}

	gccDir    = filepath.FromSlash("testdata/gcc-9.1.0")
	sqliteDir = filepath.FromSlash("testdata/sqlite-amalgamation-3300100")
	tccDir    = filepath.FromSlash("testdata/tcc-0.9.27")
	testWD    string

	gccTestDecls = cc.Source{Name: "<gcc-test-builtin>", Value: `
char *strchr(const char *s, int c);
char *strcpy(char *dest, const char *src);
double copysign(double x, double y);
double fabs(double x);
float copysignf(float x, float y);
int abs(int j);
int ffs(int i);
int isprint(int c);
int memcmp(const void *s1, const void *s2, size_t n);
int printf(const char *format, ...);
int snprintf(char *str, size_t size, const char *format, ...);
int sprintf(char *str, const char *format, ...);
int strcmp(const char *s1, const char *s2);
size_t strlen(const char *s);
void *malloc(size_t size);
void *memcpy(void *dest, const void *src, size_t n);
void *memset(void *s, int c, size_t n);
void abort(void);
void exit(int status);
void free(void *ptr);
	`}
)

func TestMain(m *testing.M) {
	isTesting = true

	defer func() {
		os.Exit(m.Run())
	}()

	flag.BoolVar(&oQuiet, "q", false, "")
	flag.BoolVar(&oTODO, "todo", false, "")
	flag.BoolVar(&traceIL, "il", false, "print generated IL")

	flag.Parse()
	var err error
	if testWD, err = os.Getwd(); err != nil {
		panic("Cannot determine working dir: " + err.Error())
	}

	if *oDownload {
		download()
	}
}

func download() {
	tmp, err := ioutil.TempDir("", "")
	if err != nil {
		fmt.Fprintf(os.Stderr, "%s\n", err)
		return
	}

	defer os.RemoveAll(tmp)

	for _, v := range downloads {
		dir := filepath.FromSlash(v.dir)
		root := filepath.Dir(v.dir)
		fi, err := os.Stat(dir)
		switch {
		case err == nil:
			if !fi.IsDir() {
				fmt.Fprintf(os.Stderr, "expected %s to be a directory\n", dir)
			}
			continue
		default:
			if !os.IsNotExist(err) {
				fmt.Fprintf(os.Stderr, "%s", err)
				continue
			}

			if v.dev && !*oDev {
				fmt.Printf("Not downloading (no -dev) %v MB from %s\n", float64(v.sz)/1000, v.url)
				continue
			}

		}

		if err := func() error {
			fmt.Printf("Downloading %v MB from %s\n", float64(v.sz)/1000, v.url)
			resp, err := http.Get(v.url)
			if err != nil {
				return err
			}

			defer resp.Body.Close()

			base := filepath.Base(v.url)
			name := filepath.Join(tmp, base)
			f, err := os.Create(name)
			if err != nil {
				return err
			}

			defer os.Remove(name)

			n, err := io.Copy(f, resp.Body)
			if err != nil {
				return err
			}

			if _, err := f.Seek(0, io.SeekStart); err != nil {
				return err
			}

			switch {
			case strings.HasSuffix(base, ".tar.bz2"):
				b2r := bzip2.NewReader(bufio.NewReader(f))
				tr := tar.NewReader(b2r)
				for {
					hdr, err := tr.Next()
					if err != nil {
						if err != io.EOF {
							return err
						}

						return nil
					}

					switch hdr.Typeflag {
					case tar.TypeDir:
						if err = os.MkdirAll(filepath.Join(root, hdr.Name), 0770); err != nil {
							return err
						}
					case tar.TypeReg, tar.TypeRegA:
						f, err := os.OpenFile(filepath.Join(root, hdr.Name), os.O_CREATE|os.O_WRONLY, os.FileMode(hdr.Mode))
						if err != nil {
							return err
						}

						w := bufio.NewWriter(f)
						if _, err = io.Copy(w, tr); err != nil {
							return err
						}

						if err := w.Flush(); err != nil {
							return err
						}

						if err := f.Close(); err != nil {
							return err
						}
					default:
						return fmt.Errorf("unexpected tar header typeflag %#02x", hdr.Typeflag)
					}
				}
			case strings.HasSuffix(base, ".tar.gz"):
				return untar(root, bufio.NewReader(f))
			case strings.HasSuffix(base, ".zip"):
				r, err := zip.NewReader(f, n)
				if err != nil {
					return err
				}

				for _, f := range r.File {
					fi := f.FileInfo()
					if fi.IsDir() {
						if err := os.MkdirAll(filepath.Join(root, f.Name), 0770); err != nil {
							return err
						}

						continue
					}

					if err := func() error {
						rc, err := f.Open()
						if err != nil {
							return err
						}

						defer rc.Close()

						dname := filepath.Join(root, f.Name)
						g, err := os.Create(dname)
						if err != nil {
							return err
						}

						defer g.Close()

						n, err = io.Copy(g, rc)
						return err
					}(); err != nil {
						return err
					}
				}
				return nil
			}
			panic("internal error") //TODOOK
		}(); err != nil {
			fmt.Fprintln(os.Stderr, err)
		}
	}
}

func TestTCC(t *testing.T) {
	root := filepath.Join(testWD, filepath.FromSlash(tccDir))
	if _, err := os.Stat(root); err != nil {
		t.Fatalf("Missing resources in %s. Please run 'go test -download' to fix.", root)
	}

	g := newGolden(t, fmt.Sprintf("testdata/tcc_%s_%s.golden", runtime.GOOS, runtime.GOARCH))

	defer g.close()

	todos = map[todoPos]int{}
	var files, ok int
	if *oEdit {
		defer func() {
			fmt.Printf("TCC files %s, ok %s\n", h(files), h(ok))
		}()
	}
	const dir = "tests/tests2"
	t.Run("noOpt", func(t *testing.T) {
		f, o := testTCCExec(g.w, t, filepath.Join(root, filepath.FromSlash(dir)), false)
		files += f
		ok += o
	})
	t.Logf("files %s, ok %s", h(files), h(ok))
	if !oQuiet {
		dumpTODOs(t)
	}

	g2 := newGolden(t, fmt.Sprintf("testdata/tcc_%s_%s.opt.golden", runtime.GOOS, runtime.GOARCH))

	defer g2.close()
	todos = map[todoPos]int{}
	files = 0
	ok = 0
	t.Run("opt", func(t *testing.T) {
		f, o := testTCCExec(g2.w, t, filepath.Join(root, filepath.FromSlash(dir)), true)
		files += f
		ok += o
	})
	t.Logf("files %s, ok %s", h(files), h(ok))
	if !oQuiet {
		dumpTODOs(t)
	}
}

type golden struct {
	t *testing.T
	f *os.File
	w *bufio.Writer
}

func newGolden(t *testing.T, fn string) *golden {
	f, err := os.Create(filepath.FromSlash(fn))
	if err != nil {
		t.Fatal(err)
	}

	w := bufio.NewWriter(f)
	return &golden{t, f, w}
}

func (g *golden) close() {
	if err := g.w.Flush(); err != nil {
		g.t.Fatal(err)
	}

	if err := g.f.Close(); err != nil {
		g.t.Fatal(err)
	}
}

func fail(t *testing.T, s string, args ...interface{}) {
	if *oFF {
		t.Fatalf(s, args...)
	}

	if !oQuiet {
		t.Errorf(s, args...)
	}
}

func testTCCExec(w io.Writer, t *testing.T, dir string, optimize bool) (files, ok int) {
	const binaryName = "a.out"
	blacklist := map[string]struct{}{
		"34_array_assignment.c":    {}, // gcc: 16:6: error: assignment to expression with array type
		"60_errors_and_warnings.c": {}, // Not a standalone test. gcc fails.
		"93_integer_promotion.c":   {}, // The expected output does not agree with gcc.
		"95_bitfields.c":           {}, // Included from 95_bitfields_ms.c
		"95_bitfields_ms.c":        {}, // The expected output does not agree with gcc.
		"96_nodata_wanted.c":       {}, // Not a standalone test. gcc fails.
		"99_fastcall.c":            {}, // 386 only

		"73_arm64.c":                {}, //TODO struct varargs, not supported by QBE
		"75_array_in_struct_init.c": {}, //TODO flat struct initializer
		"80_flexarray.c":            {}, //TODO Flexible array member
		"85_asm-outside-function.c": {}, //TODO
		"90_struct-init.c":          {}, //TODO cc ... in designator
		"94_generic.c":              {}, //TODO cc _Generic
		"98_al_ax_extend.c":         {}, //TODO
	}
	wd, err := os.Getwd()
	if err != nil {
		t.Fatal(err)
	}

	defer os.Chdir(wd)

	temp, err := ioutil.TempDir("", "gocc-test-")
	if err != nil {
		t.Fatal(err)
	}

	defer os.RemoveAll(temp)

	if err := os.Chdir(temp); err != nil {
		t.Fatal(err)
	}

	var re *regexp.Regexp
	if s := *oRE; s != "" {
		re = regexp.MustCompile(s)
	}

	if err := filepath.Walk(dir, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			if os.IsNotExist(err) {
				err = nil
			}
			return err
		}

		if info.IsDir() {
			return skipDir(path)
		}

		if filepath.Ext(path) != ".c" || info.Mode()&os.ModeType != 0 {
			return nil
		}

		if _, ok := blacklist[filepath.Base(path)]; ok {
			return nil
		}

		files++

		if re != nil && !re.MatchString(path) {
			return nil
		}

		if *oTrace {
			fmt.Fprintln(os.Stderr, files, ok, path)
		}

		if err := os.Remove(binaryName); err != nil && !os.IsNotExist(err) {
			return err
		}

		goccArgs := []string{"gocc", "-o", binaryName, "-Dasm=__asm__", "-w"}
		var args []string
		switch base := filepath.Base(path); base {
		case
			"22_floating_point.c",
			"24_math_library.c":

			goccArgs = append(goccArgs, "-lm")
		case "31_args.c":
			args = []string{"arg1", "arg2", "arg3", "arg4", "arg5"}
		case "46_grep.c":
			if err := copyFile(path, filepath.Join(temp, base)); err != nil {
				return err
			}

			args = []string{`[^* ]*[:a:d: ]+\:\*-/: $`, base}
		}

		if !func() (r bool) {
			defer func() {
				if err := recover(); err != nil {
					if *oStackTrace {
						fmt.Printf("%s\n", stack())
					}
					if *oTrace {
						fmt.Println(err)
					}
					if !oQuiet {
						fail(t, "%s: %v", path, err)
					}
					r = false
				}
			}()

			goccArgs = append(goccArgs, path)
			switch base := filepath.Base(path); base {
			case
				"22_floating_point.c",
				"24_math_library.c":

				goccArgs = append(goccArgs, "-lm")
			}
			if _, err := execute(goccArgs, optimize); err != nil {
				if *oTrace {
					fmt.Println(err)
				}
				if !oQuiet {
					fail(t, "%s: %v", path, err)
				}
				return false
			}

			return true
		}() {
			return nil
		}

		out, err := exec.Command("./"+binaryName, args...).CombinedOutput()
		if err != nil {
			if *oTrace {
				fmt.Println(err)
			}
			t.Errorf("%v: %v", path, err)
			return nil
		}

		if *oTraceO {
			fmt.Printf("%s\n", out)
		}
		exp, err := ioutil.ReadFile(noExt(path) + ".expect")
		if err != nil {
			if os.IsNotExist(err) {
				fmt.Fprintln(w, filepath.Base(path))
				ok++
				return nil
			}

			return err
		}

		out = trim(out)
		exp = trim(exp)

		switch base := filepath.Base(path); base {
		case "70_floating_point_literals.c": //TODO TCC binary extension
			a := strings.Split(string(exp), "\n")
			exp = []byte(strings.Join(a[:35], "\n"))
		}

		if !bytes.Equal(out, exp) {
			if *oTrace {
				fmt.Println(err)
			}
			t.Errorf("%v: %s\nout\n%s\nexp\n%s", path, cmp.Diff(string(exp), string(out)), out, exp)
			return nil
		}

		fmt.Fprintln(w, filepath.Base(path))
		ok++
		return nil
	}); err != nil {
		fail(t, "%v", err)
	}
	return files, ok
}

func TestTCCGo(t *testing.T) {
	root := filepath.Join(testWD, filepath.FromSlash(tccDir))
	if _, err := os.Stat(root); err != nil {
		t.Fatalf("Missing resources in %s. Please run 'go test -download' to fix.", root)
	}

	g := newGolden(t, fmt.Sprintf("testdata/tcc_go_%s_%s.golden", runtime.GOOS, runtime.GOARCH))

	defer g.close()

	todos = map[todoPos]int{}
	var files, ok int
	if *oEdit {
		defer func() {
			fmt.Printf("TCC files %s, ok %s\n", h(files), h(ok))
		}()
	}
	const dir = "tests/tests2"
	f, o := testTCCGoExec(g.w, t, filepath.Join(root, filepath.FromSlash(dir)), false)
	files += f
	ok += o
	t.Logf("files %s, ok %s", h(files), h(ok))
	if !oQuiet {
		dumpTODOs(t)
	}
}

func testTCCGoExec(w io.Writer, t *testing.T, dir string, optimize bool) (files, ok int) {
	const main = "main.go"
	blacklist := map[string]struct{}{
		"34_array_assignment.c":    {}, // gcc: 16:6: error: assignment to expression with array type
		"60_errors_and_warnings.c": {}, // Not a standalone test. gcc fails.
		"93_integer_promotion.c":   {}, // The expected output does not agree with gcc.
		"95_bitfields.c":           {}, // Included from 95_bitfields_ms.c
		"95_bitfields_ms.c":        {}, // The expected output does not agree with gcc.
		"96_nodata_wanted.c":       {}, // Not a standalone test. gcc fails.
		"99_fastcall.c":            {}, // 386 only

		"40_stdio.c":                {}, //TODO
		"42_function_pointer.c":     {}, //TODO
		"46_grep.c":                 {}, //TODO
		"73_arm64.c":                {}, //TODO struct varargs, not supported by QBE
		"75_array_in_struct_init.c": {}, //TODO flat struct initializer
		"80_flexarray.c":            {}, //TODO Flexible array member
		"85_asm-outside-function.c": {}, //TODO
		"90_struct-init.c":          {}, //TODO cc ... in designator
		"94_generic.c":              {}, //TODO cc _Generic
		"98_al_ax_extend.c":         {}, //TODO
	}
	wd, err := os.Getwd()
	if err != nil {
		t.Fatal(err)
	}

	defer os.Chdir(wd)

	temp, err := ioutil.TempDir("", "gocc-test-")
	if err != nil {
		t.Fatal(err)
	}

	defer os.RemoveAll(temp)

	if err := os.Chdir(temp); err != nil {
		t.Fatal(err)
	}

	var re *regexp.Regexp
	if s := *oRE; s != "" {
		re = regexp.MustCompile(s)
	}

	if err := filepath.Walk(dir, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			if os.IsNotExist(err) {
				err = nil
			}
			return err
		}

		if info.IsDir() {
			return skipDir(path)
		}

		if filepath.Ext(path) != ".c" || info.Mode()&os.ModeType != 0 {
			return nil
		}

		if _, ok := blacklist[filepath.Base(path)]; ok {
			return nil
		}

		files++

		if re != nil && !re.MatchString(path) {
			return nil
		}

		if *oTrace {
			fmt.Fprintln(os.Stderr, files, ok, path)
		}

		if err := os.Remove(main); err != nil && !os.IsNotExist(err) {
			return err
		}

		goccArgs := []string{"gocc", "-o", main, "-Dasm=__asm__"}
		var args []string
		switch base := filepath.Base(path); base {
		case
			"22_floating_point.c",
			"24_math_library.c":
			goccArgs = append(goccArgs, "-lm")
		case "31_args.c":
			args = []string{"arg1", "arg2", "arg3", "arg4", "arg5"}
		case "46_grep.c":
			if err := copyFile(path, filepath.Join(temp, base)); err != nil {
				return err
			}
			args = []string{`[^* ]*[:a:d: ]+\:\*-/: $`, base}
		}
		if !func() (r bool) {
			defer func() {
				if err := recover(); err != nil {
					if *oStackTrace {
						fmt.Printf("%s\n", stack())
					}
					if *oTrace {
						fmt.Println(err)
					}
					if !oQuiet {
						fail(t, "%s: %v", path, err)
					}
					r = false
				}
			}()

			goccArgs = append(goccArgs, path)
			if _, err := execute(goccArgs, optimize); err != nil {
				if *oTrace {
					fmt.Println(err)
				}
				if !oQuiet {
					fail(t, "%s: %v", path, err)
				}
				return false
			}

			return true
		}() {
			return nil
		}

		out, err := exec.Command("go", append([]string{"run", main}, args...)...).CombinedOutput()
		if err != nil {
			if *oTrace {
				fmt.Println(err)
			}
			t.Errorf("%v: %s\n%v", path, out, err)
			return nil
		}

		if *oTraceO {
			fmt.Printf("%s\n", out)
		}
		exp, err := ioutil.ReadFile(noExt(path) + ".expect")
		if err != nil {
			if os.IsNotExist(err) {
				fmt.Fprintln(w, filepath.Base(path))
				ok++
				return nil
			}

			return err
		}

		out = trim(out)
		exp = trim(exp)

		switch base := filepath.Base(path); base {
		case "70_floating_point_literals.c": //TODO TCC binary extension
			a := strings.Split(string(exp), "\n")
			exp = []byte(strings.Join(a[:35], "\n"))
		}

		if !bytes.Equal(out, exp) {
			if *oTrace {
				fmt.Println(err)
			}
			t.Errorf("%v: %s\nout\n%s\nexp\n%s", path, cmp.Diff(string(exp), string(out)), out, exp)
			return nil
		}

		fmt.Fprintln(w, filepath.Base(path))
		ok++
		return nil
	}); err != nil {
		fail(t, "%v", err)
	}
	return files, ok
}

func noExt(s string) string {
	ext := filepath.Ext(s)
	if ext == "" {
		panic("internal error") //TODOOK
	}
	return s[:len(s)-len(ext)]
}

func copyFile(src, dst string) error {
	b, err := ioutil.ReadFile(src)
	if err != nil {
		return err
	}

	return ioutil.WriteFile(dst, b, 0660)
}

func trim(b []byte) (r []byte) {
	b = bytes.TrimLeft(b, "\n")
	b = bytes.TrimRight(b, "\n")
	a := bytes.Split(b, []byte("\n"))
	for i, v := range a {
		a[i] = bytes.TrimSpace(v)
	}
	return bytes.Join(a, []byte("\n"))
}

func execute(args []string, optimize bool, more ...cc.Source) ([]byte, error) {
	args = append(args, "-w")
	if *oM32 {
		args = append(args, "-m32")
	}
	if optimize {
		args = append(args, "-O")
	}
	t, err := newTask(args)
	if err != nil {
		return nil, err
	}

	t.testSources = more
	t.doNotCache = true
	var stdout, stderr bytes.Buffer
	rc := t.main(&stdout, &stderr)
	if stdout.Len() != 0 {
		fmt.Printf("%s", stdout.Bytes())
	}
	if stderr.Len() != 0 {
		return nil, fmt.Errorf("%s", stderr.Bytes())
	}

	if rc != 0 {
		return nil, fmt.Errorf("internal error")
	}

	return stdout.Bytes(), nil
}

func skipDir(path string) error {
	if strings.HasPrefix(filepath.Base(path), ".") {
		return filepath.SkipDir
	}

	return nil
}

func h(v interface{}) string {
	switch x := v.(type) {
	case int:
		return humanize.Comma(int64(x))
	case int64:
		return humanize.Comma(x)
	case uint64:
		return humanize.Comma(int64(x))
	case float64:
		return humanize.CommafWithDigits(x, 0)
	default:
		panic(fmt.Errorf("%T", x)) //TODOOK
	}
}

func TestMiniGMP(t *testing.T) {
	g := newGolden(t, fmt.Sprintf("testdata/mini-gmp_%s_%s.golden", runtime.GOOS, runtime.GOARCH))

	defer g.close()

	var files, ok int
	t.Run("noOpt", func(t *testing.T) {
		files, ok = testMiniGMP(g.w, t, false, nil)
	})
	t.Logf("files %s, ok %s", h(files), h(ok))

	g2 := newGolden(t, fmt.Sprintf("testdata/mini-gmp_%s_%s.opt.golden", runtime.GOOS, runtime.GOARCH))

	defer g2.close()

	var blacklist map[string]struct{}
	if arch == 32 || *oM32 {
		blacklist = map[string]struct{}{
			"t-double": {}, // gcc 7.4 -O bug
			"t-cmp_d":  {}, // gcc 7.4 -O bug
		}
	}
	t.Run("opt", func(t *testing.T) {
		files, ok = testMiniGMP(g2.w, t, true, blacklist)
	})
	t.Logf("files %s, ok %s", h(files), h(ok))
}

func testMiniGMP(w io.Writer, t *testing.T, opt bool, blacklist map[string]struct{}) (files, ok int) {
	var re *regexp.Regexp
	if s := *oRE; s != "" {
		re = regexp.MustCompile(s)
	}

	b, err := ioutil.ReadFile(filepath.FromSlash("testdata/mini-gmp.tar.gz"))
	if err != nil {
		t.Fatal(err)
	}

	dir, err := ioutil.TempDir("", "gocc-test-")
	if err != nil {
		t.Fatal(err)
	}

	defer os.RemoveAll(dir)

	if err := untar(dir, bytes.NewReader(b)); err != nil {
		t.Fatal(err)
	}

	wd, err := os.Getwd()
	if err != nil {
		t.Fatal(err)
	}

	if err := os.Chdir(filepath.Join(dir, filepath.FromSlash("mini-gmp/tests"))); err != nil {
		t.Fatal(err)
	}

	defer os.Chdir(wd)

	for _, v := range strings.Split(strings.TrimSpace(`
gocc -w -c testutils.c -o testutils.o
gocc -w -c t-add.c -o t-add.o
gocc -w -c hex-random.c -o hex-random.o
gocc -w -c mini-random.c -o mini-random.o
gocc -w t-add.o hex-random.o mini-random.o testutils.o -lgmp -lm -lmcheck -o t-add
gocc -w -c t-sub.c -o t-sub.o
gocc -w t-sub.o hex-random.o mini-random.o testutils.o -lgmp -lm -lmcheck -o t-sub
gocc -w -c t-mul.c -o t-mul.o
gocc -w t-mul.o hex-random.o mini-random.o testutils.o -lgmp -lm -lmcheck -o t-mul
gocc -w -c t-invert.c -o t-invert.o
gocc -w t-invert.o hex-random.o mini-random.o testutils.o -lgmp -lm -lmcheck -o t-invert
gocc -w -c t-div.c -o t-div.o
gocc -w t-div.o hex-random.o mini-random.o testutils.o -lgmp -lm -lmcheck -o t-div
gocc -w -c t-div_2exp.c -o t-div_2exp.o
gocc -w t-div_2exp.o hex-random.o mini-random.o testutils.o -lgmp -lm -lmcheck -o t-div_2exp
gocc -w -c t-double.c -o t-double.o
gocc -w t-double.o hex-random.o mini-random.o testutils.o -lgmp -lm -lmcheck -o t-double
gocc -w -c t-cmp_d.c -o t-cmp_d.o
gocc -w t-cmp_d.o hex-random.o mini-random.o testutils.o -lgmp -lm -lmcheck -o t-cmp_d
gocc -w -c t-gcd.c -o t-gcd.o
gocc -w t-gcd.o hex-random.o mini-random.o testutils.o -lgmp -lm -lmcheck -o t-gcd
gocc -w -c t-lcm.c -o t-lcm.o
gocc -w t-lcm.o hex-random.o mini-random.o testutils.o -lgmp -lm -lmcheck -o t-lcm
gocc -w -c t-import.c -o t-import.o
gocc -w t-import.o hex-random.o mini-random.o testutils.o -lgmp -lm -lmcheck -o t-import
gocc -w -c t-comb.c -o t-comb.o
gocc -w t-comb.o hex-random.o mini-random.o testutils.o -lgmp -lm -lmcheck -o t-comb
gocc -w -c t-signed.c -o t-signed.o
gocc -w t-signed.o hex-random.o mini-random.o testutils.o -lgmp -lm -lmcheck -o t-signed
gocc -w -c t-sqrt.c -o t-sqrt.o
gocc -w t-sqrt.o hex-random.o mini-random.o testutils.o -lgmp -lm -lmcheck -o t-sqrt
gocc -w -c t-root.c -o t-root.o
gocc -w t-root.o hex-random.o mini-random.o testutils.o -lgmp -lm -lmcheck -o t-root
gocc -w -c t-powm.c -o t-powm.o
gocc -w t-powm.o hex-random.o mini-random.o testutils.o -lgmp -lm -lmcheck -o t-powm
gocc -w -c t-logops.c -o t-logops.o
gocc -w t-logops.o hex-random.o mini-random.o testutils.o -lgmp -lm -lmcheck -o t-logops
gocc -w -c t-bitops.c -o t-bitops.o
gocc -w t-bitops.o hex-random.o mini-random.o testutils.o -lgmp -lm -lmcheck -o t-bitops
gocc -w -c t-scan.c -o t-scan.o
gocc -w t-scan.o hex-random.o mini-random.o testutils.o -lgmp -lm -lmcheck -o t-scan
gocc -w -c t-str.c -o t-str.o
gocc -w t-str.o hex-random.o mini-random.o testutils.o -lgmp -lm -lmcheck -o t-str
gocc -w -c t-reuse.c -o t-reuse.o
gocc -w t-reuse.o hex-random.o mini-random.o testutils.o -lgmp -lm -lmcheck -o t-reuse
gocc -w -c t-aorsmul.c -o t-aorsmul.o
gocc -w t-aorsmul.o hex-random.o mini-random.o testutils.o -lgmp -lm -lmcheck -o t-aorsmul
gocc -w -c t-limbs.c -o t-limbs.o
gocc -w t-limbs.o hex-random.o mini-random.o testutils.o -lgmp -lm -lmcheck -o t-limbs
gocc -w -c t-cong.c -o t-cong.o
gocc -w t-cong.o hex-random.o mini-random.o testutils.o -lgmp -lm -lmcheck -o t-cong
gocc -w -c t-pprime_p.c -o t-pprime_p.o
gocc -w t-pprime_p.o hex-random.o mini-random.o testutils.o -lgmp -lm -lmcheck -o t-pprime_p
`), "\n") {

		if *oTrace {
			fmt.Fprintln(os.Stderr, v)
		}
		out, err := execute(strings.Split(v, " "), opt)
		if err != nil {
			t.Fatalf("%s\n%v", out, err)
		}
	}
	for _, v := range strings.Split("t-add t-sub t-mul t-invert t-div t-div_2exp t-double t-cmp_d t-gcd t-lcm t-import t-comb t-signed t-sqrt t-root t-powm t-logops t-bitops t-scan t-str t-reuse t-aorsmul t-limbs t-cong t-pprime_p", " ") {
		if _, ok := blacklist[v]; ok {
			continue
		}

		if *oTrace {
			fmt.Fprintln(os.Stderr, v)
		}
		files++
		if re != nil && !re.MatchString(v) {
			continue
		}

		out, err := exec.Command("./" + v).CombinedOutput()
		if err != nil {
			t.Errorf("%v\n%s\n%v", v, out, err)
			continue
		}

		fmt.Fprintln(w, v)
		ok++
	}
	return files, ok
}

func untar(root string, r io.Reader) error {
	gr, err := gzip.NewReader(r)
	if err != nil {
		return err
	}

	tr := tar.NewReader(gr)
	for {
		hdr, err := tr.Next()
		if err != nil {
			if err != io.EOF {
				return err
			}

			return nil
		}

		switch hdr.Typeflag {
		case tar.TypeDir:
			if err = os.MkdirAll(filepath.Join(root, hdr.Name), 0770); err != nil {
				return err
			}
		case tar.TypeReg, tar.TypeRegA:
			f, err := os.OpenFile(filepath.Join(root, hdr.Name), os.O_CREATE|os.O_WRONLY, os.FileMode(hdr.Mode))
			if err != nil {
				return err
			}

			w := bufio.NewWriter(f)
			if _, err = io.Copy(w, tr); err != nil {
				return err
			}

			if err := w.Flush(); err != nil {
				return err
			}

			if err := f.Close(); err != nil {
				return err
			}
		default:
			return fmt.Errorf("unexpected tar header typeflag %#02x", hdr.Typeflag)
		}
	}
}

func dumpTODOs(t *testing.T) {
	var a []string
	sum := 0
	for k, v := range todos {
		sum += v
		a = append(a, fmt.Sprintf("%6d %s:%d", v, k.file, k.line))
	}
	sort.Sort(sort.Reverse(sort.StringSlice(a)))
	t.Logf("\n%s\nsum: %d", strings.Join(a, "\n"), sum)
}

func TestGCCExec(t *testing.T) {
	root := filepath.Join(testWD, filepath.FromSlash(gccDir))
	if _, err := os.Stat(root); err != nil {
		t.Fatalf("Missing resources in %s. Please run 'go test -download -dev' to fix.", root)
	}

	g := newGolden(t, fmt.Sprintf("testdata/gcc_exec_%s_%s.golden", runtime.GOOS, runtime.GOARCH))

	defer g.close()

	todos = map[todoPos]int{}
	var files, ok int
	if *oEdit {
		defer func() {
			fmt.Printf("GCC files %s, ok %s\n", h(files), h(ok))
		}()
	}
	const dir = "gcc/testsuite/gcc.c-torture/execute"
	t.Run("noOpt", func(t *testing.T) {
		f, o := testGCCExec(g.w, t, filepath.Join(root, filepath.FromSlash(dir)), false)
		files += f
		ok += o
	})
	t.Logf("files %s, ok %s", h(files), h(ok))
	if !oQuiet {
		dumpTODOs(t)
	}

	g2 := newGolden(t, fmt.Sprintf("testdata/gcc_exec_%s_%s.opt.golden", runtime.GOOS, runtime.GOARCH))

	defer g2.close()
	todos = map[todoPos]int{}
	files = 0
	ok = 0
	t.Run("opt", func(t *testing.T) {
		f, o := testGCCExec(g2.w, t, filepath.Join(root, filepath.FromSlash(dir)), true)
		files += f
		ok += o
	})
	t.Logf("files %s, ok %s", h(files), h(ok))
	if !oQuiet {
		dumpTODOs(t)
	}
}

func testGCCExec(w io.Writer, t *testing.T, dir string, opt bool) (files, ok int) {
	const binaryName = "a.out"
	blacklist := map[string]struct{}{ //TODO-
		"20021127-1.c": {}, // non standard GCC behavior
		"strlen-3.c":   {}, // not a standalone test
		"eeprof-1.c":   {}, // requires instrumentation

		"20010904-1.c":        {}, //TODO __attribute__((aligned(alignment)))
		"20010904-2.c":        {}, //TODO __attribute__((aligned(alignment)))
		"20040411-1.c":        {}, //TODO typedef VLA
		"20040423-1.c":        {}, //TODO typedef VLA
		"20041218-2.c":        {}, //TODO struct VLA
		"20050215-1.c":        {}, //TODO __attribute__ aligned
		"991014-1.c":          {}, //TODO struct size overflow
		"align-3.c":           {}, //TODO attr
		"const-addr-expr-1.c": {}, //TODO complex const addresss initialzer
		"pr23135.c":           {}, //TODO QBE OOM
		"pr41935.c":           {}, //TODO * VLA
		"pr64006.c":           {}, //TODO __builtin_mul_overflow
		"pr82210.c":           {}, //TODO VLA struct array, hangs
		"pr85095.c":           {}, //TODO __builtin_add_overflow
		"rbug.c":              {}, //TODO floating point rounding?
		"simd-1.c":            {}, //TODO __attribute__((vector_size (16)))
		"simd-2.c":            {}, //TODO __attribute__((vector_size (16)))
	}
	if opt {
		blacklist["20101011-1.c"] = struct{}{} // gcc 7.4 does not respect __attribute__ ((used)).
	}
	if opt && (arch == 32 || *oM32) {
		blacklist["20030331-1.c"] = struct{}{}    // gcc 7.4 -O -m32 bug
		blacklist["960830-1.c"] = struct{}{}      // asm
		blacklist["floatunsisf-1.c"] = struct{}{} // gcc 7.4 -O -m32 bug
	}
	wd, err := os.Getwd()
	if err != nil {
		t.Fatal(err)
	}

	defer os.Chdir(wd)

	temp, err := ioutil.TempDir("", "gocc-test-")
	if err != nil {
		t.Fatal(err)
	}

	defer os.RemoveAll(temp)

	if err := os.Chdir(temp); err != nil {
		t.Fatal(err)
	}

	var re *regexp.Regexp
	if s := *oRE; s != "" {
		re = regexp.MustCompile(s)
	}

	if err := filepath.Walk(dir, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			if os.IsNotExist(err) {
				err = nil
			}
			return err
		}

		if info.IsDir() {
			return skipDir(path)
		}

		if strings.Contains(filepath.ToSlash(path), "/builtins/") {
			return nil
		}

		if filepath.Ext(path) != ".c" || info.Mode()&os.ModeType != 0 {
			return nil
		}

		if _, ok := blacklist[filepath.Base(path)]; ok {
			return nil
		}

		files++

		if re != nil && !re.MatchString(path) {
			return nil
		}

		if *oTrace {
			fmt.Fprintln(os.Stderr, files, ok, path)
		}

		if err := os.Remove(binaryName); err != nil && !os.IsNotExist(err) {
			return err
		}

		goccArgs := []string{"gocc", "-o", binaryName, "-Dasm=__asm__", "-w"}
		if !func() (r bool) {
			defer func() {
				if err := recover(); err != nil {
					if *oStackTrace {
						fmt.Printf("%s\n", stack())
					}
					if *oTrace {
						fmt.Println(err)
					}
					if !oQuiet {
						fail(t, "%s: %v", path, err)
					}
					r = false
				}
			}()

			if _, err := execute(append(goccArgs, path, "-lm"), opt, gccTestDecls); err != nil {
				if *oTrace {
					fmt.Println(err)
				}
				if !oQuiet {
					fail(t, "%s: %v", path, err)
				}
				return false
			}

			return true
		}() {
			return nil
		}

		out, err := exec.Command("./" + binaryName).CombinedOutput()
		if err != nil {
			if *oTrace {
				fmt.Println(err)
			}
			t.Errorf("%v: %v: exec fail", path, err)
			return nil
		}

		if *oTraceO {
			fmt.Printf("%s\n", out)
		}

		fmt.Fprintln(w, filepath.Base(path))
		ok++
		return nil
	}); err != nil {
		fail(t, "%v", err)
	}
	return files, ok
}

func TestGCCGoExec(t *testing.T) {
	root := filepath.Join(testWD, filepath.FromSlash(gccDir))
	if _, err := os.Stat(root); err != nil {
		t.Fatalf("Missing resources in %s. Please run 'go test -download -dev' to fix.", root)
	}

	g := newGolden(t, fmt.Sprintf("testdata/gcc_go_exec_%s_%s.golden", runtime.GOOS, runtime.GOARCH))

	defer g.close()

	todos = map[todoPos]int{}
	var files, ok int
	if *oEdit {
		defer func() {
			fmt.Printf("GCC files %s, ok %s\n", h(files), h(ok))
		}()
	}
	const dir = "gcc/testsuite/gcc.c-torture/execute"
	f, o := testGCCGoExec(g.w, t, filepath.Join(root, filepath.FromSlash(dir)), false)
	files += f
	ok += o
	t.Logf("files %s, ok %s", h(files), h(ok))
	if !oQuiet {
		dumpTODOs(t)
	}
}

func testGCCGoExec(w io.Writer, t *testing.T, dir string, opt bool) (files, ok int) {
	const (
		bin  = "a.out"
		main = "main.go"
		crt  = "crt.go"
	)
	blacklist := map[string]struct{}{ //TODO-
		"20021127-1.c": {}, // non standard GCC behavior
		"strlen-3.c":   {}, // not a standalone test
		"eeprof-1.c":   {}, // requires instrumentation

		"20010904-1.c":        {}, //TODO __attribute__((aligned(alignment)))
		"20010904-2.c":        {}, //TODO __attribute__((aligned(alignment)))
		"20040411-1.c":        {}, //TODO typedef VLA
		"20040423-1.c":        {}, //TODO typedef VLA
		"20041218-2.c":        {}, //TODO struct VLA
		"20050215-1.c":        {}, //TODO __attribute__ aligned
		"991014-1.c":          {}, //TODO struct size overflow
		"align-3.c":           {}, //TODO attr
		"const-addr-expr-1.c": {}, //TODO complex const addresss initialzer
		"pr23135.c":           {}, //TODO QBE OOM
		"pr41935.c":           {}, //TODO * VLA
		"pr64006.c":           {}, //TODO __builtin_mul_overflow
		"pr82210.c":           {}, //TODO VLA struct array, hangs
		"pr85095.c":           {}, //TODO __builtin_add_overflow
		"rbug.c":              {}, //TODO floating point rounding?
		"simd-1.c":            {}, //TODO __attribute__((vector_size (16)))
		"simd-2.c":            {}, //TODO __attribute__((vector_size (16)))
	}
	if opt {
		blacklist["20101011-1.c"] = struct{}{} // gcc 7.4 does not respect __attribute__ ((used)).
	}
	if opt && (arch == 32 || *oM32) {
		blacklist["20030331-1.c"] = struct{}{}    // gcc 7.4 -O -m32 bug
		blacklist["960830-1.c"] = struct{}{}      // asm
		blacklist["floatunsisf-1.c"] = struct{}{} // gcc 7.4 -O -m32 bug
	}
	wd, err := os.Getwd()
	if err != nil {
		t.Fatal(err)
	}

	defer os.Chdir(wd)

	temp, err := ioutil.TempDir("", "gocc-test-")
	if err != nil {
		t.Fatal(err)
	}

	defer os.RemoveAll(temp)

	if err := os.Chdir(temp); err != nil {
		t.Fatal(err)
	}

	var re *regexp.Regexp
	if s := *oRE; s != "" {
		re = regexp.MustCompile(s)
	}

	if err := filepath.Walk(dir, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			if os.IsNotExist(err) {
				err = nil
			}
			return err
		}

		if info.IsDir() {
			return skipDir(path)
		}

		if strings.Contains(filepath.ToSlash(path), "/builtins/") {
			return nil
		}

		if filepath.Ext(path) != ".c" || info.Mode()&os.ModeType != 0 {
			return nil
		}

		if _, ok := blacklist[filepath.Base(path)]; ok {
			return nil
		}

		files++

		if re != nil && !re.MatchString(path) {
			return nil
		}

		if *oTrace {
			fmt.Fprintln(os.Stderr, files, ok, path)
		}

		if err := os.Remove(main); err != nil && !os.IsNotExist(err) {
			return err
		}

		goccArgs := []string{
			"gocc",
			"-o", main,
			"-Dasm=__asm__",
			"-DSIGNAL_SUPPRESS",
			"-gocc-long-double-is-double",
			"-gocc-emit-definitions",
		}
		if !func() (r bool) {
			defer func() {
				if err := recover(); err != nil {
					if *oStackTrace {
						fmt.Printf("%s\n", stack())
					}
					if *oTrace {
						fmt.Println(err)
					}
					if !oQuiet {
						fail(t, "%s: %v", path, err)
					}
					r = false
				}
			}()

			if out, err := execute(append(goccArgs, path), opt, gccTestDecls); err != nil {
				if *oTrace {
					fmt.Printf("%s\n%s\n", out, err)
				}
				if !oQuiet {
					fail(t, "%s: %s\n%s", path, out, err)
				}
				return false
			}

			return true
		}() {
			return nil
		}

		os.Remove(bin)
		out, err := exec.Command("go", "build", "-o", bin, main).CombinedOutput()
		if err != nil {
			if *oTrace {
				fmt.Printf("%s\n%s\n", out, err)
			}
			t.Errorf("%v: %s\n%s", path, out, err)
			return nil
		}

		if out, err = exec.Command("./" + bin).CombinedOutput(); err != nil {
			if *oTrace {
				fmt.Printf("%s\n%s\n", out, err)
			}
			t.Errorf("%v: %v: exec fail", path, err)
			return nil
		}

		if *oTraceO {
			fmt.Printf("%s\n", out)
		}

		fmt.Fprintln(w, filepath.Base(path))
		ok++
		return nil
	}); err != nil {
		fail(t, "%v", err)
	}
	return files, ok
}

func TestSQLite(t *testing.T) {
	root := filepath.Join(testWD, filepath.FromSlash(sqliteDir))
	if _, err := os.Stat(root); err != nil {
		t.Fatalf("Missing resources in %s. Please run 'go test -download' to fix.", root)
	}

	t.Run("noOpt", func(t *testing.T) { testSQLite(t, root, false) })
	t.Run("opt", func(t *testing.T) { testSQLite(t, root, true) })
}

func testSQLite(t *testing.T, dir string, opt bool) {
	const binaryName = "a.out"
	wd, err := os.Getwd()
	if err != nil {
		t.Fatal(err)
	}

	defer os.Chdir(wd)

	temp, err := ioutil.TempDir("", "gocc-test-")
	if err != nil {
		t.Fatal(err)
	}

	defer os.RemoveAll(temp)

	if err := os.Chdir(temp); err != nil {
		t.Fatal(err)
	}

	if !func() (r bool) {
		defer func() {
			if err := recover(); err != nil {
				if *oStackTrace {
					fmt.Printf("%s\n", stack())
				}
				if *oTrace {
					fmt.Println(err)
				}
				if !oQuiet {
					fail(t, "%s: %v", dir, err)
				}
				r = false
			}
		}()

		if _, err := execute(append([]string{"gocc"}, filepath.Join(dir, "shell.c"), filepath.Join(dir, "sqlite3.c"), "-lpthread", "-ldl"), opt); err != nil {
			if *oTrace {
				fmt.Println(err)
			}
			if !oQuiet {
				fail(t, "%s: %v", dir, err)
			}
			return false
		}

		return true
	}() {
		return
	}

	out, err := exec.Command("./"+binaryName, "tmp", "create table t(i); insert into t values(42); select 11*i from t;").CombinedOutput()
	if err != nil {
		if *oTrace {
			fmt.Println(err)
		}
		fail(t, "%v", err)
		return
	}

	if g, e := strings.TrimSpace(string(out)), "462"; g != e {
		t.Errorf("%q %q", g, e)
	}
	if *oTraceO {
		fmt.Printf("%s\n", out)
	}

	if out, err = exec.Command("./"+binaryName, "tmp", "select 13*i from t;").CombinedOutput(); err != nil {
		if *oTrace {
			fmt.Println(err)
		}
		fail(t, "%v", err)
		return
	}

	if g, e := strings.TrimSpace(string(out)), "546"; g != e {
		t.Errorf("%q %q", g, e)
	}
	if *oTraceO {
		fmt.Printf("%s\n", out)
	}
}

func TestSQLiteGo(t *testing.T) {
	root := filepath.Join(testWD, filepath.FromSlash(sqliteDir))
	if _, err := os.Stat(root); err != nil {
		t.Fatalf("Missing resources in %s. Please run 'go test -download' to fix.", root)
	}

	testSQLiteGo(t, root)
}

func testSQLiteGo(t *testing.T, dir string) {
	const main = "main.go"
	wd, err := os.Getwd()
	if err != nil {
		t.Fatal(err)
	}

	defer os.Chdir(wd)

	temp, err := ioutil.TempDir("", "gocc-test-")
	if err != nil {
		t.Fatal(err)
	}

	defer os.RemoveAll(temp)

	if err := os.Chdir(temp); err != nil {
		t.Fatal(err)
	}

	if !func() (r bool) {
		defer func() {
			if err := recover(); err != nil {
				if *oStackTrace {
					fmt.Printf("%s\n", stack())
				}
				if *oTrace {
					fmt.Println(err)
				}
				if !oQuiet {
					fail(t, "%s: %v", dir, err)
				}
				r = false
			}
		}()

		if out, err := execute(append([]string{"gocc"},
			filepath.Join(dir, "shell.c"),
			"-o", "shell.go",
			"-DLONGDOUBLE_TYPE=double",
			"-DSQLITE_DEBUG",
			"-DSQLITE_DEFAULT_MEMSTATUS=0",
			"-DSQLITE_DEFAULT_WAL_SYNCHRONOUS=1",
			"-DSQLITE_DQS=0",
			"-DSQLITE_LIKE_DOESNT_MATCH_BLOBS",
			"-DSQLITE_MAX_EXPR_DEPTH=0",
			"-DSQLITE_MEMDEBUG",
			"-DSQLITE_OMIT_DECLTYPE",
			"-DSQLITE_OMIT_DEPRECATED",
			"-DSQLITE_OMIT_PROGRESS_CALLBACK",
			"-DSQLITE_OMIT_SHARED_CACHE",
			"-DSQLITE_THREADSAFE=0",
			"-DSQLITE_USE_ALLOCA",
		), false); err != nil {
			if *oTrace {
				fmt.Printf("%s\n%s\n", out, err)
			}
			if !oQuiet {
				fail(t, "%s: %s\n%s", dir, out, err)
			}
			return false
		}

		if out, err := execute(append([]string{"gocc"},
			filepath.Join(dir, "sqlite3.c"),
			"-o", "sqlite3.go",
			"-qbec-pkgname", "main",
			"-DLONGDOUBLE_TYPE=double",
			"-DSQLITE_DEBUG",
			"-DSQLITE_DEFAULT_MEMSTATUS=0",
			"-DSQLITE_DEFAULT_WAL_SYNCHRONOUS=1",
			"-DSQLITE_DQS=0",
			"-DSQLITE_LIKE_DOESNT_MATCH_BLOBS",
			"-DSQLITE_MAX_EXPR_DEPTH=0",
			"-DSQLITE_MEMDEBUG",
			"-DSQLITE_OMIT_DECLTYPE",
			"-DSQLITE_OMIT_DEPRECATED",
			"-DSQLITE_OMIT_PROGRESS_CALLBACK",
			"-DSQLITE_OMIT_SHARED_CACHE",
			"-DSQLITE_THREADSAFE=0",
			"-DSQLITE_USE_ALLOCA",
		),
			false,
		); err != nil {
			if *oTrace {
				fmt.Printf("%s\n%s\n", out, err)
			}
			if !oQuiet {
				fail(t, "%s: %s\n%s", dir, out, err)
			}
			return false
		}

		return true
	}() {
		return
	}

	// out, err := exec.Command("go", "build", "-tags", "crt.dmesg", "-gcflags", "-e", "-o", "test", "shell.go", "sqlite3.go").CombinedOutput()
	out, err := exec.Command("go", "build", "-gcflags", "-e", "-o", "test", "shell.go", "sqlite3.go").CombinedOutput()
	if err != nil {
		if *oTrace {
			fmt.Printf("%s\n%s\n", out, err)
		}
		fail(t, "%v", err)
		return
	}

	if out, err = exec.Command("./test", "tmp", "create table t(i); insert into t values(42); select 11*i from t;").CombinedOutput(); err != nil {
		if *oTrace {
			fmt.Printf("%s\n%s\n", out, err)
		}
		fail(t, "%v", err)
		return
	}

	if g, e := strings.TrimSpace(string(out)), "462"; g != e {
		t.Errorf("%q %q", g, e)
	}
	if *oTraceO {
		fmt.Printf("%s\n", out)
	}

	if out, err = exec.Command("./test", "tmp", "select 13*i from t;").CombinedOutput(); err != nil {
		if *oTrace {
			fmt.Printf("%s\n%s\n", out, err)
		}
		fail(t, "%v", err)
		return
	}

	if g, e := strings.TrimSpace(string(out)), "546"; g != e {
		t.Errorf("%q %q", g, e)
	}
	if *oTraceO {
		fmt.Printf("%s\n", out)
	}
}

func fields(t cc.Type) string {
	var b strings.Builder
	fields2(t, &b, "")
	return b.String()
}

func fields2(t cc.Type, b *strings.Builder, pref string) {
	fmt.Fprintf(b, "%s==== typ %v, kind %v, sz %v align %v\n", pref, t, t.Kind(), t.Size(), t.Align())
	for i := 0; i < t.NumField(); i++ {
		f := t.FieldByIndex([]int{i})
		fmt.Fprintf(b,
			"%snm %q, type %v, sz %v, align %v, off %v, pad %v, bitfield %v, boff %v bits %v\n",
			pref, f.Name(), f.Type(), f.Type().Size(), f.Type().Align(), f.Offset(), f.Padding(),
			f.IsBitField(), f.BitFieldOffset(), f.BitFieldWidth(),
		)
		switch f.Type().Kind() {
		case cc.Struct, cc.Union:
			fields2(f.Type(), b, pref+"· ")
		}
	}
}

func TestCSmith(t *testing.T) {
	gcc, err := exec.LookPath("gcc")
	if err != nil {
		t.Fatalf("%v", err)
		return
	}

	csmith, err := exec.LookPath("csmith")
	if err != nil {
		t.Fatalf("%v", err)
		return
	}

	if _, err := exec.LookPath("gocc"); err != nil {
		t.Fatalf("%v: please run go install", err)
		return
	}

	binaryName := filepath.FromSlash("./a.out")
	wd, err := os.Getwd()
	if err != nil {
		t.Fatal(err)
	}

	defer os.Chdir(wd)

	temp, err := ioutil.TempDir("", "gocc-test-")
	if err != nil {
		t.Fatal(err)
	}

	defer os.RemoveAll(temp)

	if err := os.Chdir(temp); err != nil {
		t.Fatal(err)
	}

	fixedBugs := []string{
		"--bitfields --no-const-pointers --no-consts --no-packed-struct --no-volatile-pointers --no-volatiles --paranoid --max-nested-struct-level 10 -s 1906742816",
		"--bitfields --no-const-pointers --no-consts --no-packed-struct --no-volatile-pointers --no-volatiles --paranoid --max-nested-struct-level 10 -s 612971101",
		"--bitfields --no-const-pointers --no-consts --no-packed-struct --no-volatile-pointers --no-volatiles --paranoid --max-nested-struct-level 10 -s 3629008936",
		"--bitfields --max-nested-struct-level 10 --no-const-pointers --no-consts --no-packed-struct --no-volatile-pointers --no-volatiles --paranoid -s 4130344133",
		"--bitfields --max-nested-struct-level 10 --no-const-pointers --no-consts --no-packed-struct --no-volatile-pointers --no-volatiles --paranoid -s 3130410542",
		"--bitfields --max-nested-struct-level 10 --no-const-pointers --no-consts --no-packed-struct --no-volatile-pointers --no-volatiles --paranoid -s 1833258637",
		"--bitfields --max-nested-struct-level 10 --no-const-pointers --no-consts --no-packed-struct --no-volatile-pointers --no-volatiles --paranoid -s 3126091077",
		"--bitfields --max-nested-struct-level 10 --no-const-pointers --no-consts --no-packed-struct --no-volatile-pointers --no-volatiles --paranoid -s 2205128324",
		"--bitfields --max-nested-struct-level 10 --no-const-pointers --no-consts --no-packed-struct --no-volatile-pointers --no-volatiles --paranoid -s 3043990076",
		"--bitfields --max-nested-struct-level 10 --no-const-pointers --no-consts --no-packed-struct --no-volatile-pointers --no-volatiles --paranoid -s 2517344771",
		"--bitfields --max-nested-struct-level 10 --no-const-pointers --no-consts --no-packed-struct --no-volatile-pointers --no-volatiles --paranoid -s 56498550",
		"--bitfields --max-nested-struct-level 10 --no-const-pointers --no-consts --no-packed-struct --no-volatile-pointers --no-volatiles --paranoid -s 3645367888",
		"--bitfields --max-nested-struct-level 10 --no-const-pointers --no-consts --no-packed-struct --no-volatile-pointers --no-volatiles --paranoid -s 169375684",
		"--bitfields --max-nested-struct-level 10 --no-const-pointers --no-consts --no-packed-struct --no-volatile-pointers --no-volatiles --paranoid -s 3578720023",
		"--bitfields --max-nested-struct-level 10 --no-const-pointers --no-consts --no-packed-struct --no-volatile-pointers --no-volatiles --paranoid -s 1885311141",
	}
	ch := time.After(*oCSmith)
	t0 := time.Now()
	var files, ok int
	var size int64
out:
	for i := 0; ; i++ {
		extra := ""
		var args string
		switch {
		case i < len(fixedBugs):
			args += fixedBugs[i]
			a := strings.Split(fixedBugs[i], " ")
			extra = strings.Join(a[len(a)-2:], " ")
			t.Log(args)
		default:
			select {
			case <-ch:
				break out
			default:
			}

			args += csmithDefaultArgs
		}
		csOut, err := exec.Command(csmith, strings.Split(args, " ")...).Output()
		if err != nil {
			t.Fatalf("%v\n%s", err, csOut)
		}

		if fn := *oBlackBox; fn != "" {
			if err := ioutil.WriteFile(fn, csOut, 0660); err != nil {
				t.Fatal(err)
			}
		}

		if err := ioutil.WriteFile("main.c", csOut, 0660); err != nil {
			t.Fatal(err)
		}

		csp := fmt.Sprintf("-I%s", filepath.FromSlash("/usr/include/csmith"))
		ccOut, err := exec.Command(gcc, "-o", binaryName, "main.c", csp).CombinedOutput()
		if err != nil {
			t.Fatalf("%s\n%s\ncc: %v", extra, ccOut, err)
		}

		binOutA, err := func() ([]byte, error) {
			ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
			defer cancel()

			return exec.CommandContext(ctx, binaryName).CombinedOutput()
		}()
		if err != nil {
			continue
		}

		size += int64(len(csOut))

		if err := os.Remove(binaryName); err != nil {
			t.Fatal(err)
		}

		// noOpt
		files++
		j, err := newTask([]string{"gocc", "-o", binaryName, "-O0", "-lm", csp, "main.c"})
		j.doNotCacheMain = true
		j.Config3.MaxSourceLine = 1 << 19
		if err != nil {
			t.Error(err)
			break
		}

		func() {
			var stdout, stderr bytes.Buffer

			defer func() {
				if err := recover(); err != nil {
					t.Fatalf("%s\n%s\ngocc: %s\n%s\n%v\n%s", extra, csOut, stdout.Bytes(), stderr.Bytes(), err, debug.Stack())
				}
			}()

			if rc := j.main(&stdout, &stderr); rc != 0 || stdout.Len() != 0 {
				t.Fatalf("%s\n%s\ngocc: %s\n%s\n%v", extra, csOut, stdout.Bytes(), stderr.Bytes(), err)
			}
		}()

		binOutB, err := func() ([]byte, error) {
			ctx, cancel := context.WithTimeout(context.Background(), 100*time.Second)
			defer cancel()

			return exec.CommandContext(ctx, binaryName).CombinedOutput()
		}()
		if err != nil {
			t.Errorf("%s\n%s\ngocc: %v", extra, csOut, err)
			break
		}

		if g, e := binOutB, binOutA; !bytes.Equal(g, e) {
			t.Errorf("%s\n%s\ngocc: %v\ngot: %s\nexp: %s", extra, csOut, err, g, e)
			break
		}

		ok++
		if *oTrace {
			fmt.Fprintln(os.Stderr, time.Since(t0), files, ok, " no opt")
		}

		if err := os.Remove(binaryName); err != nil {
			t.Fatal(err)
		}

		// opt
		files++
		j, err = newTask([]string{"gocc", "-o", binaryName, "-lm", csp, "main.c"})
		j.doNotCacheMain = true
		j.Config3.MaxSourceLine = 1 << 19
		if err != nil {
			t.Fatal(err)
		}

		func() {
			var stdout, stderr bytes.Buffer

			defer func() {
				if err := recover(); err != nil {
					t.Fatalf("%s\n%s\ngocc: %s\n%s\n%v\n%s", extra, csOut, stdout.Bytes(), stderr.Bytes(), err, debug.Stack())
				}
			}()

			if rc := j.main(&stdout, &stderr); rc != 0 || stdout.Len() != 0 {
				t.Fatalf("%s\n%s\ngocc: %s\n%s\n%v", extra, csOut, stdout.Bytes(), stderr.Bytes(), err)
			}
		}()

		binOutB, err = func() ([]byte, error) {
			ctx, cancel := context.WithTimeout(context.Background(), 100*time.Second)
			defer cancel()

			return exec.CommandContext(ctx, binaryName).CombinedOutput()
		}()
		if err != nil {
			t.Errorf("%s\n%s\ngocc: %v", extra, csOut, err)
			break
		}

		if g, e := binOutB, binOutA; !bytes.Equal(g, e) {
			t.Errorf("%s\n%s\ngocc: %v\ngot: %s\nexp: %s", extra, csOut, err, g, e)
			break
		}

		ok++
		if *oTrace {
			fmt.Fprintln(os.Stderr, time.Since(t0), files, ok, " opt")
		}

		// Go
		os.Remove("main.go")
		os.Remove(binaryName)
		files++
		j, err = newTask([]string{"gocc", "-o", "main.go", csp, "main.c", "-gocc-long-double-is-double"})
		j.doNotCacheMain = true
		j.Config3.MaxSourceLine = 1 << 19
		if err != nil {
			t.Fatal(err)
		}

		func() {
			var stdout, stderr bytes.Buffer

			defer func() {
				if err := recover(); err != nil {
					t.Fatalf("%s\n%s\ngocc: %s\n%s\n%v\n%s", extra, csOut, stdout.Bytes(), stderr.Bytes(), err, debug.Stack())
				}
			}()

			if rc := j.main(&stdout, &stderr); rc != 0 || stdout.Len() != 0 {
				t.Fatalf("%s\n%s\ngocc: %s\n%s\n%v", extra, csOut, stdout.Bytes(), stderr.Bytes(), err)
			}
		}()

		out, err := exec.Command("go", "build", "-o", binaryName, "main.go").CombinedOutput()
		if err != nil {
			t.Errorf("%s\n%s\ngocc: %v", extra, out, err)
			break
		}

		binOutB, err = func() ([]byte, error) {
			ctx, cancel := context.WithTimeout(context.Background(), 100*time.Second)
			defer cancel()

			return exec.CommandContext(ctx, binaryName).CombinedOutput()
		}()
		if err != nil {
			t.Errorf("%s\n%s\ngocc: %v", extra, csOut, err)
			break
		}

		if g, e := binOutB, binOutA; !bytes.Equal(g, e) {
			t.Errorf("%s\n%s\ngocc: %v\ngot: %s\nexp: %s", extra, csOut, err, g, e)
			break
		}

		ok++
		if *oTrace {
			fmt.Fprintln(os.Stderr, time.Since(t0), files, ok, " Go")
		}
	}
	d := time.Since(t0)
	t.Logf("files %v, bytes %v, ok %v in %v", h(files), h(size), h(ok), d)
}

type compCertResult struct {
	compiler string
	test     string
	run      time.Duration
	k        float64

	compileOK bool
	execOK    bool
	resultOK  bool
}

func (r *compCertResult) String() string {
	var s string
	if r.k != 0 {
		s = fmt.Sprintf("%8.3f", r.k)
		if r.k == 1 {
			s += " *"
		}
	}
	return fmt.Sprintf("%10v%15v%10.3fms%6v%6v%6v%s", r.compiler, r.test, float64(r.run)/float64(time.Millisecond), r.compileOK, r.execOK, r.resultOK, s)
}

func TestCompCert(t *testing.T) {
	const root = "testdata/CompCert-3.6/test/c"

	b, err := ioutil.ReadFile(filepath.FromSlash(root + "/Results/knucleotide-input.txt"))
	if err != nil {
		t.Fatal(err)
	}

	dir := filepath.FromSlash(root)
	m, err := filepath.Glob(filepath.Join(dir, "*.c"))
	if err != nil {
		t.Fatal(err)
	}

	for i, v := range m {
		v, err := filepath.Abs(v)
		if err != nil {
			t.Fatal(err)
		}

		m[i] = v
	}

	rdir, err := filepath.Abs(filepath.FromSlash(root + "/Results"))
	if err != nil {
		t.Fatal(err)
	}

	wd, err := os.Getwd()
	if err != nil {
		t.Fatal(err)
	}

	defer os.Chdir(wd)

	temp, err := ioutil.TempDir("", "gocc-test-")
	if err != nil {
		t.Fatal(err)
	}

	defer os.RemoveAll(temp)

	if err := os.Chdir(temp); err != nil {
		t.Fatal(err)
	}

	if err := os.Mkdir("Results", 0770); err != nil {
		t.Fatal(err)
	}

	if err := ioutil.WriteFile(filepath.FromSlash("Results/knucleotide-input.txt"), b, 0660); err != nil {
		t.Fatal(err)
	}

	var r []*compCertResult
	t.Run("gcc", func(t *testing.T) { r = append(r, testCompCertGcc(t, m, *oCertN, rdir)...) })
	t.Run("gocc", func(t *testing.T) { r = append(r, testCompCertGocc(t, m, *oCertN, rdir)...) })
	t.Run("goccgo", func(t *testing.T) { r = append(r, testCompCertGoccgo(t, m, *oCertN, rdir)...) })
	consider := map[string]struct{}{}
	for _, v := range r {
		consider[v.test] = struct{}{}
	}
	for _, v := range r {
		if ok := v.compileOK && v.execOK && v.resultOK; !ok {
			delete(consider, v.test)
		}
	}
	times := map[string]time.Duration{}
	tests := map[string][]*compCertResult{}
	for _, v := range r {
		if _, ok := consider[v.test]; !ok {
			continue
		}

		times[v.compiler] += v.run
		tests[v.test] = append(tests[v.test], v)
	}
	for _, a := range tests {
		if len(a) < 2 {
			continue
		}
		min := time.Duration(-1)
		for _, v := range a {
			if min < 0 || v.run < min {
				min = v.run
			}
		}
		for _, v := range a {
			v.k = float64(v.run) / float64(min)
		}
	}
	t.Log("  compiler           test    T         comp  exec match    coef")
	for _, v := range r {
		t.Log(v)
	}
	var a []string
	for k := range times {
		a = append(a, k)
	}
	sort.Strings(a)
	t.Logf("Considered tests: %d/%d", len(consider), len(m))
	min := time.Duration(-1)
	for _, v := range times {
		if min < 0 || v < min {
			min = v
		}
	}
	for _, k := range a {
		t.Logf("%10s%15v %6.3f", k, times[k], float64(times[k])/float64(min))
	}
}

func testCompCertGcc(t *testing.T, files []string, N int, rdir string) (r []*compCertResult) {
	const nm = "gcc"
next:
	for _, fn := range files {
		base := filepath.Base(fn)
		if *oTrace {
			fmt.Println(base)
		}
		bin := nm + "-" + base + ".out"
		out, err := exec.Command("gcc", "-O", "-o", bin, fn, "-lm").CombinedOutput()
		if err != nil {
			t.Errorf("%s: %s:\n%s", base, err, out)
			r = append(r, &compCertResult{nm, base, 0, 0, false, false, false})
			continue
		}

		t0 := time.Now()
		for i := 0; i < N; i++ {
			if out, err = exec.Command("./" + bin).CombinedOutput(); err != nil {
				t.Errorf("%s: %s:\n%s", base, err, out)
				r = append(r, &compCertResult{nm, base, 0, 0, true, false, false})
				continue next
			}
		}
		d := time.Since(t0) / time.Duration(N)
		r = append(r, &compCertResult{nm, base, d, 0, true, true, checkResult(t, out, base, rdir)})
	}
	return r
}

func checkResult(t *testing.T, out []byte, base, rdir string) bool {
	base = base[:len(base)-len(filepath.Ext(base))]
	b, err := ioutil.ReadFile(filepath.Join(rdir, base))
	if err != nil {
		t.Errorf("%v: %v", base, err)
		return false
	}

	if !bytes.Equal(out, b) {
		t.Logf("got\n%s", hex.Dump(out))
		t.Logf("exp\n%s", hex.Dump(b))
		t.Errorf("%v: result differs", base)
		return false
	}

	return true
}

func testCompCertGocc(t *testing.T, files []string, N int, rdir string) (r []*compCertResult) {
	const nm = "gocc"
next:
	for _, fn := range files {
		base := filepath.Base(fn)
		if *oTrace {
			fmt.Println(base)
		}
		bin := nm + "-" + base + ".out"
		out, err := execute([]string{"gocc", "-O", "-o", bin, fn, "-lm"}, false)
		if err != nil {
			t.Errorf("%s: %s:\n%s", base, err, out)
			r = append(r, &compCertResult{nm, base, 0, 0, false, false, false})
			continue
		}

		t0 := time.Now()
		for i := 0; i < N; i++ {
			if out, err = exec.Command("./" + bin).CombinedOutput(); err != nil {
				t.Errorf("%s: %s:\n%s", base, err, out)
				r = append(r, &compCertResult{nm, base, 0, 0, true, false, false})
				continue next
			}
		}
		d := time.Since(t0) / time.Duration(N)
		r = append(r, &compCertResult{nm, base, d, 0, true, true, checkResult(t, out, base, rdir)})
	}
	return r
}

func testCompCertGoccgo(t *testing.T, files []string, N int, rdir string) (r []*compCertResult) {
	const nm = "goccgo"
next:
	for _, fn := range files {
		base := filepath.Base(fn)
		if *oTrace {
			fmt.Println(base)
		}
		src := nm + "-" + base + ".go"
		bin := nm + "-" + base + ".out"
		out, err := execute([]string{"gocc", "-o", src, fn}, false)
		if err != nil {
			t.Errorf("%s: %s:\n%s", base, err, out)
			r = append(r, &compCertResult{nm, base, 0, 0, false, false, false})
			continue
		}

		//if out, err = exec.Command("go", "build", "-o", bin, "-tags", "crt.dmesg", src).CombinedOutput(); err != nil {
		if out, err = exec.Command("go", "build", "-o", bin, src).CombinedOutput(); err != nil {
			t.Errorf("%s: %s:\n%s", base, err, out)
			r = append(r, &compCertResult{nm, base, 0, 0, false, false, false})
			continue next
		}

		t0 := time.Now()
		for i := 0; i < N; i++ {
			if out, err = exec.Command("./" + bin).CombinedOutput(); err != nil {
				t.Errorf("%s: %s:\n%s", base, err, out)
				r = append(r, &compCertResult{nm, base, 0, 0, true, false, false})
				continue next
			}
		}
		d := time.Since(t0) / time.Duration(N)
		r = append(r, &compCertResult{nm, base, d, 0, true, true, checkResult(t, out, base, rdir)})
	}
	return r
}

func dumpLayout(t cc.Type) string {
	switch t.Kind() {
	case cc.Struct, cc.Union:
		// ok
	default:
		return t.String()
	}

	nf := t.NumField()
	var a []string
	for i := 0; i < nf; i++ {
		f := t.FieldByIndex([]int{i})
		a = append(a, fmt.Sprintf("%2d: %q: BitFieldOffset %v, BitFieldWidth %v, IsBitField %v, Mask: %#0x, off: %v, pad %v",
			i, f.Name(), f.BitFieldOffset(), f.BitFieldWidth(),
			f.IsBitField(), f.Mask(), f.Offset(), f.Padding(),
		))
	}
	return strings.Join(a, "\n")
}
