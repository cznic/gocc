// Copyright 2019 The GOCC Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

// Command gocc implements a C99 compiler (Work in progress).
//
// Installation
//
// To install/update gocc invoke:
//
//     $ go get [-u] modernc.org/gocc
//
// Online documentation
//
// See https://godoc.org/modernc.org/gocc.
//
// Status
//
// This software is slowly nearing alpha status.
//
// Supported platforms
//
// The code is known to work on 64 bit Linux. No other operating systems or
// architectures were tested and should be assumed to not work at all.
//
// Maintainers/testers for other operating systems/architectures are welcome.
package main // import "modernc.org/gocc"
