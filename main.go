// Copyright 2019 The GOCC Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package main // import "modernc.org/gocc"

//TODO https://todo.sr.ht/~mcf/cc-issues/52
//TODO https://todo.sr.ht/~mcf/cc-issues/56
//TODO cdecl

import (
	"bufio"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"path/filepath"
	"runtime"
	"runtime/debug"
	"strings"
	"sync"
	"unsafe"

	"modernc.org/cc/v3"
	"modernc.org/fileutil"
	"modernc.org/opt"
	qbec "modernc.org/qbe"
)

const (
	version = "0.1.0 pre-alpha"

	builtin = `
#define __DI__
#define __FUNCTION__ __func__
#define __HI__
#define __PRETTY_FUNCTION__ __func__
#define __QI__
#define __SI__
#define __builtin_abort abort
#define __builtin_abs abs
#define __builtin_alloca alloca
#define __builtin_bcopy bcopy
#define __builtin_cimag cimag
#define __builtin_cimagf cimagf
#define __builtin_cimagl cimagl
#define __builtin_conj conj
#define __builtin_conjf conjf
#define __builtin_conjl conjl
#define __builtin_constant_p(x) __builtin_constant_p_impl(0, x)
#define __builtin_copysign copysign
#define __builtin_copysignf copysignf
#define __builtin_creal creal
#define __builtin_crealf crealf
#define __builtin_creall creall
#define __builtin_expect(exp, c) exp
#define __builtin_fabs fabs
#define __builtin_ffs ffs
#define __builtin_fprintf fprintf
#define __builtin_fprintf_unlocked fprintf
#define __builtin_fputc fputc
#define __builtin_fputs fputs
#define __builtin_fputs_unlocked fputs
#define __builtin_free free
#define __builtin_fwrite fwrite
#define __builtin_index index
#define __builtin_labs labs
#define __builtin_llabs llabs
#define __builtin_malloc malloc
#define __builtin_memchr memchr
#define __builtin_memcmp memcmp
#define __builtin_memcpy memcpy
#define __builtin_memmove memmove
#define __builtin_mempcpy memcpy
#define __builtin_memset memset
#define __builtin_offsetof(type, member) ((__SIZE_TYPE__)&(((type*)0)->member))
#define __builtin_prefetch(...) (void)(__VA_ARGS__)
#define __builtin_printf printf
#define __builtin_printf_unlocked printf
#define __builtin_putchar putchar
#define __builtin_puts puts
#define __builtin_snprintf snprintf
#define __builtin_sprintf sprintf
#define __builtin_strcat strcat
#define __builtin_strchr strchr
#define __builtin_strcmp strcmp
#define __builtin_strcpy strcpy
#define __builtin_strlen strlen
#define __builtin_strncat strncat
#define __builtin_strncmp strncmp
#define __builtin_trap abort
#define __builtin_unreachable abort
#define __builtin_va_arg(ap, type) (type)__gocc_va_arg(ap)
#define __builtin_va_copy(dst, src) dst = src
#define __builtin_va_end(ap) __gocc_va_end(ap)
#define __builtin_va_start(ap, v) __gocc_va_start(ap)
#define __declspec(...)
#define __extension__
#define __signed__ signed
#define __sync_synchronize(...)
#define __word__
#define asm __asm__
#define fputs_unlocked fputs

#if __SIZEOF_POINTER__ == 8
typedef void* __builtin_va_list[3];
#else
typedef void* __builtin_va_list[1];
typedef long double __float128;
#endif

#ifndef __GNUC__
#define __attribute(x)
#define __attribute__(x)
#define alloca(size)   __gocc_alloca (size)
#endif


#ifdef __PTRDIFF_TYPE__
typedef __PTRDIFF_TYPE__ ptrdiff_t;
#endif

#ifdef __SIZE_TYPE__
typedef __SIZE_TYPE__ size_t;
#endif

#ifdef __WCHAR_TYPE__
typedef __WCHAR_TYPE__ wchar_t;
#endif

#ifdef __UINT16_TYPE__
__UINT16_TYPE__ __builtin_bswap16 (__UINT16_TYPE__ x);
#endif

#ifdef __UINT32_TYPE__
__UINT32_TYPE__ __builtin_bswap32 (__UINT32_TYPE__ x);
#endif

#ifdef __UINT64_TYPE__
__UINT64_TYPE__ __builtin_bswap64 (__UINT64_TYPE__ x);
#endif

#if __SIZEOF_POINTER__ == 4 && !defined(__ILP32__)
#define __ILP32__ 1
#endif

typedef struct { char real, imag; } __COMPLEX_CHAR_TYPE__;
typedef struct { double real, imag; } __COMPLEX_DOUBLE_TYPE__;
typedef struct { float real, imag; } __COMPLEX_FLOAT_TYPE__;
typedef struct { int real, imag; } __COMPLEX_INT_TYPE__;
typedef struct { long double real, imag; } __COMPLEX_LONG_DOUBLE_TYPE__;
typedef struct { long real, imag; } __COMPLEX_LONG_TYPE__;
typedef struct { long long real, imag; } __COMPLEX_LONG_LONG_TYPE__;
typedef struct { long long unsigned real, imag; } __COMPLEX_LONG_LONG_UNSIGNED_TYPE__;
typedef struct { long unsigned real, imag; } __COMPLEX_LONG_UNSIGNED_TYPE__;
typedef struct { short real, imag; } __COMPLEX_SHORT_TYPE__;
typedef struct { unsigned real, imag; } __COMPLEX_UNSIGNED_TYPE__;
typedef struct { unsigned short real, imag; } __COMPLEX_SHORT_UNSIGNED_TYPE__;

int __builtin_clzll (unsigned long long); //TODO-
int __builtin_constant_p_impl(int, ...);
int __printf__ ( const char * format, ... ); //TODO-
int __scanf__ ( const char *format, ... ); //TODO-
void *__gocc_alloca(size_t size);
void *__gocc_va_arg(void* ap);
void *__gocc_va_end(void* ap);
void *__gocc_va_start(void* ap);
`
)

var (
	_ = ioutil.ReadFile

	arch              = 8 * unsafe.Sizeof(uintptr(0))
	hostConfigOnce32  sync.Once
	hostConfigOnce64  sync.Once
	hostPredef32      string
	hostIncludes32    []string
	hostSysIncludes32 []string
	hostPredef64      string
	hostIncludes64    []string
	hostSysIncludes64 []string

	isTesting bool
	traceIL   bool
)

func hostConfig(opts ...string) (hostPredef string, hostIncludes, hostSysIncludes []string, err error) {
	hostPredef, hostIncludes, hostSysIncludes, err = cc.HostConfig("", opts...)
	if err != nil {
		return "", nil, nil, err
	}

	a := strings.Split(hostPredef, "\n")
	w := 0
	for _, v0 := range a {
		v := strings.TrimSpace(strings.ToLower(v0))
		if !strings.HasPrefix(v, "#define __gnu") && !strings.HasPrefix(v, "#define __gcc") {
			a[w] = v0
			w++
		}
	}
	hostPredef = strings.Join(a[:w], "\n")
	return hostPredef, hostIncludes, hostSysIncludes, err
}

func internalError() int                               { return internalErrorf("") }
func internalErrorf(s string, args ...interface{}) int { panic(fmt.Errorf(s, args...)) }

type task struct {
	args []string
	ast  *cc.AST
	cc.Config
	includePaths    []string
	inputs          []int
	intType         cc.Type
	predefined      string
	ptrType         cc.Type
	qbeArgs         []string
	stderr          io.Writer
	stdout          io.Writer
	sysIncludePaths []string
	temps           []string
	testSources     []cc.Source // testing //TODO-
	wr              io.Writer

	opts struct {
		D                []string                 // -D
		I                []string                 // -I
		U                []string                 // -U
		o                string                   // -o
		qbecPkgName      string                   // -qbec-pkgname
		qbecStaticPrefix string                   // -qbec-static-prefix
		qbecVolatile     map[cc.StringID]struct{} // -qbec-volatile
		rpath            string                   // -rpath
		soname           string                   // -soname
		std              string                   // -std

		C                        bool // -C
		E                        bool // -E
		c                        bool // -c
		fPIC                     bool // -fPIC
		fpic                     bool // -fpic
		goccAllocatedDeclarators bool // -gocc-allocated-declarators
		goccEmitDefintions       bool // -gocc-emit-definitions
		goccPedantic             bool // -gocc-pedantic
		goccTrc                  bool
		m32                      bool // -m32
		m64                      bool // -m64
		noWholeArchive           bool // --no-whole-archive
		shared                   bool // -shared
		wholeArchive             bool // --whole-archive
	}

	doNotCache     bool
	doNotCacheMain bool
	genQBE         bool // -o foo.qbe
}

var (
	accept = []string{".c", ".h"}
)

// args are like os.Args, len always > 0.
func newTask(args []string) (t *task, err error) {
	if dmesgs {
		dmesg("newTask %q", args)
	}
	wd, err := os.Getwd()
	if err != nil {
		return nil, err
	}

	t = &task{
		args: args,
	}
	t.Config.PragmaHandler = t.pragmaHandler
	t.Config.WorkingDir = wd

	p := opt.NewSet()
	p.Opt("C", func(opt string) error { t.opts.C = true; return nil })
	p.Opt("E", func(opt string) error { t.opts.E = true; return nil })
	p.Opt("c", func(opt string) error { t.opts.c = true; t.qbeArgs = append(t.qbeArgs, opt); return nil })
	p.Opt("fPIC", func(opt string) error { t.opts.fPIC = true; t.qbeArgs = append(t.qbeArgs, opt); return nil })
	p.Opt("fpic", func(opt string) error { t.opts.fpic = true; t.qbeArgs = append(t.qbeArgs, opt); return nil })
	p.Opt("m32", func(opt string) error { t.opts.m32 = true; t.qbeArgs = append(t.qbeArgs, opt); return nil })
	p.Opt("m64", func(opt string) error { t.opts.m64 = true; t.qbeArgs = append(t.qbeArgs, opt); return nil })
	p.Opt("shared", func(opt string) error { t.opts.shared = true; t.qbeArgs = append(t.qbeArgs, opt); return nil })
	p.Arg("D", true, func(opt, arg string) error {
		t.opts.D = append(t.opts.D, arg)
		t.qbeArgs = append(t.qbeArgs, opt+arg) //TODO-?
		return nil
	})
	p.Arg("qbec-crt", false, func(opt, arg string) error {
		t.qbeArgs = append(t.qbeArgs, opt, arg)
		return nil
	})
	p.Arg("qbec-import", false, func(opt, arg string) error {
		t.qbeArgs = append(t.qbeArgs, opt, arg)
		return nil
	})
	p.Arg("qbec-dot-import", false, func(opt, arg string) error {
		t.qbeArgs = append(t.qbeArgs, opt, arg)
		return nil
	})
	p.Arg("qbec-pkgname", false, func(opt, arg string) error {
		t.opts.qbecPkgName = arg //TODO-
		t.qbeArgs = append(t.qbeArgs, opt, arg)
		return nil
	})
	p.Arg("qbec-static-prefix", false, func(opt, arg string) error {
		t.opts.qbecStaticPrefix = arg //TODO-
		t.qbeArgs = append(t.qbeArgs, opt, arg)
		return nil
	})
	p.Arg("qbec-volatile", false, func(opt, arg string) error {
		a := strings.Split(arg, ",")
		if t.opts.qbecVolatile == nil {
			t.opts.qbecVolatile = map[cc.StringID]struct{}{}
		}
		for _, v := range a {
			t.opts.qbecVolatile[cc.String(strings.TrimSpace(v))] = struct{}{}
		}
		return nil
	})
	p.Arg("I", true, func(opt, arg string) error { t.opts.I = append(t.opts.I, arg); return nil })
	p.Arg("U", true, func(opt, arg string) error {
		t.opts.U = append(t.opts.U, arg)
		t.qbeArgs = append(t.qbeArgs, opt+arg) //TODO-?
		return nil
	})
	p.Arg("o", false, func(opt, arg string) error {
		if t.opts.o != "" {
			return fmt.Errorf("multiple -o options present")
		}

		t.opts.o = arg
		switch filepath.Ext(arg) {
		case ".qbe":
			t.genQBE = true
		default:
			t.qbeArgs = append(t.qbeArgs, opt, arg)
		}
		return nil
	})
	p.Arg("rpath", false, func(opt, arg string) error {
		t.opts.rpath = arg
		t.qbeArgs = append(t.qbeArgs, "-Wl,-rpath", fmt.Sprintf("-Wl,%s", arg))
		return nil
	})
	p.Arg("std", false, func(opt, arg string) error {
		t.opts.std = arg
		t.qbeArgs = append(t.qbeArgs, fmt.Sprintf("-std=%v", arg))
		return nil
	})
	p.Arg("soname", false, func(opt, arg string) error {
		t.opts.soname = arg
		t.qbeArgs = append(t.qbeArgs, "-Wl,-soname", fmt.Sprintf("-Wl,%s", arg))
		return nil
	})
	p.Opt("-whole-archive", func(opt string) error {
		t.opts.wholeArchive = true
		t.qbeArgs = append(t.qbeArgs, "-Wl,--whole-archive")
		return nil
	})
	p.Opt("-no-whole-archive", func(opt string) error {
		t.opts.noWholeArchive = true
		t.qbeArgs = append(t.qbeArgs, "-Wl,--no-whole-archive")
		return nil
	})
	p.Opt("gocc-long-double-is-double", func(opt string) error { t.Config.LongDoubleIsDouble = true; return nil })
	p.Opt("gocc-emit-definitions", func(opt string) error { t.opts.goccEmitDefintions = true; return nil })
	p.Opt("gocc-pedantic", func(opt string) error { t.opts.goccPedantic = true; return nil })
	p.Opt("gocc-allocated-declarators", func(opt string) error { t.opts.goccAllocatedDeclarators = true; return nil })
	p.Opt("gocc-trc", func(opt string) error { t.opts.goccTrc = true; return nil })
	if err := p.Parse(args[1:], func(name string) error {
		t.qbeArgs = append(t.qbeArgs, name)
		if strings.HasPrefix(name, "-") {
			return nil
		}

		ext := filepath.Ext(name)
		for _, a := range accept {
			if ext == a {
				t.inputs = append(t.inputs, len(t.qbeArgs)-1)
				return nil
			}
		}

		return nil
	}); err != nil {
		return nil, err
	}

	if t.opts.c && t.opts.o == "" && len(t.inputs) != 0 {
		base := filepath.Base(t.qbeArgs[t.inputs[0]])
		ext := filepath.Ext(base)
		base = base[:len(base)-len(ext)]
		base += ".o"
		t.qbeArgs = append(t.qbeArgs, "-o", base)
	}

	if s := t.opts.o; filepath.Ext(s) == ".go" && t.opts.qbecPkgName == "" && t.opts.qbecStaticPrefix == "" {
		s = filepath.Base(s)
		s = s[:len(s)-len(".go")]
		s = strings.ReplaceAll(s, "-", "")
		s = strings.ReplaceAll(s, ".", "")
		s = strings.ReplaceAll(s, "_", "")
		if !strings.HasPrefix(s, "s") {
			s = "s" + s
		}
		t.qbeArgs = append(t.qbeArgs, "-qbec-static-prefix", s+"_")
	}

	return t, nil
}

func (t *task) pragmaHandler(p cc.Pragma, toks []cc.Token) {
	if len(toks) == 0 {
		return
	}
	switch s := toks[0].Value.String(); s {
	case "push_macro", "pop_macro":
		toks = toks[1:]
		if len(toks) == 3 && toks[0].Rune == '(' && toks[1].Rune == cc.STRINGLITERAL && toks[2].Rune == ')' {
			nm := toks[1].String()
			if len(nm) < 3 {
				todo(nil) //TODO report error
			}
			nm = nm[1 : len(nm)-1]
			switch s {
			case "push_macro":
				p.PushMacro(nm)
			case "pop_macro":
				p.PopMacro(nm)
			default:
				panic("internal error") //TODOOK
			}
			break
		}

		todo(nil) //TODO report error
	}
}

func (t *task) w(s string, args ...interface{}) {
	//fmt.Printf(s, args...) //TODO-
	if _, err := fmt.Fprintf(t.wr, s, args...); err != nil {
		panic(err) // Handled in main
	}
}

// The "real" main, possibly executed by the server process, if available, ie.
// in a different process than main is executing in.
func (t *task) main(stdout, stderr io.Writer) (exitCode int) {
	if dmesgs {
		dmesg("main entered: %v", os.Args)

		defer func() {
			dmesg("main exiting: %v", exitCode)
		}()
	}
	t.stdout = stdout
	t.stderr = stderr

	var hostPredef string
	var hostIncludes, hostSysIncludes []string
	var err error
	switch {
	case t.opts.m32 && arch != 32:
		hostConfigOnce32.Do(func() { hostPredef32, hostIncludes32, hostSysIncludes32, err = hostConfig("-m32") })
		hostPredef = hostPredef32
		hostIncludes = hostIncludes32
		hostSysIncludes = hostSysIncludes32
	case t.opts.m64 && arch != 64:
		hostConfigOnce32.Do(func() { hostPredef64, hostIncludes64, hostSysIncludes64, err = hostConfig("-m64") })
		hostPredef = hostPredef64
		hostIncludes = hostIncludes64
		hostSysIncludes = hostSysIncludes64
	default:
		hostConfigOnce64.Do(func() { hostPredef64, hostIncludes64, hostSysIncludes64, err = hostConfig() })
		hostPredef = hostPredef64
		hostIncludes = hostIncludes64
		hostSysIncludes = hostSysIncludes64
	}
	if err != nil {
		fmt.Fprintln(stderr, err)
		return 1
	}

	// https://pubs.opengroup.org/onlinepubs/9699919799/utilities/c99.html
	//
	// Headers whose names are enclosed in double-quotes ( "" ) shall be
	// searched for first in the directory of the file with the #include
	// line, then in directories named in -I options, and last in the usual
	// places
	t.includePaths = append([]string{"@"}, t.opts.I...)
	t.includePaths = append(t.includePaths, hostIncludes...)
	t.includePaths = append(t.includePaths, hostSysIncludes...)
	t.includePaths = append(t.includePaths, filepath.FromSlash("/usr/include")) //TODO nix only
	// For headers whose names are enclosed in angle brackets ( "<>" ), the
	// header shall be searched for only in directories named in -I options
	// and then in the usual places.
	t.sysIncludePaths = append(t.opts.I, hostSysIncludes...)

	// if dmesgs {
	// 	dmesg("includePaths:\n%v", strings.Join(t.includePaths, "\n"))
	// 	dmesg("sysIncludePaths:\n%v", strings.Join(t.sysIncludePaths, "\n"))
	// }

	t.predefined = hostPredef
	arch := runtime.GOARCH
	switch {
	case t.opts.m32:
		switch arch {
		case "386":
			// nop
		case "amd64":
			arch = "386"
		default:
			fmt.Fprintf(stderr, "-m32 not supported on arch %s", arch)
			return 1
		}
	}

	t.Config.ABI, err = cc.NewABI(runtime.GOOS, arch)
	if err != nil {
		fmt.Fprintln(stderr, err)
		return 1
	}

	var sources []cc.Source
	if t.predefined != "" {
		sources = append(sources, cc.Source{Name: "<predefined>", Value: t.predefined})
	}
	sources = append(sources, cc.Source{Name: "<built-in>", Value: builtin})
	sources = append(sources, t.testSources...)

	if len(t.opts.D) != 0 {
		var a []string
		for _, v := range t.opts.D {
			if i := strings.IndexByte(v, '='); i > 0 {
				a = append(a, fmt.Sprintf("#define %s %s", v[:i], v[i+1:]))
				continue
			}

			a = append(a, fmt.Sprintf("#define %s 1", v))
		}
		a = append(a, "\n")
		sources = append(sources, cc.Source{Name: "<defines>", Value: strings.Join(a, "\n")})
	}

	if len(t.opts.U) != 0 {
		var a []string
		for _, v := range t.opts.U {
			a = append(a, fmt.Sprintf("#undef %s", v))
		}
		a = append(a, "\n")
		sources = append(sources, cc.Source{Name: "<undefines>", Value: strings.Join(a, "\n")})
	}

	if t.opts.E {
		t.Config.PreprocessOnly = true
		if t.opts.C {
			t.Config.PreserveWhiteSpace = true
		}
		for _, i := range t.inputs {
			sources = append(sources, cc.Source{Name: t.qbeArgs[i]})
		}
		w := bufio.NewWriter(stdout)

		defer w.Flush()

		if err := cc.Preprocess(&t.Config, t.includePaths, t.sysIncludePaths, sources, w); err != nil {
			fmt.Fprintln(t.stdout, err)
			return 1
		}

		return 0
	}

	defer func() {
		for _, v := range t.temps {
			os.Remove(v)
		}
	}()

	var f *os.File
	var w *bufio.Writer
	for _, i := range t.inputs {
		nm := t.qbeArgs[i]
		switch {
		case t.genQBE:
			if w == nil {
				var err error
				if f, err = os.Create(t.opts.o); err != nil {
					fmt.Fprintln(stderr, err)
					return 1
				}

				w = bufio.NewWriter(f)

				defer func() {
					if err := w.Flush(); err != nil {
						fmt.Fprintln(stderr, err)
						exitCode = 1
					}
					if err := f.Close(); err != nil {
						fmt.Fprintln(stderr, err)
						exitCode = 1
					}
				}()
			}

			if err := t.compile1(sources, nm, w); err != nil {
				fmt.Fprintln(stderr, err)
				return 1
			}
		default:
			temp, err := fileutil.TempFile("", "gocc-", ".qbe")
			if err != nil {
				fmt.Fprintf(stderr, "cannot create temp file: %v\n", err)
				return 1
			}

			t.temps = append(t.temps, temp.Name())
			w := bufio.NewWriter(temp)

			if err := t.compile1(sources, nm, w); err != nil {
				fmt.Fprintln(stderr, err)
				return 1
			}

			t.qbeArgs[i] = temp.Name()
			if err := w.Flush(); err != nil {
				fmt.Fprintln(stderr, err)
				return 1
			}

			if err := temp.Close(); err != nil {
				fmt.Fprintln(stderr, err)
				return 1
			}
		}
	}
	if t.genQBE {
		return 0
	}

	return qbec.Compile(append([]string{"qbec"}, t.qbeArgs...), t.stdout, t.stderr, nil)
}

func (t *task) compile1(sources []cc.Source, fn string, w io.Writer) (err error) {
	doNotCache := t.doNotCacheMain && filepath.Base(fn) == "main.c" || t.doNotCache
	sources = append(sources, cc.Source{Name: fn, DoNotCache: doNotCache})
	if t.opts.goccPedantic {
		t.Config.RejectCaseRange = true
		t.Config.RejectElseExtraTokens = true
		t.Config.RejectEmptyCompositeLiterals = true
		t.Config.RejectEmptyDeclarations = true
		t.Config.RejectEmptyInitializerList = true
		t.Config.RejectEmptyStructs = true
		t.Config.RejectEndifExtraTokens = true
		t.Config.RejectFinalBackslash = true
		t.Config.RejectIfdefExtraTokens = true
		t.Config.RejectIfndefExtraTokens = true
		t.Config.RejectInvalidVariadicMacros = true
		t.Config.RejectLabelValues = true
		t.Config.RejectLateBinding = true
		t.Config.RejectLineExtraTokens = true
		t.Config.RejectMissingConditionalExpr = true
		t.Config.RejectMissingDeclarationSpecifiers = true
		t.Config.RejectMissingFinalNewline = true
		t.Config.RejectMissingFinalStructFieldSemicolon = true
		t.Config.RejectNestedFunctionDefinitions = true
		t.Config.RejectParamSemicolon = true
		t.Config.RejectStatementExpressions = true
		t.Config.RejectTypeof = true
		t.Config.RejectUndefExtraTokens = true
	}
	if t.opts.goccTrc {
		t.Config.InjectTracingCode = true
	}
	if t.ast, err = cc.Translate(&t.Config, t.includePaths, t.sysIncludePaths, sources); err != nil {
		return err
	}

	t.intType = t.Config.ABI.Type(cc.Int)
	t.ptrType = t.Config.ABI.Type(cc.Ptr)

	// if dmesgs {
	// 	dmesg("%v sources for %s", len(sources), fn)
	// 	for i, v := range sources {
	// 		dmesg("#%v: Name %s:", i+1, v.Name)
	// 		switch {
	// 		case v.Value != "":
	// 			dmesg("Value:\n%s", v.Value)
	// 		default:
	// 			switch filepath.Ext(v.Name) {
	// 			case ".c", ".h":
	// 				b, err := ioutil.ReadFile(v.Name)
	// 				switch {
	// 				case err != nil:
	// 					dmesg("error: %s", err)
	// 				default:
	// 					dmesg("Value:\n%s", b)
	// 				}
	// 			}
	// 		}
	// 	}
	// }

	t.wr = w
	q := newQBE(t)

	defer func() {
		if e := recover(); e != nil && err == nil {
			err = fmt.Errorf("%v\n%s", e, debug.Stack())
		}
	}()

	q.gen()

	if len(q.externalAsm) != 0 && err == nil {
		err = fmt.Errorf("%v: assembler in C not supported", q.pos(q.externalAsm[0]))
	}
	return err
}

func main() {
	if dmesgs {
		dmesg("main entered %v", os.Args)
	}
	t, err := newTask(os.Args)
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		if dmesgs {
			dmesg("%v:\nmain exiting: 1", err)
		}
		os.Exit(1)
	}

	rc := t.main(os.Stdout, os.Stderr)
	if dmesgs {
		dmesg("main exiting: %v", rc)
	}
	os.Exit(rc)
}
