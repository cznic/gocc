// Copyright 2019 The GOCC Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package main // import "modernc.org/gocc"

import (
	"fmt"
	"math"

	"modernc.org/cc/v3"
)

var (
	_ initializer = (*computedInitializer)(nil)
	_ initializer = (*constInitializer)(nil)
)

type initializer interface {
	initializerArithmetic(t cc.Type, n *cc.AssignmentExpression, off uintptr)
	initializerArrayString(t cc.Type, n cc.Node, id cc.StringID, off uintptr)
	initializerArrayWideString(t cc.Type, n cc.Node, id cc.StringID, off uintptr)
	initializerPointer(t cc.Type, n *cc.AssignmentExpression, off uintptr)
	initializerStruct(t cc.Type, n *cc.AssignmentExpression, off uintptr)
}

type computedInitializer struct {
	dst int
	q   *qbe
}

func newComputedInitializer(q *qbe, n cc.Node, dst int, t cc.Type) *computedInitializer {
	//TODO Is the zeroing actually needed?
	q.w("\tcall $memset(%s %%t%d, w 0, %s %d)\t\t# %v:\n", q.ptr, dst, q.ptr, t.Size(), q.pos(n)) //TODO 2nd q.ptr -> q.typ(q.sizeT)
	return &computedInitializer{q: q, dst: dst}
}

func (c *computedInitializer) initializerArithmetic(t cc.Type, n *cc.AssignmentExpression, off uintptr) {
	if n.Operand.IsZero() && n.Operand.Type().IsIntegerType() {
		return
	}

	e := c.q.assignmentExpression(n, false)
	e = c.q.convert(n, e, n.Operand.Type(), t, nil)
	p := c.q.t("add %%t%[2]d, %d\t\t# %v:", c.q.ptr, c.dst, off, c.q.pos(n))
	c.q.store(n, p, e, nil, t, true)
}

func (c *computedInitializer) initializerArrayString(t cc.Type, n cc.Node, sid cc.StringID, off uintptr) {
	s := sid.String()
	if uintptr(len(s)) > t.Len() {
		todo(n)
	}

	sz := t.Elem().Size()
	p := c.q.t("copy %%t%[2]d\t\t# %v: %s", c.q.ptr, c.dst, c.q.pos(n), c.q.strComment(sid))
	for i := 0; i < len(s); i++ {
		r := c.q.t("copy %[2]d", c.q.typ(n, t.Elem()), s[i])
		c.q.store(n, p, r, nil, t.Elem(), true)
		p = c.q.t("add %%t%[2]d, %d", c.q.ptr, p, sz)
	}
}

func (c *computedInitializer) initializerArrayWideString(t cc.Type, n cc.Node, sid cc.StringID, off uintptr) {
	s := []rune(sid.String())
	if uintptr(len(s)) > t.Len() {
		todo(n)
	}

	sz := t.Elem().Size()
	p := c.q.t("copy %%t%[2]d\t\t# %v: %s", c.q.ptr, c.dst, c.q.pos(n), c.q.strComment(sid))
	for _, r := range s {
		r := c.q.t("copy %[2]d", c.q.typ(n, t.Elem()), r)
		c.q.store(n, p, r, nil, t.Elem(), true)
		p = c.q.t("add %%t%[2]d, %d", c.q.ptr, p, sz)
	}
}

func (c *computedInitializer) initializerPointer(t cc.Type, n *cc.AssignmentExpression, off uintptr) {
	if n.Operand.IsZero() {
		return
	}

	switch x := n.Operand.Value().(type) {
	case cc.StringValue:
		switch t.Elem().Kind() {
		case cc.Char, cc.SChar, cc.UChar:
			sid := cc.StringID(x)
			//TODO- e := c.q.t("copy %[2]s\t\t# %v: %s", c.q.ptr, fmt.Sprintf(`$.%d + %d`, c.q.string(sid), n.Operand.Offset()), c.q.pos(n), c.q.strComment(sid))
			e := c.q.t("copy %[2]s\t\t# %v: %s", c.q.ptr, fmt.Sprintf(`$.%d`, c.q.string(sid)), c.q.pos(n), c.q.strComment(sid))
			if o := n.Operand.Offset(); o != 0 {
				e = c.q.t("add %%t%[2]d, %d", c.q.ptr, e, o)
			}
			p := c.q.t("add %%t%[2]d, %d\t\t# %v:", c.q.ptr, c.dst, off, c.q.pos(n))
			c.q.store(n, p, e, nil, t, true)
		default:
			todo(n, t)
		}
	case nil:
		var e int
		e = c.q.assignmentExpression(n, false)
		p := c.q.t("add %%t%[2]d, %d\t\t# %v:", c.q.ptr, c.dst, off, c.q.pos(n))
		c.q.store(n, p, e, nil, t, true)
	case cc.Int64Value, cc.Uint64Value:
		c.initializerArithmetic(t, n, off)
	default:
		todo(n, t, n.Operand.Value())
	}
}

func (c *computedInitializer) initializerStruct(t cc.Type, n *cc.AssignmentExpression, off uintptr) {
	switch n.Operand.Type().Kind() {
	case cc.Struct, cc.Union:
		rhs := c.q.assignmentExpressionPtr(n)
		lhs := c.q.t("add %%t%[2]d, %d\t\t# %v:", c.q.ptr, c.dst, off, c.q.pos(n))
		c.q.w("\tcall $memcpy(%s %%t%d, %s %%t%d, %s %d)\t\t# %v:\n", c.q.ptr, lhs, c.q.ptr, rhs, c.q.ptr, t.Size(), c.q.pos(n))
	default:
		todo(n)
	}
}

type constInitializer struct {
	b   []byte
	ld  map[uintptr]string
	ptr map[uintptr]string
	q   *qbe
}

func newConstInitializer(q *qbe, t cc.Type) *constInitializer {
	return &constInitializer{q: q, b: make([]byte, t.Size())}
}

func (c *constInitializer) initializerArithmetic(t cc.Type, n *cc.AssignmentExpression, off uintptr) {
	//TODO remove type switches
	if n.Operand.IsZero() && n.Operand.Type().IsIntegerType() {
		return
	}

	if t.IsIntegerType() {
		var v uint64
		switch x := n.Operand.Value().(type) {
		case cc.Int64Value:
			v = uint64(x)
		case cc.Uint64Value:
			v = uint64(x)
		case cc.Float64Value:
			v = uint64(int64(x))
		default:
			todo(n, t, off, n.Operand.Type(), n.Operand.Value(), fmt.Sprintf("%T", x))
		}
		c.initializerUint64(t, n, v, off)
		return
	}

	switch t.Kind() {
	case cc.Float:
		switch x := n.Operand.Value().(type) {
		case cc.Float64Value:
			c.initializerUint64(t, n, uint64(math.Float32bits(float32(x))), off)
		case cc.Float32Value:
			c.initializerUint64(t, n, uint64(math.Float32bits(float32(x))), off)
		case cc.Int64Value:
			c.initializerUint64(t, n, uint64(math.Float32bits(float32(x))), off)
		case cc.Uint64Value:
			c.initializerUint64(t, n, uint64(math.Float32bits(float32(x))), off)
		default:
			todo(n, t, off, n.Operand.Type(), n.Operand.Value())
		}
	case cc.Double:
		switch x := n.Operand.Value().(type) {
		case cc.Float64Value:
			c.initializerUint64(t, n, math.Float64bits(float64(x)), off)
		case cc.Float32Value:
			c.initializerUint64(t, n, math.Float64bits(float64(x)), off)
		case cc.Int64Value:
			c.initializerUint64(t, n, math.Float64bits(float64(x)), off)
		case cc.Uint64Value:
			c.initializerUint64(t, n, math.Float64bits(float64(x)), off)
		default:
			todo(n, t, off, n.Operand.Type(), n.Operand.Value())
		}
	case cc.LongDouble:
		c.setLD(off, n.Operand.Value())
	case cc.ComplexDouble:
		switch x := n.Operand.Value().(type) {
		case cc.Complex128Value:
			c.initializerUint64(t.Real().Type(), n, math.Float64bits(real(x)), off+t.Real().Offset())
			c.initializerUint64(t.Imag().Type(), n, math.Float64bits(imag(x)), off+t.Imag().Offset())
		default:
			todo(n, t, off, n.Operand.Type(), n.Operand.Value())
		}
	case cc.ComplexLongDouble:
		v := n.InitializerOperand.Value().(cc.Complex256Value)
		c.setLD(off, v.Re)
		c.setLD(off+c.q.Config.ABI.Types[cc.LongDouble].Size, v.Im)
	case cc.ComplexFloat:
		switch x := n.Operand.Value().(type) {
		case cc.Complex64Value:
			c.initializerUint64(t.Real().Type(), n, uint64(math.Float32bits(real(x))), off+t.Real().Offset())
			c.initializerUint64(t.Imag().Type(), n, uint64(math.Float32bits(imag(x))), off+t.Imag().Offset())
		case cc.Complex128Value:
			c.initializerUint64(t.Real().Type(), n, uint64(math.Float32bits(float32(real(x)))), off+t.Real().Offset())
			c.initializerUint64(t.Imag().Type(), n, uint64(math.Float32bits(float32(imag(x)))), off+t.Imag().Offset())
		default:
			todo(n, t, off, n.Operand.Type(), n.Operand.Value(), fmt.Sprintf("%T", x))
		}
	default:
		todo(n, t, t.Kind())
	}
}

func (c *constInitializer) setLD(off uintptr, v cc.Value) {
	switch x := v.(type) {
	case cc.Int64Value, cc.Uint64Value:
		c.setLDStr(off, fmt.Sprintf("ld_%d", x))
	case cc.Float64Value:
		switch v := float64(x); {
		case math.IsNaN(v):
			c.setLDStr(off, "ld_nan")
		case math.IsInf(v, 1):
			c.setLDStr(off, "ld_inf")
		case math.IsInf(v, -1):
			c.setLDStr(off, "ld_-inf")
		default:
			c.setLDStr(off, fmt.Sprintf("ld_%g", v))
		}
	case *cc.Float128Value:
		c.setLDStr(off, ldStr(x))
	default:
		todo(nil, fmt.Sprintf("%T", x))
	}
}

func ldStr(v *cc.Float128Value) string {
	switch {
	case v.NaN:
		return "ld_nan"
	case v.N.IsInf():
		switch {
		case v.N.Sign() > 0:
			return "ld_inf"
		default:
			return "ld_-inf"
		}
	default:
		return "ld_" + v.N.Text('e', -1)
	}
}

func (c *constInitializer) setLDStr(off uintptr, s string) {
	if c.ld == nil {
		c.ld = map[uintptr]string{}
	}
	c.ld[off] = s
	for i := uintptr(0); i < c.q.Config.ABI.Types[cc.LongDouble].Size; i++ {
		c.b[off] = 255
		off++
	}
}

func (c *constInitializer) initializerArrayString(t cc.Type, n cc.Node, sid cc.StringID, off uintptr) {
	s := sid.String()
	if t.IsIntegerType() || uintptr(len(s)) > t.Len() {
		later(n, "flexible array member (?)")
	}

	for i := 0; i < len(s); i++ {
		c.initializerUint64(t.Elem(), n, uint64(s[i]), off+uintptr(i))
	}
}

func (c *constInitializer) initializerArrayWideString(t cc.Type, n cc.Node, sid cc.StringID, off uintptr) {
	s := []rune(sid.String())
	if uintptr(len(s)) > t.Len() {
		todo(n)
	}

	sz := t.Elem().Size()
	for i, r := range s {
		c.initializerUint64(t.Elem(), n, uint64(r), off+uintptr(i)*sz)
	}
}

func (c *constInitializer) initializerStruct(t cc.Type, n *cc.AssignmentExpression, off uintptr) {
	switch n.Operand.Type().Kind() {
	case cc.Struct, cc.Union:
		switch x := n.Operand.Value().(type) {
		case *cc.InitializerValue:
			switch x.Type().Kind() {
			case cc.Struct, cc.Union:
				c.q.initializer(c, x.List(), off)
			default:
				todo(n, t, x.Type(), x.IsConst())
			}
		default:
			panic(internalErrorf("%v: %q, %T", n.Position(), t, x))
		}
	default:
		todo(n, t, n.Operand.Type())
	}
}

func (c *constInitializer) initializerUint64(t cc.Type, n cc.Node, v uint64, off uintptr) {
	if v == 0 {
		return
	}

	sz := t.Size()
	if t.IsBitFieldType() {
		f := t.BitField()
		if l := uintptr(len(c.b)); off+sz > l { //TODO copy this to run time behavior of initializers
			sz = l - off
		}
		var raw uint64
		switch sz {
		case 1:
			raw = uint64(c.b[off])
		case 2:
			raw = uint64(c.q.Config.ABI.ByteOrder.Uint16(c.b[off:]))
		case 4:
			raw = uint64(c.q.Config.ABI.ByteOrder.Uint32(c.b[off:]))
		case 8:
			raw = c.q.Config.ABI.ByteOrder.Uint64(c.b[off:])
		default:
			todo(n, t, off, t.Size(), v)
		}
		raw &^= f.Mask()
		v <<= uint(f.BitFieldOffset())
		v &= f.Mask()
		v |= raw
	}

	switch sz {
	case 1:
		c.b[off] = byte(v)
	case 2:
		c.q.Config.ABI.ByteOrder.PutUint16(c.b[off:], uint16(v))
	case 4:
		c.q.Config.ABI.ByteOrder.PutUint32(c.b[off:], uint32(v))
	case 8:
		c.q.Config.ABI.ByteOrder.PutUint64(c.b[off:], v)
	default:
		todo(n, t, off, t.Size(), v)
	}
}

func (c *constInitializer) setPtr(off uintptr, nm string) {
	if c.ptr == nil {
		c.ptr = map[uintptr]string{}
	}
	c.ptr[off] = nm
	switch c.q.Config.ABI.Types[cc.Ptr].Size {
	case 4:
		c.q.Config.ABI.ByteOrder.PutUint32(c.b[off:], math.MaxUint32)
	case 8:
		c.q.Config.ABI.ByteOrder.PutUint64(c.b[off:], math.MaxUint64)
	default:
		panic(internalError())
	}
}

func (c *constInitializer) initializerPointer(t cc.Type, n *cc.AssignmentExpression, off uintptr) {
	if n.Operand.IsZero() {
		return
	}

	switch x := n.Operand.Value().(type) {
	case cc.StringValue:
		switch t.Elem().Kind() {
		case cc.Char, cc.SChar, cc.UChar, cc.Void:
			c.setPtr(off, fmt.Sprintf(`$.%d + %d`, c.q.string(cc.StringID(x)), n.Operand.Offset()))
		default:
			todo(n, t)
		}
	case cc.WideStringValue:
		switch t.Elem().Kind() {
		case cc.Int:
			c.setPtr(off, fmt.Sprintf(`$.%d + %d`, c.q.string(-cc.StringID(x)), n.Operand.Offset()))
		default:
			todo(n, t)
		}
	case cc.Int64Value:
		c.initializerUint64(t, n, uint64(x), off)
	case cc.Uint64Value:
		c.initializerUint64(t, n, uint64(x), off)
	case nil:
		switch d := n.Operand.Declarator(); {
		case d != nil:
			c.setPtr(off, fmt.Sprintf("%s + %d", c.q.declName(n, d), n.Operand.Offset()))
		default:
			todo(n)
		}
	case *cc.InitializerValue:
		switch x.Type().Kind() {
		case cc.Ptr:
			a := c.q.anonymousInitializer(nil, t.Elem(), n, x.List())
			c.setPtr(off, fmt.Sprintf("$.%d + %d", a, n.Operand.Offset()))
		case cc.Array:
			a := c.q.anonymousInitializer(nil, x.Type(), n, x.List())
			c.setPtr(off, fmt.Sprintf("$.%d + %d", a, n.Operand.Offset()))
		default:
			todo(n, t, x.Type(), x.IsConst(), off)
		}
	default:
		todo(n, t, n.Operand.Value(), fmt.Sprintf("%T", x))
	}
}

func (c *constInitializer) emit(q *qbe) {
	sz := len(c.b)
	for n := sz; n > 0; n-- {
		if c.b[n-1] != 0 {
			break
		}

		c.b = c.b[:n-1]
	}
	switch {
	case len(c.b) == 0:
		q.w(" z %d ", sz)
	default:
		typ := ""
		for off := uintptr(0); off < uintptr(len(c.b)); {
			if nm, ok := c.ptr[off]; ok {
				if typ != "" {
					q.w(",")
				}
				q.w(` %s %s`, q.ptr, nm)
				typ = q.ptr
				off += c.q.Config.ABI.Types[cc.Ptr].Size
				continue
			}

			if s, ok := c.ld[off]; ok {
				if typ != "" {
					q.w(",")
				}
				q.w(` ld %s`, s)
				typ = "ld"
				off += c.q.Config.ABI.Types[cc.LongDouble].Size
				continue
			}

			v := c.b[off]
			switch typ {
			case "l", "w", "ld":
				q.w(", b")
				typ = "b"
			case "":
				q.w("b")
				typ = "b"
			}
			q.w(" %d", v)
			off++
		}
		if len(c.b) != sz {
			q.w(", z %d ", sz-len(c.b))
		}
	}
}

func (q *qbe) initializer(w initializer, list []*cc.Initializer, off0 uintptr) {
	for _, n := range list {
		t := n.Type()
		off := off0 + n.Offset
		if t.IsArithmeticType() {
			w.initializerArithmetic(t, n.AssignmentExpression, off)
			continue
		}

		switch t.Kind() {
		case cc.Ptr:
			w.initializerPointer(t, n.AssignmentExpression, off)
		case cc.Array:
			switch e := t.Elem(); e.Kind() {
			case cc.Char, cc.SChar, cc.UChar:
				w.initializerArrayString(t, n, cc.StringID(n.AssignmentExpression.Operand.Value().(cc.StringValue)), off)
			case q.wchar.Kind():
				w.initializerArrayWideString(t, n, cc.StringID(n.AssignmentExpression.Operand.Value().(cc.WideStringValue)), off)
			default:
				todo(n, e, e.Alias(), e.Kind(), off)
			}
		case cc.Struct, cc.Union:
			w.initializerStruct(t, n.AssignmentExpression, off)
		default:
			todo(n, t, t.Alias(), t.Kind(), off)
		}
	}
}
