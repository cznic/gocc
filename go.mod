module modernc.org/gocc

go 1.16

require (
	github.com/dustin/go-humanize v1.0.0
	github.com/google/go-cmp v0.5.5
	modernc.org/cc/v3 v3.32.2
	modernc.org/fileutil v1.0.0
	modernc.org/opt v0.1.1
	modernc.org/qbe v0.0.1
)
