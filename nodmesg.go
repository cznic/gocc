// Copyright 2019 The GOCC Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

// +build !gocc.dmesg

package main // import "modernc.org/gocc"

const dmesgs = false

func dmesg(s string, args ...interface{}) {}
