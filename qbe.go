// Copyright 2019 The GOCC Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

//TODO statement: handle complex types

/*TODO

avoid local ptr copy of a in:

int *f(int a[]) {
	a += 2;
	return a;
}

export function l $"f"(l %v01) { # main.c:7:5:
@.1
	%v1 =l alloc8 8		# main.c:7:12: a pointer to int
	%t1 =l copy %v01		# main.c:7:12: a		# qbe.go:350
	storel %t1, %v1
	...

*/

package main // import "modernc.org/gocc"

import (
	"fmt"
	"math"
	"os"
	"path/filepath"
	"runtime"
	"runtime/debug"
	"sort"
	"strings"

	"modernc.org/cc/v3"
)

type todoPos struct {
	file string
	line int
}

var (
	oQuiet bool
	oTODO  bool
	todos  map[todoPos]int
)

func report(n cc.Node, args ...interface{}) { //TODO-
	pc, f, l, _ := runtime.Caller(1)
	fn := runtime.FuncForPC(pc)
	var s string
	if n != nil {
		s = n.Position().String()
	}
	if !oQuiet {
		fmt.Printf("\n%s:%d: # <----\n%v:\n\n# REPORT %s: %s", f, l, s, fn.Name(), fmt.Sprintln(args...))
	}
	switch {
	case oTODO:
		os.Stdout.Sync()
		os.Exit(1)
	default:
		panic(fmt.Errorf("\n%s:%d: # <----\n%v:\n\n# REPORT %s: %s", f, l, s, fn.Name(), fmt.Sprintln(args...)))
	}
}

func later(n cc.Node, args ...interface{}) { //TODO-
	pc, f, l, _ := runtime.Caller(1)
	fn := runtime.FuncForPC(pc)
	var s string
	if n != nil {
		s = n.Position().String()
	}
	if !oQuiet {
		fmt.Printf("\n%s:%d: # <----\n%v:\n\n# LATER %s: %s", f, l, s, fn.Name(), fmt.Sprintln(args...))
	}
	switch {
	case oTODO:
		os.Stdout.Sync()
		os.Exit(1)
	default:
		panic(fmt.Errorf("\n%s:%d: # <----\n%v:\n\n# LATER %s: %s", f, l, s, fn.Name(), fmt.Sprintln(args...)))
	}
}

func todo(n cc.Node, args ...interface{}) { //TODO-
	pc, f, l, _ := runtime.Caller(1)
	fn := runtime.FuncForPC(pc)
	var s string
	if n != nil {
		s = n.Position().String()
	}
	if !oQuiet {
		fmt.Printf("\n%s:%d: # <----\n%v:\n\n# TODO %s: %s", f, l, s, fn.Name(), fmt.Sprintln(args...))
	}
	switch {
	case oTODO:
		os.Stdout.Sync()
		os.Exit(1)
	default:
		if todos == nil {
			todos = map[todoPos]int{}
		}
		todos[todoPos{f, l}]++
		panic(fmt.Errorf("\n%s:%d: # <----\n%v:\n\n# TODO %s: %s", f, l, s, fn.Name(), fmt.Sprintln(args...)))
	}
}

type nestedFunction struct {
	name int
	n    *cc.FunctionDefinition
}

type queuedInitializer struct {
	comment     string
	initializer *constInitializer
	name        int
	typ         cc.Type

	d *cc.Declarator
}

type structInfo struct {
	ordinal int
	t       cc.Type
}

type qbe struct {
	*task
	anons               int
	breakLabel          int
	callSiteComplexExpr map[*cc.AssignmentExpression]int
	cases               map[*cc.LabeledStatement]int
	compositeLiterals   map[*cc.PostfixExpression]int
	continueLabel       int
	declaredLabels      map[int]struct{}
	escaped             map[*cc.Declarator]bool
	externalAsm         []*cc.Asm
	fd                  *cc.FunctionDefinition
	fn                  cc.Type
	initializerQueue    []queuedInitializer
	labels              int
	localStatic         map[*cc.Declarator]int
	localVars           map[*cc.Declarator]int
	nestedFunctions     []nestedFunction
	values              map[*cc.Declarator]bool
	prototypes          map[cc.StringID]struct{}
	ptr                 string
	quoted              []byte
	renames             map[*cc.Declarator]int
	returnComplexExpr   map[*cc.Expression]int
	scopeVLAs           []*cc.Declarator
	stringQueue         []cc.StringID
	strings             map[cc.StringID]int
	structs             map[string]*structInfo
	tempVars            int
	uintptr             cc.Type
	vlas                map[*cc.Declarator]int
	wchar               cc.Type
}

func newQBE(t *task) *qbe {
	r := &qbe{
		callSiteComplexExpr: map[*cc.AssignmentExpression]int{},
		compositeLiterals:   map[*cc.PostfixExpression]int{},
		declaredLabels:      map[int]struct{}{},
		escaped:             map[*cc.Declarator]bool{},
		localStatic:         map[*cc.Declarator]int{},
		localVars:           map[*cc.Declarator]int{},
		values:              map[*cc.Declarator]bool{},
		prototypes:          map[cc.StringID]struct{}{},
		renames:             map[*cc.Declarator]int{},
		returnComplexExpr:   map[*cc.Expression]int{},
		strings:             map[cc.StringID]int{},
		task:                t,
		vlas:                map[*cc.Declarator]int{},
		wchar:               t.ast.WideCharType,
	}
	r.ptr = r.typ(nil, t.ptrType)
	sz := t.ptrType.Size()
	for _, v := range []cc.Kind{cc.UInt, cc.ULong, cc.ULongLong} {
		t := t.ABI.Type(v)
		if t.Size() == sz {
			r.uintptr = t
			break
		}
	}
	if r.uintptr == nil {
		panic(internalError())
	}
	return r
}

func (q *qbe) t(s string, args ...interface{}) int {
	if strings.HasSuffix(s, "\n") { //TODO-
		panic("TODO")
	}

	r := q.temp()
	q.w("\t%%t%d =%s ", r, args[0])
	q.w(s, args...)
	_, f, l, _ := runtime.Caller(1)
	q.w("\t\t# %s:%d", filepath.Base(f), l)
	q.w("\n")
	return r
}

func (q *qbe) tn(s string, n int, args ...interface{}) int {
	if strings.HasSuffix(s, "\n") { //TODO-
		panic("TODO")
	}
	q.w("\t%%t%d =%s ", n, args[0])
	q.w(s, args...)
	q.w("\n")
	return n
}

func (q *qbe) gen() {
	q.w("# %s\n\n", strings.Join(q.args, " "))
	if q.opts.goccEmitDefintions {
		var ids []cc.StringID
		for k := range q.ast.Macros {
			ids = append(ids, k)
		}
		sort.Slice(ids, func(i, j int) bool { return ids[i].String() < ids[j].String() })
		for _, v := range ids {
			m := q.ast.Macros[v]
			if m.IsFnLike() {
				continue
			}

			q.w("#qbe:define %v ", v)
			op, err := q.ast.Eval(m)
			switch {
			case err != nil:
				var a []string
				for _, v := range m.ReplacementTokens() {
					a = append(a, v.String())
				}
				q.w("%s", strings.Join(a, " "))
			default:
				switch x := op.Value().(type) {
				case cc.Int64Value:
					switch {
					case op.Type().IsSignedType():
						q.w("%d", x)
					default:
						q.w("%du", uint64(x))
					}
				default:
					panic(internalErrorf("%T", x))
				}
			}
			q.w("\n")
		}
		ids = ids[:0]
		for k := range q.ast.Enums {
			ids = append(ids, k)
		}
		sort.Slice(ids, func(i, j int) bool { return ids[i].String() < ids[j].String() })
		for _, v := range ids {
			op := q.ast.Enums[v]
			q.w("#qbe:enumconst %v ", v)
			switch x := op.Value().(type) {
			case cc.Int64Value:
				switch {
				case op.Type().IsSignedType():
					q.w("%d", x)
				default:
					q.w("%du", uint64(x))
				}
			default:
				panic(internalErrorf("%T", x))
			}
			q.w("\n")
		}
		ids = ids[:0]
		for k := range q.ast.StructTypes {
			ids = append(ids, k)
		}
		sort.Slice(ids, func(i, j int) bool { return ids[i].String() < ids[j].String() })
		for _, v := range ids {
			q.defineStructType(v)
		}
	}
	for n := q.task.ast.TranslationUnit; n != nil; n = n.TranslationUnit {
		q.externalDeclaration(n.ExternalDeclaration)
	}
	var a []string
	for k := range q.structs {
		a = append(a, k)
	}
	sort.Strings(a)
	if len(a) != 0 {
		q.w("\n")
	}
	for _, v := range a {
		si := q.structs[v]
		q.typeDefinition(si.t, si.ordinal)
	}
	if len(a) != 0 {
		q.w("\n")
	}
	q.w("\n# %s\n", strings.Join(q.args, " "))
}

func (q *qbe) defineStructType(nm cc.StringID) {
	t := q.ast.StructTypes[nm]
	q.w("#qbe:")
	switch {
	case t.Kind() == cc.Union:
		q.w("union")
	default:
		q.w("struct")
	}
	q.w(" %s ", nm)
	q.structType(t)
	q.w("\n")
}

func (q *qbe) structType(t cc.Type) {
	if t.Kind() == cc.Union {
		q.w("[%d]byte", t.Size())
		return
	}

	q.w("struct{")
	nf := t.NumField()
	var x [1]int
	for i := 0; i < nf; i++ {
		x[0] = i
		f := t.FieldByIndex(x[:])
		switch s := f.Name(); s {
		case 0:
			q.w("F%d ", f.Offset())
		default:
			q.w("F%s ", s)
		}
		switch {
		case f.BitFieldWidth() != 0:
			q.w("byte /*TODO bitfield */ ")
		default:
			q.field(f.Type())
		}
		if i+1 < nf {
			q.w("; ")
		}
	}
	q.w("}")
}

func (q *qbe) field(t cc.Type) {
	t = q.undelyingType(t)
	if t.IsIntegerType() {
		q.w("int%d", 8*t.Size())
		return
	}

	switch k := kind(t); k {
	case cc.Float, cc.Float32:
		q.w("float32")
	case cc.Double, cc.Float64:
		q.w("float64")
	case cc.Ptr:
		q.w("uintptr")
	case cc.Array:
		q.w("[%d]", t.Len())
		q.field(t.Elem())
	case cc.Struct, cc.Union:
		q.structType(t)
	default:
		q.w("[%d]byte /*TODO %v */", t.Size(), t)
	}
}

func (q *qbe) typeDefinition(t cc.Type, ordinal int) {
	q.w("\n# %s, align %d, size %d", t, t.Align(), t.Size())
	q.w("\ntype :.%d = align %d {", ordinal, t.Align())
	if t.Kind() == cc.Union {
		q.w(" %d }", t.Size())
		return
	}

	q.typeDefinitionRegular(t)
	q.w("}")
}

func (q *qbe) typeDefinitionRegular(t cc.Type) {
	x := []int{0}
	nf := t.NumField()
	var off uintptr
	for i := 0; i < nf; i++ {
		x[0] = i
		f := t.FieldByIndex(x)
		if f.IsBitField() {
			continue
		}

		fo := f.Offset()
		if fo != off {
			q.w("b %d, ", fo-off)
		}
		off = fo

		ft := f.Type()
		k := kind(ft)
		switch {
		case ft.IsArithmeticType():
			q.w("%s, ", q.tdType(ft))
		case k == cc.Ptr:
			q.w("%s, ", q.ptr)
		case k == cc.Array:
			elem, elems := q.array(ft)
			q.w("%s %d, ", q.tdType(elem), elems)
		case k == cc.Struct || k == cc.Union:
			q.w("%s, ", q.tdType(ft))
		default:
			todo(nil, t, ft, k)
		}
		off += ft.Size()
	}
	switch {
	case off != t.Size():
		q.w("b %d", t.Size()-off)
	case nf == 0:
		q.w("b 0")
	}
}

func (q *qbe) array(t cc.Type) (cc.Type, uintptr) {
	elem := t.Elem()
	n := t.Len()
	if elem.Kind() != cc.Array {
		return elem, n
	}

	sub, m := q.array(elem)
	return sub, m * n
}

func (q *qbe) tdType(t cc.Type) string {
	switch kind(t) {
	case cc.Float:
		return "s"
	case cc.Double:
		return "d"
	case cc.LongDouble:
		return "ld"
	case cc.Function:
		return q.ptr
	case cc.Array:
		panic(internalError())
	case cc.Struct, cc.Union:
		si := q.structs[q.undelyingType(t).String()]
		if si == nil {
			todo(nil, t, q.undelyingType(t))
		}
		return fmt.Sprintf(":.%d", si.ordinal)
	}

	switch t.Size() {
	case 1:
		return "b"
	case 2:
		return "h"
	case 4:
		return "w"
	case 8:
		return "l"
	}
	panic("internal error") //TODOOK
}

func (q *qbe) externalDeclaration(n *cc.ExternalDeclaration) {
	switch n.Case {
	case cc.ExternalDeclarationFuncDef: // FunctionDefinition
		q.functionDefinition(n.FunctionDefinition)
	case cc.ExternalDeclarationDecl: // Declaration
		q.declaration(n.Declaration)
	case cc.ExternalDeclarationAsm: // AsmFunctionDefinition
		todo(n)
	case cc.ExternalDeclarationAsmStmt: // AsmStatement
		q.asmStatementExternal(n.AsmStatement)
	case cc.ExternalDeclarationEmpty: // ';'
		// nop
	case cc.ExternalDeclarationPragma: // PragmaSTDC
		todo(n)
	default:
		panic("internal error") //TODOOK
	}
}

func (q *qbe) asmStatementExternal(n *cc.AsmStatement) {
	// Asm AttributeSpecifierList ';'
	if n.AttributeSpecifierList != nil {
		todo(n)
	}

	q.asmExternal(n.Asm)
}

func (q *qbe) asmExternal(n *cc.Asm) {
	// "asm" AsmQualifierList '(' STRINGLITERAL AsmArgList ')'
	if n.AsmQualifierList != nil {
		todo(n)
	}

	if n.AsmArgList != nil {
		todo(n)
	}

	q.externalAsm = append(q.externalAsm, n)
}

// DeclarationSpecifiers Declarator DeclarationList CompoundStatement
func (q *qbe) functionDefinition(n *cc.FunctionDefinition) {
	if len(n.ComputedGotos) != 0 {
		report(n, "computed gotos not supported")
	}

	nm, nested := q.renames[n.Declarator]
	q.w("\n")
	for k := range q.returnComplexExpr {
		delete(q.returnComplexExpr, k)
	}
	for k := range q.declaredLabels {
		delete(q.declaredLabels, k)
	}
	for k := range q.callSiteComplexExpr {
		delete(q.callSiteComplexExpr, k)
	}
	for k := range q.compositeLiterals {
		delete(q.compositeLiterals, k)
	}
	for k := range q.escaped {
		delete(q.escaped, k)
	}
	for k := range q.localStatic {
		delete(q.localStatic, k)
	}
	for k := range q.localVars {
		delete(q.localVars, k)
	}
	for k := range q.values {
		delete(q.values, k)
	}
	for k := range q.vlas {
		delete(q.vlas, k)
	}
	q.scopeVLAs = nil
	q.breakLabel = 0
	q.continueLabel = 0
	q.labels = 0
	q.nestedFunctions = q.nestedFunctions[:0]
	q.tempVars = 0

	d := n.Declarator
	t := d.Type()
	q.fn = t
	q.fd = n
	params := t.Parameters()
	if len(params) == 1 && params[0].Type().Kind() == cc.Void {
		params = nil
	}
	if !nested && !d.IsStatic() {
		q.w("export ")
	}
	q.w("function ")
	switch r := t.Result(); r.Kind() {
	case cc.Void:
		// nop
	default:
		q.w("%s ", q.abiType(n, r))
	}
	switch {
	case nested:
		q.w("$.%d(", nm)
	case d.Linkage == cc.External:
		q.w("$%q(", d.Name())
	case d.Linkage == cc.Internal:
		q.w("$%q(", d.Name())
	default:
		panic("internal error") //TODOOK
	}
	for i, v := range params {
		if v.Type().Kind() == cc.Array && v.Type().IsVLA() {
			later(n, "VLA param")
		}

		q.w("%s ", q.abiType(v.Declarator(), v.Type()))
		d := v.Declarator()
		if d == nil {
			todo(n)
		}

		ord := len(q.localVars) + 1
		q.localVars[d] = ord
		q.values[d] = true
		switch t := d.Type(); {
		case q.canValue(d):
			q.w("%%v%d", ord)
		case (d.AddressTaken || d.Write != 0) && kind(t) != cc.Struct && t.Kind() != cc.Union:
			q.w("%%v0%d", ord)
			if q.opts.goccAllocatedDeclarators {
				q.reportAllocatedDeclarator(d)
			}
		default:
			q.w("%%v%d", ord)
		}
		if i < len(params)-1 {
			q.w(", ")
		}
	}
	if t.IsVariadic() {
		q.w(", ...")
	}
	q.w(") { # %v:\n@.%d\n", q.pos(d), q.label())
	for _, v := range params {
		d := v.Declarator()
		if q.canValue(d) {
			continue
		}

		t := d.Type()
		if !(d.AddressTaken || d.Write != 0) || kind(t) == cc.Struct || t.Kind() == cc.Union {
			continue
		}

		t = t.Decay()
		if t.IsIncomplete() {
			todo(d, t)
		}

		switch t.Align() {
		case 1, 2, 4:
			q.w("\t%%v%d =%s alloc4 %d\t\t# %v: %s %s\n", q.localVars[d], q.ptr, t.Size(), q.pos(d), d.Name(), t)
		case 8:
			q.w("\t%%v%d =%s alloc8 %d\t\t# %v: %s %s\n", q.localVars[d], q.ptr, t.Size(), q.pos(d), d.Name(), t)
		case 16:
			q.w("\t%%v%d =%s alloc16 %d\t\t# %v: %s %s\n", q.localVars[d], q.ptr, t.Size(), q.pos(d), d.Name(), t)
		default:
			todo(n, t, t.Size(), t.Align())
		}
		c := q.t("copy %%v0%[2]d\t\t# %v: %s", q.typ(d, t), q.localVars[d], q.pos(d), d.Name())
		q.w("\tstore%s %%t%d, %%v%d\n", q.extType(t), c, q.localVars[d])
		q.escaped[d] = true
		delete(q.values, d)
	}
	for _, v := range n.CallSiteComplexExpr {
		t := v.Promote()
		var c int
		switch t.Align() {
		case 1, 2, 4:
			c = q.t("alloc4 %[2]d\t\t# %v: %s", q.ptr, t.Size(), q.pos(v), t)
		case 8:
			c = q.t("alloc8 %[2]d\t\t# %v: %s", q.ptr, t.Size(), q.pos(v), t)
		case 16:
			c = q.t("alloc16 %[2]d\t\t# %v: %s", q.ptr, t.Size(), q.pos(v), t)
		default:
			todo(v, t, t.Size(), t.Align())
		}
		q.callSiteComplexExpr[v] = c
	}
	if rt := t.Result(); rt.IsComplexType() {
		for _, v := range n.ReturnComplexExpr {
			var c int
			switch rt.Align() {
			case 4:
				c = q.t("alloc4 %[2]d\t\t# %v: %s", q.ptr, rt.Size(), q.pos(v), rt)
			case 8:
				c = q.t("alloc8 %[2]d\t\t# %v: %s", q.ptr, rt.Size(), q.pos(v), rt)
			default:
				todo(v, rt, rt.Size(), rt.Align())
			}
			q.returnComplexExpr[v] = c
		}
	}
	for _, v := range n.CompositeLiterals {
		t := v.TypeName.Type()
		var c int
		switch t.Align() {
		case 1, 2, 4:
			c = q.t("alloc4 %[2]d\t\t# %v: %s", q.ptr, t.Size(), q.pos(v), t)
		case 8:
			c = q.t("alloc8 %[2]d\t\t# %v: %s", q.ptr, t.Size(), q.pos(v), t)
		default:
			todo(v, t, t.Size(), t.Align())
		}
		q.compositeLiterals[v] = c
	}
	for _, v := range n.VLAs {
		t := v.Type()
		c := len(q.localVars) + 1
		q.localVars[v] = c
		switch q.ptrType.Align() {
		case 4:
			q.w("\t%%v%d =%s alloc4 %d\t\t# %v: %s %s\n", c, q.ptr, q.ptrType.Size(), q.pos(v), v.Name(), t)
		case 8:
			q.w("\t%%v%d =%s alloc8 %d\t\t# %v: %s %s\n", c, q.ptr, q.ptrType.Size(), q.pos(v), v.Name(), t)
		default:
			todo(v, t, t.Size(), t.Align())
		}
		q.w("\tstore%s 0, %%v%d\n", q.ptr, c)
		q.vlas[v] = c
		q.escaped[v] = true
	}
	for _, v := range n.InitDeclarators {
		if d := v.Declarator; d.Linkage == cc.None && q.localVars[d] == 0 {
			q.initDeclarator(v, true)
		}
	}
	q.compoundStatementLeave(n.CompoundStatement, true)
	q.defaultResult(d)
	q.w("}\n")
	q.fn = nil
	q.fd = nil
	if len(q.initializerQueue)+len(q.stringQueue)+len(q.nestedFunctions) != 0 {
		q.w("\n")
	}
	for len(q.initializerQueue)+len(q.stringQueue)+len(q.nestedFunctions) != 0 {
		q.flushInitializers()
		// Must be after initializers, they can produce pointers to string data.
		q.flushStrings()
		q.flushNestedFunctions()
	}
	q.fn = nil
	q.fd = nil
}

func (q *qbe) reportAllocatedDeclarator(d *cc.Declarator) {
	fmt.Fprintf(q.task.stderr, "%v: %s %s: declarator will be allocated, type: %v (%v, %v), address taken: %v\n",
		d.Position(), q.fd.Declarator.Name(), d.Name(), d.Type(), d.Type().Kind(), d.Type().Size(), d.AddressTaken,
	)
}

func (q *qbe) registerStruct(n cc.Node, t cc.Type) int {
	t = q.undelyingType(t)
	switch kind(t) {
	case cc.Struct, cc.Union:
		// ok
	default:
		return -1
	}

	s := t.String()
	if si, ok := q.structs[s]; ok {
		return si.ordinal
	}

	if q.structs == nil {
		q.structs = map[string]*structInfo{}
	}
	ord := len(q.structs) + 1
	q.structs[s] = &structInfo{ord, t}
	x := []int{0}
	nf := t.NumField()
	for i := 0; i < nf; i++ {
		x[0] = i
		f := t.FieldByIndex(x)
		ft := q.undelyingType(f.Type())
		switch kind(ft) {
		case cc.Struct, cc.Union:
			q.registerStruct(nil, ft)
		case cc.Array:
			q.registerStruct(nil, ft.Elem())
		}
	}
	return ord
}

func (q *qbe) undelyingType(t cc.Type) cc.Type {
	switch {
	case t.IsAliasType():
		return q.undelyingType(t.Alias())
	case t.IsTaggedType():
		return q.undelyingType(t.Alias())
	}

	return t
}

func (q *qbe) isVaList(t cc.Type) bool { return t.String() == "va_list" }

func kind(t cc.Type) cc.Kind {
	if !t.IsComplexType() {
		return t.Kind()
	}

	return cc.Struct
}

func (q *qbe) defaultResult(d *cc.Declarator) {
	q.w("@.%d\n", q.label())
	t := d.Type().Result()
	switch kind(t) {
	case cc.Void:
		q.w("\tret\n")
	case cc.Ptr:
		q.w("\tret 0\n")
	case cc.Double:
		q.w("\tret d_0\n")
	case cc.Struct, cc.Union:
		var a int
		switch t.Align() {
		case 1, 2, 4:
			a = q.t("alloc4 %[2]d", q.ptr, t.Size())
		case 8:
			a = q.t("alloc8 %[2]d", q.ptr, t.Size())
		case 16:
			a = q.t("alloc16 %[2]d", q.ptr, t.Size())
		default:
			todo(d, t.Align())
		}
		q.w("\tret %%t%d\n", a)
	case cc.Float:
		q.w("\tret s_0\n")
	case cc.LongDouble:
		q.w("\tret ld_0\n")
	default:
		if t.IsIntegerType() {
			q.w("\tret 0\n")
			return
		}

		todo(d, t, t.Kind())
		panic(internalErrorf("%v: %v, %v", d.Position(), t, t.Kind()))
	}
}

func (q *qbe) flushInitializers() {
	for _, v := range q.initializerQueue {
		q.w("\ndata $.%d = align %d {", v.name, v.typ.Align())
		switch {
		case v.initializer == nil:
			q.w(" z %d ", v.typ.Size())
		default:
			v.initializer.emit(q)
		}
		q.w("}\t\t%s\n", v.comment)
	}
	q.initializerQueue = q.initializerQueue[:0]
}

func (q *qbe) flushNestedFunctions() {
	for i := 0; i < len(q.nestedFunctions); i++ {
		v := q.nestedFunctions[i]
		q.functionDefinition(v.n)
	}
	q.nestedFunctions = q.nestedFunctions[:0]
	for k := range q.renames {
		delete(q.renames, k)
	}
}

func (q *qbe) flushStrings() {
	for _, v := range q.stringQueue {
		switch {
		case v < 0:
			q.renderLStringData(q.strings[v], -v)
		default:
			q.renderStringData(q.strings[v], v)
		}
	}
	q.stringQueue = q.stringQueue[:0]
}

func (q *qbe) renderLStringData(id int, v cc.StringID) {
	switch {
	case v == 0:
		q.w("\ndata $.%d = { z %d }\n", id, q.wchar.Size())
	default:
		q.w("\ndata $.%d = { %s", id, q.extType(q.wchar))
		for _, r := range v.String() {
			q.w(" %d", r)
		}
		q.w(", z %d }\n", q.wchar.Size())
	}
}

func (q *qbe) renderStringData(id int, v cc.StringID) {
	q.w("\ndata $.%d = { ", id)
	switch {
	case v == 0:
		q.w("z 1 }\n")
	default:
		q.w("b %s", q.quotedString(v.String()))
		q.w(", b 0 }\n")
	}
}

func (q *qbe) quotedString(s string) []byte {
	q.quoted = q.quoted[:0]
	q.quote('"')
	for i := 0; i < len(s); i++ {
		c := s[i]
		switch c {
		case '\b':
			q.quote('\\', 'b')
			continue
		case '\f':
			q.quote('\\', 'f')
			continue
		case '\n':
			q.quote('\\', 'n')
			continue
		case '\r':
			q.quote('\\', 'r')
			continue
		case '\t':
			q.quote('\\', 't')
			continue
		case '\\':
			q.quote('\\', '\\')
			continue
		case '"':
			q.quote('\\', '"')
			continue
		}

		switch {
		case c < ' ' || c >= 0x7f:
			q.quote('\\', octal(c>>6), octal(c>>3), octal(c))
		default:
			q.quote(c)
		}
	}
	q.quote('"')
	return q.quoted
}

func octal(b byte) byte { return '0' + b&7 }

func (q *qbe) quote(b ...byte) { q.quoted = append(q.quoted, b...) }

// '{' BlockItemList '}'
func (q *qbe) compoundStatement(n *cc.CompoundStatement, void bool) (r int, vlas []*cc.Declarator) {
	save := q.scopeVLAs
	q.scopeVLAs = nil
	for n := n.BlockItemList; n != nil; n = n.BlockItemList {
		r = q.blockItem(n.BlockItem, void)
	}
	vlas = q.scopeVLAs
	q.scopeVLAs = save
	return r, vlas
}

func (q *qbe) compoundStatementLeave(n *cc.CompoundStatement, void bool) (r int) {
	r, vlas := q.compoundStatement(n, void)
	q.leaveScope(vlas)
	return r
}

func (q *qbe) leaveScope(vlas []*cc.Declarator) {
	for _, d := range vlas {
		p := q.t("load%[2]s %%v%d", q.ptr, q.loadType(q.ptrType), q.vlas[d])
		q.w("\tcall $free(%s %%t%d)\n", q.abiType(d, q.ptrType), p)
		q.w("\tstore%[2]s 0, %%v%d\n", q.ptr, q.loadType(q.ptrType), q.vlas[d])
	}
}

func (q *qbe) blockItem(n *cc.BlockItem, void bool) int {
	switch n.Case {
	case cc.BlockItemDecl: // Declaration
		q.declaration(n.Declaration)
	case cc.BlockItemStmt: // Statement
		return q.statementLeave(n.Statement, void)
	case cc.BlockItemLabel: // LabelDeclaration
		todo(n, n.Case)
	case cc.BlockItemFuncDef: // DeclarationSpecifiers Declarator CompoundStatement
		if len(n.Closure()) != 0 {
			report(n, "non empty closure")
		}

		a := q.anon()
		q.renames[n.Declarator] = a
		q.nestedFunctions = append(q.nestedFunctions, nestedFunction{a, n.FunctionDefinition()})
	case cc.BlockItemPragma: // PragmaSTDC
		todo(n, n.Case)
	default:
		panic("internal error") //TODOOK
	}
	return -1
}

func (q *qbe) statement(n *cc.Statement, void bool) (r int, vlas []*cc.Declarator) {
	switch n.Case {
	case cc.StatementLabeled: // LabeledStatement
		q.labeledStatement(n.LabeledStatement)
	case cc.StatementCompound: // CompoundStatement
		return q.compoundStatement(n.CompoundStatement, void)
	case cc.StatementExpr: // ExpressionStatement
		return q.expressionStatement(n.ExpressionStatement, void), nil
	case cc.StatementSelection: // SelectionStatement
		q.selectionStatement(n.SelectionStatement)
	case cc.StatementIteration: // IterationStatement
		q.iterationStatement(n.IterationStatement)
	case cc.StatementJump: // JumpStatement
		q.jumpStatement(n.JumpStatement)
	case cc.StatementAsm: // AsmStatement
		q.asmStatement(n.AsmStatement)
	default:
		panic("internal error") //TODOOK
	}
	return -1, nil
}

func (q *qbe) statementLeave(n *cc.Statement, void bool) int {
	switch n.Case {
	case cc.StatementCompound: // CompoundStatement
		q.compoundStatementLeave(n.CompoundStatement, void)
		return -1
	default:
		r, _ := q.statement(n, void)
		return r
	}
}

func (q *qbe) asmStatement(n *cc.AsmStatement) {
	// Asm AttributeSpecifierList ';'
	if n.AttributeSpecifierList != nil {
		todo(n)
	}

	q.asm(n.Asm)
}

func (q *qbe) asm(n *cc.Asm) {
	// "asm" AsmQualifierList '(' STRINGLITERAL AsmArgList ')'
	if n.AsmQualifierList != nil {
		//TODO todo(n.AsmQualifierList)
	}

	if n.Token3.Value != 0 {
		later(n, "assembler instructions")
	}

	for n := n.AsmArgList; n != nil; n = n.AsmArgList {
		for n := n.AsmExpressionList; n != nil; n = n.AsmExpressionList {
			if n.AssignmentExpression.Operand.Value() != nil {
				return
			}

			report(n.AssignmentExpression, "asm")
		}
	}
}

func (q *qbe) declareLabel(n int) {
	if _, ok := q.declaredLabels[n]; ok {
		return
	}

	q.w("@.%d\n", n)
	q.declaredLabels[n] = struct{}{}
}

func (q *qbe) labeledStatement(n *cc.LabeledStatement) {
	switch n.Case {
	case cc.LabeledStatementLabel: // IDENTIFIER ':' AttributeSpecifierList Statement
		q.w("@%s\n", n.Token.Value)
		if n.AttributeSpecifierList != nil {
			todo(n)
		}
		q.statement(n.Statement, true)
	case cc.LabeledStatementCaseLabel: // "case" ConstantExpression ':' Statement
		q.declareLabel(q.cases[n])
		q.statement(n.Statement, true)
	case cc.LabeledStatementRange: // "case" ConstantExpression "..." ConstantExpression ':' Statement
		todo(n)
	case cc.LabeledStatementDefault: // "default" ':' Statement
		q.declareLabel(q.cases[n])
		q.statement(n.Statement, true)
	default:
		todo(n, n.Case)
		panic("internal error") //TODOOK
	}
}

func (q *qbe) selectionStatement(n *cc.SelectionStatement) {
	switch n.Case {
	case cc.SelectionStatementIf: // "if" '(' Expression ')' Statement
		// if expr != 0 goto a else goto z
		// @a
		// statement
		// @z
		a := q.label()
		z := q.label()
		q.jnz(n.Expression, a, z)
		q.w("@.%d\n", a)
		q.statement(n.Statement, true)
		q.w("@.%d\n", z)
	case cc.SelectionStatementIfElse: // "if" '(' Expression ')' Statement "else" Statement
		// if expr != 0 goto a else goto b
		// @a
		// statement
		// goto z
		// @b
		// statement2
		// @z
		a := q.label()
		b := q.label()
		z := q.label()
		q.jnz(n.Expression, a, b)
		q.w("@.%d\n", a)
		q.statement(n.Statement, true)
		q.w("\tjmp @.%d\n", z)
		q.w("@.%d\n", b)
		q.statement(n.Statement2, true)
		q.w("@.%d\n", z)
	case cc.SelectionStatementSwitch: // "switch" '(' Expression ')' Statement
		e := q.expression(n.Expression, false)
		e = q.convert(n.Expression, e, n.Expression.Operand.Type(), n.Promote(), nil)
		// q.w("\t#qbe:switch: %%t%d\n", e)
		save := q.cases
		cases := n.Cases()
		q.cases = make(map[*cc.LabeledStatement]int, len(cases))
		for _, v := range cases {
			q.assignCaseLabel(v, q.cases)
		}
		switch {
		//TODO case len(cases) > 16: //TODO benchmark tune
		//TODO 	todo(n, len(cases))
		default:
			q.smallSwitch(e, q.typ(n, n.Promote()), cases, n.Statement)
		}
		q.cases = save
	default:
		todo(n, n.Case)
		panic("internal error") //TODOOK
	}
}

func (q *qbe) assignCaseLabel(n *cc.LabeledStatement, m map[*cc.LabeledStatement]int) int {
	if x, ok := m[n]; ok {
		return x
	}

	if n.Statement.Case == cc.StatementLabeled {
		x := q.assignCaseLabel(n.Statement.LabeledStatement, m)
		m[n] = x
		return x
	}

	x := q.label()
	m[n] = x
	return x
}

func (q *qbe) jnz(n *cc.Expression, nz, z int) {
	switch t := n.Operand.Type(); {
	case t.IsComplexType():
		// re, im = expr
		// jnz re, nz, a
		// @a
		// jnz im, nz, z
		a := q.label()
		re, im := q.complexExpression(n, false)
		test := q.t("cne%[2]s %%t%d, 0", "w", q.typ(n, t.Real().Type()), re) //TODO "w"->.int
		q.w("\tjnz %%t%d, @.%d, @.%d\n", test, nz, a)
		q.w("@.%d\n", a)
		test = q.t("cne%[2]s %%t%d, 0", "w", q.typ(n, t.Imag().Type()), im) //TODO "w"->.int
		q.w("\tjnz %%t%d, @.%d, @.%d\n", test, nz, z)
	default:
		e := q.expression(n, false)
		e = q.nonZero(n, e, n.Operand)
		q.w("\tjnz %%t%d, @.%d, @.%d\n", e, nz, z)
	}
}

func (q *qbe) smallSwitch(expr int, et string, cases []*cc.LabeledStatement, stmt *cc.Statement) {
	it := q.typ(stmt, q.intType)
	var def *cc.LabeledStatement
	for _, n := range cases {
		switch n.Case {
		case cc.LabeledStatementCaseLabel:
			v := n.ConstantExpression.Operand.Value()
			// q.w("\t#qbe:switchCase: %d @.%d\n", v, q.cases[n])
			c := q.t("ceq%[2]s %%t%d, %d\t\t# %v:", it, et, expr, v, q.pos(n))
			l := q.label()
			q.w("\tjnz %%t%d, @.%d, @.%d\n", c, q.cases[n], l)
			q.w("@.%d\n", l)
		case cc.LabeledStatementRange:
			later(n, "labeled statement range")
		case cc.LabeledStatementDefault:
			// q.w("\t#qbe:switchDefault: @.%d\n", q.cases[n])
			def = n
		default:
			todo(n, n.Case)
			panic("internal error") //TODOOK
		}
	}
	save := q.breakLabel
	q.breakLabel = q.label()
	switch def {
	case nil:
		// q.w("\t#qbe:switchDefault: @.%d\n", q.breakLabel)
		// q.w("\t#qbe:switchEnd:\n")
		q.w("\tjmp @.%d\n", q.breakLabel)
		q.w("@.%d\n", q.label())
	default:
		// q.w("\t#qbe:switchEnd:\n")
		q.w("\tjmp @.%d\n", q.cases[def])
		q.w("@.%d\n", q.label())
	}
	q.statement(stmt, true)
	q.w("@.%d\n", q.breakLabel)
	q.breakLabel = save
}

func (q *qbe) iterationStatement(n *cc.IterationStatement) {
	switch n.Case {
	case cc.IterationStatementWhile: // "while" '(' Expression ')' Statement
		// @a
		// if expr != 0 goto b else goto z
		// @b
		// statement
		// goto a
		// z@
		a := q.label()
		b := q.label()
		z := q.label()
		bs := q.breakLabel
		q.breakLabel = z
		cs := q.continueLabel
		q.continueLabel = a
		q.w("@.%d\n", a)
		q.jnz(n.Expression, b, z)
		q.w("@.%d\n", b)
		_, vlas := q.statement(n.Statement, true)
		q.w("\tjmp @.%d\n", a)
		q.w("@.%d\n", z)
		q.leaveScope(vlas)
		q.breakLabel = bs
		q.continueLabel = cs
	case cc.IterationStatementDo: // "do" Statement "while" '(' Expression ')' ';'
		// @a
		// statement
		// @b
		// if expr != 0 goto a else goto z
		// @z
		a := q.label()
		b := q.label()
		z := q.label()
		bs := q.breakLabel
		q.breakLabel = z
		cs := q.continueLabel
		q.continueLabel = b
		q.w("@.%d\n", a)
		_, vlas := q.statement(n.Statement, true)
		q.w("@.%d\n", b)
		q.jnz(n.Expression, a, z)
		q.w("@.%d\n", z)
		q.leaveScope(vlas)
		q.breakLabel = bs
		q.continueLabel = cs
	case cc.IterationStatementFor: // "for" '(' Expression ';' Expression ';' Expression ')' Statement
		// expr
		// @a
		// if expr2 != 0 goto b else goto z
		// @b
		// statement
		// @c
		// expr3
		// goto a
		// z@
		if n.Expression != nil {
			q.expression(n.Expression, true)
		}
		a := q.label()
		b := q.label()
		c := q.label()
		z := q.label()
		bs := q.breakLabel
		q.breakLabel = z
		cs := q.continueLabel
		q.continueLabel = c
		q.w("@.%d\n", a)
		if n.Expression2 != nil {
			q.jnz(n.Expression2, b, z)
		}
		q.w("@.%d\n", b)
		_, vlas := q.statement(n.Statement, true)
		q.w("@.%d\n", c)
		if n.Expression3 != nil {
			q.expression(n.Expression3, true)
		}
		q.w("\tjmp @.%d\n", a)
		q.w("@.%d\n", z)
		q.leaveScope(vlas)
		q.breakLabel = bs
		q.continueLabel = cs
	case cc.IterationStatementForDecl: // "for" '(' Declaration Expression ';' Expression ')' Statement
		// decl
		// @a
		// if expr != 0 goto b else goto z
		// @b
		// statement
		// @c
		// expr2
		// goto a
		// z@
		q.declaration(n.Declaration)
		a := q.label()
		b := q.label()
		c := q.label()
		z := q.label()
		bs := q.breakLabel
		q.breakLabel = z
		cs := q.continueLabel
		q.continueLabel = c
		q.w("@.%d\n", a)
		if n.Expression != nil {
			q.jnz(n.Expression, b, z)
		}
		q.w("@.%d\n", b)
		_, vlas := q.statement(n.Statement, true)
		q.w("@.%d\n", c)
		if n.Expression2 != nil {
			q.expression(n.Expression2, true)
		}
		q.w("\tjmp @.%d\n", a)
		q.w("@.%d\n", z)
		q.leaveScope(vlas)
		q.breakLabel = bs
		q.continueLabel = cs
	default:
		todo(n, n.Case)
		panic("internal error") //TODOOK
	}
}

func (q *qbe) label() int { q.labels++; return q.labels }

func (q *qbe) jumpStatement(n *cc.JumpStatement) {
	switch n.Case {
	case cc.JumpStatementGoto: // "goto" IDENTIFIER ';'
		nm := n.Token2.Value
		src := n.LexicalScope().Parent()
	out:
		for ; src != nil; src = src.Parent() {
			if a, ok := src[nm]; ok {
				for _, v := range a {
					switch v.(type) {
					case *cc.LabeledStatement:
						q.leaveScope(q.scopeVLAs)
						break out
					}
				}
			}
		}
		q.w("\tjmp @%s\t\t# %v:\n", nm, q.pos(n))
		q.w("@.%d\n", q.label())
	//0 	case cc.JumpStatementGotoExpr: // "goto" '*' Expression ';'
	//0 		todo(n)
	case cc.JumpStatementContinue: // "continue" ';'
		q.w("\tjmp @.%d\t\t# %v:\n", q.continueLabel, q.pos(n))
		q.w("@.%d\n", q.label())
	case cc.JumpStatementBreak: // "break" ';'
		q.w("\tjmp @.%d\t\t# %v:\n", q.breakLabel, q.pos(n))
		q.w("@.%d\n", q.label())
	case cc.JumpStatementReturn: // "return" Expression ';'
		var vlas []*cc.Declarator
		for k := range q.vlas {
			vlas = append(vlas, k)
		}
		sort.Slice(vlas, func(a, b int) bool { return vlas[a].NameTok().Seq() < vlas[b].NameTok().Seq() })
		for _, d := range vlas {
			p := q.t("load%[2]s %%v%d", q.ptr, q.loadType(q.ptrType), q.vlas[d])
			q.w("\tcall $free(%s %%t%d)\n", q.abiType(d, q.ptrType), p)
		}
		q.ret(n.Expression)
		q.w("@.%d\n", q.label())
	default:
		todo(n, n.Case)
		panic("internal error") //TODOOK
	}
}

func (q *qbe) ret(n *cc.Expression) {
	if n == nil {
		q.w("\tret\t\t# %v:\n", q.pos(n))
		return
	}

	t := n.Operand.Type()
	rt := q.fn.Result()
	if t.IsComplexType() {
		if rt.Kind() == cc.Void {
			q.complexExpression(n, true)
			q.w("\tret\t\t# %v:\n", q.pos(n))
			return
		}

		if d := n.Operand.Declarator(); d != nil && d.Type().Kind() == rt.Kind() {
			e := q.declarator(n, n.Operand)
			q.w("\tret %%t%d\t\t# %v:\n", e, q.pos(n))
			return
		}

		re, im := q.complexExpression(n, false)
		re, im = q.convertToComplex(n, re, im, t, rt)
		r := q.returnComplexExpr[n]
		p := q.t("add %%t%[2]d, %d", q.ptr, r, rt.Real().Offset())
		q.store(n, p, re, nil, rt.Real().Type(), true)
		p = q.t("add %%t%[2]d, %d", q.ptr, r, rt.Imag().Offset())
		q.store(n, p, im, nil, rt.Imag().Type(), true)
		q.w("\tret %%t%d\t\t# %v:\n", r, q.pos(n))
		return
	}

	if rt.Kind() == cc.Void {
		q.expression(n, true)
		q.w("\tret\t\t# %v:\n", q.pos(n))
		return
	}

	e := q.expression(n, false)
	switch t.Kind() {
	case cc.Array, cc.Struct, cc.Union:
		q.w("\tret %%t%d\t\t# %v:\n", e, q.pos(n))
		return
	}

	e = q.convert(n, e, n.Operand.Type(), rt, nil)
	q.w("\tret %%t%d\t\t# %v:\n", e, q.pos(n))
}

// Expression AttributeSpecifierList ';'
func (q *qbe) expressionStatement(n *cc.ExpressionStatement, void bool) int {
	if n.AttributeSpecifierList != nil {
		todo(n)
	}
	if n.Expression != nil {
		switch {
		case n.Expression.Operand.Type().IsComplexType():
			if !void {
				todo(n)
			}

			q.complexExpression(n.Expression, void)
		default:
			return q.expression(n.Expression, void)
		}
	}
	return -1
}

func (q *qbe) complexExpression(n *cc.Expression, void bool) (int, int) {
	switch n.Case {
	case cc.ExpressionAssign: // AssignmentExpression
		return q.complexAssignmentExpression(n.AssignmentExpression, void)
		// case cc.ExpressionComma: // Expression ',' AssignmentExpression
		// 	q.expression(n.Expression)
		// 	return q.assignmentExpression(n.AssignmentExpression)
	}
	todo(n, n.Case)
	panic(internalErrorf("%v", n.Case))
}

func (q *qbe) expression(n *cc.Expression, void bool) int {
	if void && n.IsSideEffectsFree {
		return -1
	}

	switch n.Case {
	case cc.ExpressionAssign: // AssignmentExpression
		return q.assignmentExpression(n.AssignmentExpression, void)
	case cc.ExpressionComma: // Expression ',' AssignmentExpression
		if !n.Expression.IsSideEffectsFree {
			q.expression(n.Expression, true)
		}
		return q.assignmentExpression(n.AssignmentExpression, void)
	}
	panic("internal error") //TODOOK
}

func (q *qbe) assignmentExpression(n *cc.AssignmentExpression, void bool) int {
	switch n.Case {
	case cc.AssignmentExpressionCond: // ConditionalExpression
		if void && n.IsSideEffectsFree {
			return -1
		}

		return q.conditionalExpression(n.ConditionalExpression, void)
	case cc.AssignmentExpressionAssign: // UnaryExpression '=' AssignmentExpression
		rd := n.AssignmentExpression.Operand.Declarator()
		ld := n.UnaryExpression.Operand.Declarator()
		if ld != nil && rd != nil && ld.Type().String() == "va_list" && rd.Type().String() == "va_list" { // va_copy
			q.w("\tcall $__builtin_va_copy(%s %s, %s %s)\t\t# %v:\n", q.ptr, q.declName(n, ld), q.ptr, q.declName(n, rd), q.pos(&n.Token))
			return -1
		}

		switch t := n.AssignmentExpression.Operand.Type(); kind(t) {
		case cc.Struct, cc.Union:
			rhs := q.assignmentExpression(n.AssignmentExpression, false)
			lhs := q.unaryExpressionPtr(n.UnaryExpression)
			q.w("\tcall $memcpy(%s %%t%d, %s %%t%d, %s %d)\t\t# %v:\n", q.ptr, lhs, q.ptr, rhs, q.ptr, n.UnaryExpression.Operand.Type().Size(), q.pos(&n.Token))
			return rhs
		}

		rhs := q.assignmentExpression(n.AssignmentExpression, false)
		rhs = q.convert(n, rhs, n.AssignmentExpression.Operand.Type(), n.UnaryExpression.Operand.Type(), nil)
		switch d := n.UnaryExpression.Declarator(); {
		case q.values[d]:
			return q.stored(&n.Token, d, rhs, n.UnaryExpression.Operand, void)
		default:
			lhs := q.unaryExpressionPtr(n.UnaryExpression)
			return q.store(&n.Token, lhs, rhs, n.UnaryExpression.Operand, n.UnaryExpression.Operand.Type(), void)
		}
	case cc.AssignmentExpressionMul: // UnaryExpression "*=" AssignmentExpression
		return q.asgnOp(n, "mul", void)
	case cc.AssignmentExpressionDiv: // UnaryExpression "/=" AssignmentExpression
		return q.asgnOp(n, "div", void)
	case cc.AssignmentExpressionMod: // UnaryExpression "%=" AssignmentExpression
		return q.asgnOp(n, "rem", void)
	case cc.AssignmentExpressionAdd: // UnaryExpression "+=" AssignmentExpression
		return q.asgnOp(n, "add", void)
	case cc.AssignmentExpressionSub: // UnaryExpression "-=" AssignmentExpression
		return q.asgnOp(n, "sub", void)
	case cc.AssignmentExpressionLsh: // UnaryExpression "<<=" AssignmentExpression
		return q.shiftop(n, "shl", void)
	case cc.AssignmentExpressionRsh: // UnaryExpression ">>=" AssignmentExpression
		return q.shiftop(n, "shr", void)
	case cc.AssignmentExpressionAnd: // UnaryExpression "&=" AssignmentExpression
		return q.asgnOp(n, "and", void)
	case cc.AssignmentExpressionXor: // UnaryExpression "^=" AssignmentExpression
		return q.asgnOp(n, "xor", void)
	case cc.AssignmentExpressionOr: // UnaryExpression "|=" AssignmentExpression
		return q.asgnOp(n, "or", void)
	}
	todo(n, n.Case)
	panic("internal error") //TODOOK
}

func (q *qbe) shiftop(n *cc.AssignmentExpression, operator string, void bool) int {
	operand := n.UnaryExpression.Operand
	switch kind(operand.Type()) { //TODO-
	case cc.Array:
		todo(n)
	case cc.Struct:
		todo(n)
	}

	if operator == "shr" && n.Operand.Type().IsIntegerType() && n.Operand.Type().IsSignedType() {
		operator = "sar"
	}
	rhs := q.assignmentExpression(n.AssignmentExpression, false)
	rhs = q.convert(n, rhs, n.AssignmentExpression.Operand.Type(), n.Promote(), nil)
	rhs = q.t("and %[2]d, %%t%d", q.typ(n, n.Promote()), q.shiftMask(operand.Type()), rhs)
	switch d := n.UnaryExpression.Declarator(); {
	case q.values[d]:
		t := q.declarator(n, operand)
		rhs = q.t("%[2]s %%t%d, %%t%d\t\t# %v:", q.typ(n, n.Operand.Type()), operator, t, rhs, q.pos(&n.Token))
		return q.stored(&n.Token, d, rhs, operand, void)
	default:
		lhs := q.unaryExpressionPtr(n.UnaryExpression)
		t := q.load(n, lhs, operand, operand.Type())
		rhs = q.t("%[2]s %%t%d, %%t%d\t\t# %v:", q.typ(n, n.Operand.Type()), operator, t, rhs, q.pos(&n.Token))
		return q.store(&n.Token, lhs, rhs, operand, operand.Type(), void)
	}
}

func (q *qbe) shiftMask(t cc.Type) int {
	if t.Size() > 4 {
		return 63
	}

	return 31
}

func (q *qbe) isVolatile(d *cc.Declarator) (ok bool) {
	if d == nil {
		return false
	}

	if d.Linkage != cc.None && d.Type().IsVolatile() {
		return true
	}

	if d.Linkage == cc.External {
		_, ok = q.opts.qbecVolatile[d.Name()]
	}
	return ok
}

func (q *qbe) load(n cc.Node, p int, op cc.Operand, t cc.Type) int {
	var d *cc.Declarator
	if op != nil {
		d = op.Declarator()
	}
	switch {
	case t.IsBitFieldType():
		f := t.BitField()
		v := q.t("load%[2]s %%t%d\t\t# %v:", q.typ(n, f.Type()), q.loadType(f.Type()), p, q.pos(n))
		sz := int(f.Type().Size()) * 8
		if f.Type().Size() < q.intType.Size() {
			sz = int(q.intType.Size()) * 8
		}
		v = q.t("shl %%t%[2]d, %d", q.typ(n, f.Type()), v, sz-f.BitFieldWidth()-f.BitFieldOffset())
		switch {
		case f.Type().IsSignedType():
			v = q.t("sar %%t%[2]d, %d", q.typ(n, f.Type()), v, sz-f.BitFieldWidth())
		default:
			v = q.t("shr %%t%[2]d, %d", q.typ(n, f.Type()), v, sz-f.BitFieldWidth())
		}
		return v
	default:
		switch kind(t) {
		case cc.Function:
			if q.escaped[d] {
				todo(n, d.Name())
			}

			return p
		case cc.Array:
			if q.escaped[d] {
				return q.t("load%[2]s %%t%d\t\t# %v:", q.typ(n, t), q.loadType(t), p, q.pos(n))
			}

			return p
		case cc.Struct, cc.Union:
			if q.escaped[d] { //TODO- not special
				return p
			}

			return p
		default:
			if op != nil && q.isVolatile(op.Declarator()) {
				q.w("\t#qbe:volatile\n")
			}
			return q.t("load%[2]s %%t%d\t\t# %v:", q.typ(n, t), q.loadType(t), p, q.pos(n))
		}
	}
}

func (q *qbe) stored(n cc.Node, d *cc.Declarator, rhs int, op cc.Operand, void bool) int {
	q.w("\t%%v%d =%s copy %%t%d\t\t# %v:\n", q.localVars[d], q.typ(n, d.Type()), rhs, n.Position())
	if void {
		return -1
	}

	return q.convert(n, q.declarator(n, op), d.Type(), op.Type(), nil)
}

func (q *qbe) store(n cc.Node, lhs, rhs int, op cc.Operand, t cc.Type, void bool) int {
	switch {
	case t.IsBitFieldType():
		f := t.BitField()
		v := q.t("load%[2]s %%t%d\t\t# %v:", q.typ(n, f.Type()), q.loadType(f.Type()), lhs, q.pos(n))
		v = q.t("and %%t%[2]d, %d\t\t# ^%#x", q.typ(n, f.Type()), v, ^f.Mask(), f.Mask())
		rhs = q.convert(n, rhs, t, f.Type(), nil)
		rhs = q.t("shl %%t%[2]d, %d", q.typ(n, f.Type()), rhs, f.BitFieldOffset())
		rhs = q.t("and %%t%[2]d, %d\t\t# %#[3]x", q.typ(n, f.Type()), rhs, f.Mask())
		v = q.t("or %%t%[2]d, %%t%d", q.typ(n, f.Type()), v, rhs)
		q.w("\tstore%s %%t%d, %%t%d\t\t# %v:\n", q.extType(f.Type()), v, lhs, q.pos(n))
	default:
		if op != nil && q.isVolatile(op.Declarator()) {
			q.w("\t#qbe:volatile\n")
		}
		q.w("\tstore%s %%t%d, %%t%d\t\t# %v:\n", q.extType(t), rhs, lhs, q.pos(n))
	}
	if void {
		return -1
	}

	return q.load(n, lhs, op, t)
}

func (q *qbe) asgnOp(n *cc.AssignmentExpression, operator string, void bool) int {
	operand := n.UnaryExpression.Operand
	d := operand.Declarator()
	switch kind(operand.Type()) { //TODO-
	case cc.Array:
		if d != nil && d.IsParameter {
			break
		}

		todo(n)
	case cc.Struct:
		todo(n)

	}
	var mulR uintptr = 1
	if t := operand.Type().Decay(); t.Kind() == cc.Ptr {
		mulR = t.Elem().Size()
	}

	if mulR == 0 {
		panic("internal error") //TODOOK
	}

	if (operator == "div" || operator == "rem") && n.Promote().IsIntegerType() && !n.Promote().IsSignedType() {
		operator = "u" + operator
	}
	rhs := q.assignmentExpression(n.AssignmentExpression, false)
	rhs = q.convert(n, rhs, n.AssignmentExpression.Operand.Type(), n.Promote(), nil)
	if mulR != 1 {
		rhs = q.t("mul %%t%[2]d, %d", q.ptr, rhs, mulR)
	}
	switch d := n.UnaryExpression.Declarator(); {
	case q.values[d]:
		lhs := q.declarator(n, operand)
		t := q.convert(n, lhs, operand.Type(), n.Promote(), nil)
		rhs = q.t("%[2]s %%t%d, %%t%d\t\t# %v:", q.typ(n, n.Promote()), operator, t, rhs, q.pos(&n.Token))
		rhs = q.convert(n, rhs, n.Promote(), operand.Type(), nil)
		return q.stored(&n.Token, d, rhs, operand, void)
	default:
		lhs := q.unaryExpressionPtr(n.UnaryExpression)
		t := q.load(n, lhs, operand, operand.Type())
		t = q.convert(n, t, operand.Type(), n.Promote(), nil)
		rhs = q.t("%[2]s %%t%d, %%t%d\t\t# %v:", q.typ(n, n.Promote()), operator, t, rhs, q.pos(&n.Token))
		rhs = q.convert(n, rhs, n.Promote(), operand.Type(), nil)
		return q.store(&n.Token, lhs, rhs, operand, operand.Type(), void)
	}
}

func (q *qbe) declName(n cc.Node, d *cc.Declarator) string {
	if d == nil || d.Name() == 0 {
		todo(n)
	}

	switch d.Linkage {
	case cc.None:
		if d.IsStatic() {
			return fmt.Sprintf("$.%d", q.localStatic[d])
		}

		return fmt.Sprintf("%%v%d", q.localVars[d])
	case cc.Internal:
		return fmt.Sprintf("$%q", d.Name())
	case cc.External:
		return fmt.Sprintf("$%q", d.Name())
	}
	todo(n, d.Linkage)
	panic("internal error") //TODOOK
}

func (q *qbe) convert(n cc.Node, e int, from, to cc.Type, val cc.Value) int {
	switch {
	case to.Kind() == cc.Void:
		return -1
	case from.IsIntegerType():
		return q.convertFromInt(n, e, from, to)
	case from.Kind() == cc.Ptr:
		if to.Kind() == cc.Ptr {
			return e
		}

		if to.IsIntegerType() {
			if to.Size() == from.Size() {
				return e
			}

			if from.Size() == 8 && to.Size() <= 4 {
				// Use QBE subtyping
				return e
			}
		}

		return e
	case from.Kind() == cc.Function:
		switch to.Kind() {
		case cc.Function, cc.Ptr:
			return e
		}

		if to.IsIntegerType() {
			if q.ptrType.Size() == to.Size() {
				return e
			}
		}

		todo(n, fmt.Sprintf("%v %q %v -> %q %v", e, from, from.Size(), to, to.Size()))
	case from.Decay().Kind() == cc.Ptr && to.Kind() == cc.Ptr:
		return e
	case from.Kind() == cc.Float:
		if to.IsIntegerType() {
			return q.float2Int(n, e, from, to, val)
		}

		switch to.Kind() {
		case cc.Float:
			return e
		case cc.Double, cc.LongDouble:
			return q.t("exts %%t%[2]d\t\t# %v:", q.typ(n, to), e, q.pos(n))
		default:
			todo(n, e, from, to)
		}
	case from.Kind() == cc.Double:
		if to.IsIntegerType() {
			return q.double2Int(n, e, from, to, val)
		}

		switch to.Kind() {
		case cc.Float:
			return q.t("truncd %%t%[2]d\t\t# %v:", "s", e, q.pos(n))
		case cc.Double:
			return e
		case cc.LongDouble:
			return q.t("extd %%t%[2]d\t\t# %v:", q.typ(n, to), e, q.pos(n))
		}

		todo(n, e, from, to)
	case from.Kind() == cc.LongDouble:
		if to.IsIntegerType() {
			return q.longDouble2Int(n, e, from, to, val)
		}

		switch to.Kind() {
		case cc.Float, cc.Double:
			return q.t("truncld %%t%[2]d\t\t# %v:", q.typ(n, to), e, q.pos(n))
		case cc.LongDouble:
			return e
		}

		todo(n, e, from, to)
	case from.Kind() == cc.Array:
		switch to.Kind() {
		case cc.Array, cc.Ptr:
			return e
		}
		todo(n, e, from, to)
	case from.Kind() == cc.Void:
		return -1
	case kind(from) == cc.Struct:
		switch kind(to) {
		case cc.Array, cc.Struct:
			return e
		}
		todo(n, e, from, to, "struct -> scalar")
	case from.Kind() == cc.Union:
		switch to.Kind() {
		case cc.Array, cc.Union:
			return e
		}
		todo(n, e, from, to)
	case from.IsComplexType() && from.Kind() == to.Kind():
		return e
	default:
		todo(n, e, from, from.Kind(), to, to.Kind())
	}
	panic("internal error") //TODOOK
}

type signedSaturationLimit struct {
	fmin, fmax float64
	min, max   int64
}

type unsignedSaturationLimit struct {
	fmax float64
	max  uint64
}

var (
	signedSaturationLimits = [...]signedSaturationLimit{
		1: {math.Nextafter(math.MinInt32, 0), math.Nextafter(math.MaxInt32, 0), math.MinInt32, math.MaxInt32},
		2: {math.Nextafter(math.MinInt32, 0), math.Nextafter(math.MaxInt32, 0), math.MinInt32, math.MaxInt32},
		4: {math.Nextafter(math.MinInt32, 0), math.Nextafter(math.MaxInt32, 0), math.MinInt32, math.MaxInt32},
		8: {math.Nextafter(math.MinInt64, 0), math.Nextafter(math.MaxInt64, 0), math.MinInt64, math.MaxInt64},
	}

	unsignedSaturationLimits = [...]unsignedSaturationLimit{
		1: {math.Nextafter(math.MaxUint32, 0), math.MaxUint32},
		2: {math.Nextafter(math.MaxUint32, 0), math.MaxUint32},
		4: {math.Nextafter(math.MaxUint32, 0), math.MaxUint32},
		8: {math.Nextafter(math.MaxUint64, 0), math.MaxUint64},
	}
)

func (q *qbe) float2Int(n cc.Node, e int, from, to cc.Type, val cc.Value) int {
	switch {
	case to.IsSignedType():
		if val != nil {
			switch x := val.(type) {
			case cc.Float32Value:
				limits := &signedSaturationLimits[to.Size()]
				v := float64(x)
				switch {
				case math.IsNaN(v):
					todo(n)
				case math.IsInf(v, -1):
					todo(n)
				case math.IsInf(v, 1):
					todo(n)
				case v < limits.fmin:
					return q.t("copy %[2]d", q.typ(n, to), limits.min)
				case v > limits.fmax:
					return q.t("copy %[2]d", q.typ(n, to), limits.max)
				}
			default:
				todo(n, fmt.Sprintf("%T %v", x, x))
			}
		}

		return q.t("stosi %%t%[2]d", q.typ(n, to), e)
	default:
		if val != nil {
			switch x := val.(type) {
			case cc.Float32Value:
				limits := &unsignedSaturationLimits[to.Size()]
				v := float64(x)
				switch {
				case math.IsNaN(v):
					todo(n)
				case math.IsInf(v, -1):
					todo(n)
				case math.IsInf(v, 1):
					todo(n)
				case v < 0:
					return q.t("copy %[2]d", q.typ(n, to), 0)
				case v > limits.fmax:
					return q.t("copy %[2]d", q.typ(n, to), limits.max)
				}
			default:
				todo(n, fmt.Sprintf("%T %v", x, x))
			}
		}

		return q.t("stoui %%t%[2]d", q.typ(n, to), e)
	}
}

func (q *qbe) double2Int(n cc.Node, e int, from, to cc.Type, val cc.Value) int {
	switch {
	case to.IsSignedType():
		if val != nil {
			switch x := val.(type) {
			case cc.Float64Value:
				limits := &signedSaturationLimits[to.Size()]
				v := float64(x)
				switch {
				case math.IsNaN(v):
					todo(n)
				case math.IsInf(v, -1):
					todo(n)
				case math.IsInf(v, 1):
					todo(n)
				case v < limits.fmin:
					return q.t("copy %[2]d", q.typ(n, to), limits.min)
				case v > limits.fmax:
					return q.t("copy %[2]d", q.typ(n, to), limits.max)
				}
			default:
				todo(n, fmt.Sprintf("%T %v", x, x))
			}
		}

		return q.t("dtosi %%t%[2]d", q.typ(n, to), e)
	default:
		if val != nil {
			switch x := val.(type) {
			case cc.Float64Value:
				limits := &unsignedSaturationLimits[to.Size()]
				v := float64(x)
				switch {
				case math.IsNaN(v):
					todo(n)
				case math.IsInf(v, -1):
					todo(n)
				case math.IsInf(v, 1):
					todo(n)
				case v < 0:
					return q.t("copy %[2]d", q.typ(n, to), 0)
				case v > limits.fmax:
					return q.t("copy %[2]d", q.typ(n, to), limits.max)
				}
			default:
				todo(n, fmt.Sprintf("%T %v", x, x))
			}
		}

		return q.t("dtoui %%t%[2]d", q.typ(n, to), e)
	}
}

func (q *qbe) longDouble2Int(n cc.Node, e int, from, to cc.Type, val cc.Value) int {
	switch {
	case to.IsSignedType():
		if val != nil {
			switch x := val.(type) {
			case *cc.Float128Value:
				limits := &signedSaturationLimits[to.Size()]
				switch {
				case x.NaN:
					todo(n)
				default:
					switch v, _ := x.N.Float64(); {
					case math.IsInf(v, -1):
						todo(n)
					case math.IsInf(v, 1):
						todo(n)
					case v < limits.fmin:
						return q.t("copy %[2]d", q.typ(n, to), limits.min)
					case v > limits.fmax:
						return q.t("copy %[2]d", q.typ(n, to), limits.max)
					}
				}
			default:
				todo(n, fmt.Sprintf("%T %v", x, x))
			}
		}

		return q.t("ldtosi %%t%[2]d", q.typ(n, to), e)
	default:
		if val != nil {
			switch x := val.(type) {
			case *cc.Float128Value:
				limits := &unsignedSaturationLimits[to.Size()]
				switch {
				case x.NaN:
					todo(n)
				default:
					switch v, _ := x.N.Float64(); {
					case math.IsInf(v, -1):
						todo(n)
					case math.IsInf(v, 1):
						todo(n)
					case v < 0:
						return q.t("copy %[2]d", q.typ(n, to), 0)
					case v > limits.fmax:
						return q.t("copy %[2]d", q.typ(n, to), limits.max)
					}
				}
			default:
				todo(n, fmt.Sprintf("%T %v", x, x))
			}
		}

		return q.t("ldtoui %%t%[2]d", q.typ(n, to), e)
	}
}

func (q *qbe) convertFromInt(n cc.Node, e int, from, to cc.Type) int {
	to = to.Decay()
	src := from.Size()
	dst := to.Size()
	switch to.Kind() {
	case cc.ComplexFloat:
		todo(n, e, from, src, to, dst)
	case cc.ComplexDouble:
		todo(n, e, from, src, to, dst)
	case cc.ComplexLongDouble:
		todo(n, e, from, src, to, dst, "complex long double")
	case cc.Float, cc.Double, cc.LongDouble:
		if !from.IsSignedType() {
			switch src {
			case 1, 2, 4:
				switch to.Kind() {
				case cc.Float, cc.Double, cc.LongDouble:
					t := q.Config.ABI.Type(cc.LongLong)
					e = q.convert(n, e, from, t, nil)
					return q.t("ultof %%t%[2]d\t\t# %v:", q.typ(n, to), e, q.pos(n))
				default:
					todo(n, e, from, src, to, dst)
				}
			case 8:
				return q.t("ultof %%t%[2]d\t\t# %v:", q.typ(n, to), e, q.pos(n))
			default:
				todo(n, e, from, src, to, dst)
			}
		}

		switch src {
		case 1, 2, 4:
			switch to.Kind() {
			case cc.Float:
				e = q.convert(n, e, from, q.Config.ABI.Type(cc.Int), nil)
				return q.t("swtof %%t%[2]d\t\t# %v:", q.typ(n, to), e, q.pos(n))
			default:
				e = q.convert(n, e, from, q.Config.ABI.Type(cc.LongLong), nil)
				return q.t("sltof %%t%[2]d\t\t# %v:", q.typ(n, to), e, q.pos(n))
			}
		case 8:
			return q.t("sltof %%t%[2]d\t\t# %v:", q.typ(n, to), e, q.pos(n))
		default:
			todo(n, e, from, src, to, dst)
		}
	}

	if to.IsIntegerType() {
		switch {
		case src == dst:
			return e
		case src < dst:
			return q.t("ext%[2]s %%t%d\t\t# %v:", q.typ(n, to), q.extendType(from), e, q.pos(n))
		default:
			return q.t("and %%t%[2]d, %d\t\t# %v:", q.typ(n, to), e, uint64(1)<<uint(8*dst)-1, q.pos(n))
		}
	}

	if to.Decay().Kind() == cc.Ptr || to.Kind() == cc.Function {
		dst = q.ptrType.Size()
		switch {
		case src == dst:
			return e
		case src > dst:
			return q.t("copy %%t%[2]d\t\t# %v:", q.ptr, e, q.pos(n))
		default:
			return q.t("ext%[2]s %%t%d\t\t# %v:", q.ptr, q.extendType(from), e, q.pos(n))
		}
	}

	later(n, e, from, src, to, dst, "scalar -> struct")
	panic("internal error") //TODOOK
}

func (q *qbe) conditionalExpression(n *cc.ConditionalExpression, void bool) int {
	if void && n.IsSideEffectsFree {
		return -1
	}

	switch n.Case {
	case cc.ConditionalExpressionLOr: // LogicalOrExpression
		return q.logicalOrExpression(n.LogicalOrExpression, void)
	case cc.ConditionalExpressionCond: // LogicalOrExpression '?' Expression ':' ConditionalExpression
		switch {
		case n.Expression == nil:
			//	%e = expr
			// @a
			//	jnz %e, @z, @b
			// @b
			//	%e3 = expr3
			// @c
			//	jmp @z
			// @z
			//	r = phi @a %e, @c %e3
			a := q.label()
			b := q.label()
			c := q.label()
			z := q.label()
			if n.LogicalOrExpression.Operand.Type().IsComplexType() {
				todo(n)
			}

			e := q.logicalOrExpression(n.LogicalOrExpression, false)
			e = q.nonZero(n, e, n.LogicalOrExpression.Operand)
			q.w("@.%d\n", a)
			q.w("\tjnz %%t%d, @.%d, @.%d\n", e, z, b)
			q.w("@.%d\n", b)
			e3 := q.conditionalExpression(n.ConditionalExpression, void)
			if e3 > 0 {
				e3 = q.convert(n, e3, n.ConditionalExpression.Operand.Type(), n.Operand.Type(), nil)
			}
			q.w("@.%d\n", c)
			q.w("@.%d\n", z)
			if void {
				return -1
			}

			return q.t("phi @.%[2]d %%t%d, @.%d %%t%d", q.typ(n, n.Operand.Type()), a, e, c, e3)
		default:
			//	%e = expr
			// @a
			//	jnz %e, @b, @d
			// @b
			//	%e2 = expr2
			// @c
			//	jmp @z
			// @d
			//	e3 = expr3
			// @e
			// @z
			//	r = phi @c %e2, @e %e3
		}
		a := q.label()
		b := q.label()
		c := q.label()
		d := q.label()
		e := q.label()
		z := q.label()
		if n.LogicalOrExpression.Operand.Type().IsComplexType() {
			todo(n)
		}

		e0 := q.logicalOrExpression(n.LogicalOrExpression, false)
		e0 = q.nonZero(n, e0, n.LogicalOrExpression.Operand)
		q.w("@.%d\n", a)
		q.w("\tjnz %%t%d, @.%d, @.%d\n", e0, b, d)
		q.w("@.%d\n", b)
		e2 := q.expression(n.Expression, void)
		if e2 > 0 {
			e2 = q.convert(n, e2, n.Expression.Operand.Type(), n.Operand.Type(), nil)
		}
		q.w("@.%d\n", c)
		q.w("\tjmp @.%d\n", z)
		q.w("@.%d\n", d)
		e3 := q.conditionalExpression(n.ConditionalExpression, void)
		if e3 > 0 {
			e3 = q.convert(n, e3, n.ConditionalExpression.Operand.Type(), n.Operand.Type(), nil)
		}
		q.w("@.%d\n", e)
		q.w("@.%d\n", z)
		if void {
			return -1
		}

		return q.t("phi @.%[2]d %%t%d, @.%d %%t%d", q.typ(n, n.Operand.Type()), c, e2, e, e3)
	}
	todo(n, n.Case)
	panic("internal error") //TODOOK
}

func (q *qbe) logicalOrExpression(n *cc.LogicalOrExpression, void bool) int {
	if void && n.IsSideEffectsFree {
		return -1
	}

	switch n.Case {
	case cc.LogicalOrExpressionLAnd: // LogicalAndExpression
		return q.logicalAndExpression(n.LogicalAndExpression, void)
	case cc.LogicalOrExpressionLOr: // LogicalOrExpression "||" LogicalAndExpression
		//	%e1 = expr1
		// @a
		//	jnz %e1, @z, @b
		// @b
		//	%e2 = expr2
		// @c
		//	jnz %e2, @z, @d
		// @d
		// @z
		//	r = phi @a 1, @c 1, @d 0
		a := q.label()
		b := q.label()
		c := q.label()
		d := q.label()
		z := q.label()
		if n.LogicalOrExpression.Operand.Type().IsComplexType() {
			todo(n)
		}

		e1 := q.logicalOrExpression(n.LogicalOrExpression, false)
		e1 = q.nonZero(n, e1, n.LogicalOrExpression.Operand)
		q.w("@.%d\n", a)
		q.w("\tjnz %%t%d, @.%d, @.%d\n", e1, z, b)
		if n.LogicalAndExpression.Operand.Type().IsComplexType() {
			todo(n)
		}

		q.w("@.%d\n", b)
		e2 := q.logicalAndExpression(n.LogicalAndExpression, false)
		e2 = q.nonZero(n, e2, n.LogicalAndExpression.Operand)
		q.w("@.%d\n", c)
		q.w("\tjnz %%t%d, @.%d, @.%d\n", e2, z, d)
		q.w("@.%d\n", d)
		q.w("@.%d\n", z)
		if void {
			return -1
		}

		return q.t("phi @.%[2]d 1, @.%d 1, @.%d 0", q.typ(n, n.Operand.Type()), a, c, d)

	}
	panic("internal error") //TODOOK
}

func (q *qbe) logicalAndExpression(n *cc.LogicalAndExpression, void bool) int {
	if void && n.IsSideEffectsFree {
		return -1
	}

	switch n.Case {
	case cc.LogicalAndExpressionOr: // InclusiveOrExpression
		return q.inclusiveOrExpression(n.InclusiveOrExpression, void)
	case cc.LogicalAndExpressionLAnd: // LogicalAndExpression "&&" InclusiveOrExpression
		//	e1 = expr1
		// @a
		//	jnz e1, @b, @z
		// @b
		//	e2 = expr2
		// @c
		//	jnz e2 @d, @z
		// @d
		// @z
		//	r = phi @a 0, @c 0, @d 1
		a := q.label()
		b := q.label()
		c := q.label()
		d := q.label()
		z := q.label()
		if n.LogicalAndExpression.Operand.Type().IsComplexType() {
			todo(n)
		}

		e1 := q.logicalAndExpression(n.LogicalAndExpression, false)
		e1 = q.nonZero(n, e1, n.LogicalAndExpression.Operand)
		q.w("@.%d\n", a)
		q.w("\tjnz %%t%d, @.%d, @.%d\n", e1, b, z)
		if n.LogicalAndExpression.Operand.Type().IsComplexType() {
			todo(n)
		}

		q.w("@.%d\n", b)
		if n.InclusiveOrExpression.Operand.Type().IsComplexType() {
			todo(n)
		}

		e2 := q.inclusiveOrExpression(n.InclusiveOrExpression, false)
		e2 = q.nonZero(n, e2, n.InclusiveOrExpression.Operand)
		q.w("@.%d\n", c)
		q.w("\tjnz %%t%d, @.%d, @.%d\n", e2, d, z)
		q.w("@.%d\n", d)
		q.w("@.%d\n", z)
		if void {
			return -1
		}

		return q.t("phi @.%[2]d 0, @.%d 0, @.%d 1", q.typ(n, n.Operand.Type()), a, c, d)
	}
	panic("internal error") //TODOOK
}

func (q *qbe) inclusiveOrExpression(n *cc.InclusiveOrExpression, void bool) int {
	if void {
		if n.IsSideEffectsFree {
			return -1
		}

		if n.Case != cc.InclusiveOrExpressionXor {
			q.inclusiveOrExpression(n.InclusiveOrExpression, void)
			q.exclusiveOrExpression(n.ExclusiveOrExpression, void)
			return -1
		}
	}

	switch n.Case {
	case cc.InclusiveOrExpressionXor: // ExclusiveOrExpression
		return q.exclusiveOrExpression(n.ExclusiveOrExpression, void)
	case cc.InclusiveOrExpressionOr: // InclusiveOrExpression '|' ExclusiveOrExpression
		lhs := q.inclusiveOrExpression(n.InclusiveOrExpression, void)
		lhs = q.convert(n, lhs, n.InclusiveOrExpression.Operand.Type(), n.Operand.Type(), nil)
		rhs := q.exclusiveOrExpression(n.ExclusiveOrExpression, void)
		rhs = q.convert(n, rhs, n.ExclusiveOrExpression.Operand.Type(), n.Operand.Type(), nil)
		return q.t("or %%t%[2]d, %%t%d\t\t# %v:", q.typ(n, n.Operand.Type()), lhs, rhs, q.pos(&n.Token))
	}
	panic("internal error") //TODOOK
}

func (q *qbe) exclusiveOrExpression(n *cc.ExclusiveOrExpression, void bool) int {
	if void {
		if n.IsSideEffectsFree {
			return -1
		}

		if n.Case != cc.ExclusiveOrExpressionAnd {
			q.exclusiveOrExpression(n.ExclusiveOrExpression, void)
			q.andExpression(n.AndExpression, void)
			return -1
		}
	}

	switch n.Case {
	case cc.ExclusiveOrExpressionAnd: // AndExpression
		return q.andExpression(n.AndExpression, void)
	case cc.ExclusiveOrExpressionXor: // ExclusiveOrExpression '^' AndExpression
		lhs := q.exclusiveOrExpression(n.ExclusiveOrExpression, void)
		lhs = q.convert(n, lhs, n.ExclusiveOrExpression.Operand.Type(), n.Operand.Type(), nil)
		rhs := q.andExpression(n.AndExpression, void)
		rhs = q.convert(n, rhs, n.AndExpression.Operand.Type(), n.Operand.Type(), nil)
		return q.t("xor %%t%[2]d, %%t%d\t\t# %v:", q.typ(n, n.Operand.Type()), lhs, rhs, q.pos(&n.Token))
	}
	panic("internal error") //TODOOK
}

func (q *qbe) andExpression(n *cc.AndExpression, void bool) int {
	if void {
		if n.IsSideEffectsFree {
			return -1
		}

		if n.Case != cc.AndExpressionEq {
			q.andExpression(n.AndExpression, void)
			q.equalityExpression(n.EqualityExpression, void)
			return -1
		}
	}

	switch n.Case {
	case cc.AndExpressionEq: // EqualityExpression
		return q.equalityExpression(n.EqualityExpression, void)
	case cc.AndExpressionAnd: // AndExpression '&' EqualityExpression
		lhs := q.andExpression(n.AndExpression, void)
		lhs = q.convert(n, lhs, n.AndExpression.Operand.Type(), n.Operand.Type(), nil)
		rhs := q.equalityExpression(n.EqualityExpression, void)
		rhs = q.convert(n, rhs, n.EqualityExpression.Operand.Type(), n.Operand.Type(), nil)
		return q.t("and %%t%[2]d, %%t%d\t\t# %v:", q.typ(n, n.Operand.Type()), lhs, rhs, q.pos(&n.Token))
	}
	panic("internal error") //TODOOK
}

func (q *qbe) equalityExpression(n *cc.EqualityExpression, void bool) int {
	if void {
		if n.IsSideEffectsFree {
			return -1
		}

		if n.Case != cc.EqualityExpressionRel {
			q.equalityExpression(n.EqualityExpression, void)
			q.relationalExpression(n.RelationalExpression, void)
			return -1
		}
	}

	cplx := n.Promote() != nil && n.Promote().IsComplexType()
	var op string
	switch n.Case {
	case cc.EqualityExpressionRel: // RelationalExpression
		return q.relationalExpression(n.RelationalExpression, void)
	case cc.EqualityExpressionEq: // EqualityExpression "==" RelationalExpression
		if cplx {
			todo(n)
		}

		op = "eq"
	case cc.EqualityExpressionNeq: // EqualityExpression "!=" RelationalExpression
		if cplx {
			return q.complexNeq(n)
		}

		op = "ne"
	default:
		panic("internal error") //TODOOK
	}
	switch kind(n.Promote()) {
	case cc.Array:
		todo(n)
	case cc.Struct:
		todo(n, n.Promote())
	case cc.Union:
		todo(n, n.Promote())
	}

	lhs := q.equalityExpression(n.EqualityExpression, void)
	lhs = q.convert(n, lhs, n.EqualityExpression.Operand.Type(), n.Promote(), nil)
	rhs := q.relationalExpression(n.RelationalExpression, void)
	rhs = q.convert(n, rhs, n.RelationalExpression.Operand.Type(), n.Promote(), nil)
	return q.t("c%[2]s%s %%t%d, %%t%d\t\t# %v:", "w", op, q.typ(n, n.Promote()), lhs, rhs, q.pos(&n.Token)) //TODO "w"->.int
}

func (q *qbe) complexNeq(n *cc.EqualityExpression) int {
	// EqualityExpression "!=" RelationalExpression
	re, im := q.complexEqualityExpression(n.EqualityExpression)
	re, im = q.convertToComplex(n, re, im, n.EqualityExpression.Operand.Type(), n.Promote())
	re2, im2 := q.complexRelationalExpression(n.RelationalExpression)
	re2, im2 = q.convertToComplex(n, re2, im2, n.RelationalExpression.Operand.Type(), n.Promote())
	// r = real != real
	// jnz r, @z, @a
	// @a
	// r = imag != imag
	// @z
	a := q.label()
	z := q.label()
	r := q.t("cne%[2]s %%t%d, %%t%d\t\t# %v:", "w", q.typ(n, n.Promote().Real().Type()), re, re2, q.pos(&n.Token)) //TODO "w"->.int
	q.w("\tjnz %%t%d, @.%d, @.%d\n", r, z, a)
	q.w("@.%d\n", a)
	q.tn("cne%[2]s %%t%d, %%t%d\t\t# %v:", r, "w", q.typ(n, n.Promote().Imag().Type()), im, im2, q.pos(&n.Token)) //TODO "w"->.int
	q.w("@.%d\n", z)
	return r
}

func (q *qbe) complexEqualityExpression(n *cc.EqualityExpression) (int, int) {
	switch n.Case {
	case cc.EqualityExpressionRel: // RelationalExpression
		return q.complexRelationalExpression(n.RelationalExpression)
	case cc.EqualityExpressionEq: // EqualityExpression "==" RelationalExpression
		todo(n)
	case cc.EqualityExpressionNeq: // EqualityExpression "!=" RelationalExpression
		todo(n)
	}
	panic(internalErrorf("%v", n.Case))
}

func (q *qbe) complexRelationalExpression(n *cc.RelationalExpression) (int, int) {
	switch n.Case {
	case cc.RelationalExpressionShift: // ShiftExpression
		return q.complexShiftExpression(n.ShiftExpression)
	case cc.RelationalExpressionLt: // RelationalExpression '<' ShiftExpression
		todo(n)
	case cc.RelationalExpressionGt: // RelationalExpression '>' ShiftExpression
		todo(n)
	case cc.RelationalExpressionLeq: // RelationalExpression "<=" ShiftExpression
		todo(n)
	case cc.RelationalExpressionGeq: // RelationalExpression ">=" ShiftExpression
		todo(n)
	}
	panic(internalErrorf("%v", n.Case))
}

func (q *qbe) complexShiftExpression(n *cc.ShiftExpression) (int, int) {
	switch n.Case {
	case cc.ShiftExpressionAdd: // AdditiveExpression
		return q.complexAdditiveExpression(n.AdditiveExpression)
	case cc.ShiftExpressionLsh: // ShiftExpression "<<" AdditiveExpression
		todo(n)
	case cc.ShiftExpressionRsh: // ShiftExpression ">>" AdditiveExpression
		todo(n)
	}
	panic(internalErrorf("%v", n.Case))
}

func (q *qbe) complexAdditiveExpression(n *cc.AdditiveExpression) (int, int) {
	switch n.Case {
	case cc.AdditiveExpressionMul: // MultiplicativeExpression
		return q.complexMultiplicativeExpression(n.MultiplicativeExpression)
	case cc.AdditiveExpressionAdd: // AdditiveExpression '+' MultiplicativeExpression
		re, im := q.complexAdditiveExpression(n.AdditiveExpression)
		re, im = q.convertToComplex(n, re, im, n.AdditiveExpression.Operand.Type(), n.Operand.Type())
		re2, im2 := q.complexMultiplicativeExpression(n.MultiplicativeExpression)
		re2, im2 = q.convertToComplex(n, re2, im2, n.MultiplicativeExpression.Operand.Type(), n.Operand.Type())
		re = q.t("add %%t%[2]d, %%t%d", q.typ(n, n.Operand.Type().Real().Type()), re, re2)
		im = q.t("add %%t%[2]d, %%t%d", q.typ(n, n.Operand.Type().Imag().Type()), im, im2)
		return re, im
	case cc.AdditiveExpressionSub: // AdditiveExpression '-' MultiplicativeExpression
		re, im := q.complexAdditiveExpression(n.AdditiveExpression)
		re, im = q.convertToComplex(n, re, im, n.AdditiveExpression.Operand.Type(), n.Operand.Type())
		re2, im2 := q.complexMultiplicativeExpression(n.MultiplicativeExpression)
		re2, im2 = q.convertToComplex(n, re2, im2, n.MultiplicativeExpression.Operand.Type(), n.Operand.Type())
		re = q.t("sub %%t%[2]d, %%t%d", q.typ(n, n.Operand.Type().Real().Type()), re, re2)
		im = q.t("sub %%t%[2]d, %%t%d", q.typ(n, n.Operand.Type().Imag().Type()), im, im2)
		return re, im
	}
	panic(internalErrorf("%v", n.Case))
}

func (q *qbe) complexMultiplicativeExpression(n *cc.MultiplicativeExpression) (int, int) {
	switch n.Case {
	case cc.MultiplicativeExpressionCast: // CastExpression
		return q.complexCastExpression(n.CastExpression)
	case cc.MultiplicativeExpressionMul: // MultiplicativeExpression '*' CastExpression
		re, im := q.complexMultiplicativeExpression(n.MultiplicativeExpression)
		re, im = q.convertToComplex(n, re, im, n.MultiplicativeExpression.Operand.Type(), n.Operand.Type())
		re2, im2 := q.complexCastExpression(n.CastExpression)
		re2, im2 = q.convertToComplex(n, re2, im2, n.CastExpression.Operand.Type(), n.Operand.Type())
		// (re+im·i)(re2+im2·i) = (re·re2 - im·im2) + (re·im2 + im·re2)·i
		a := q.t("mul %%t%[2]d, %%t%d\t\t# %v:", q.typ(n, n.Operand.Type().Real().Type()), re, re2, q.pos(&n.Token))
		b := q.t("mul %%t%[2]d, %%t%d", q.typ(n, n.Operand.Type().Real().Type()), im, im2)
		r := q.t("sub %%t%[2]d, %%t%d", q.typ(n, n.Operand.Type().Real().Type()), a, b)
		a = q.t("mul %%t%[2]d, %%t%d\t\t# %v:", q.typ(n, n.Operand.Type().Real().Type()), re, im2, q.pos(&n.Token))
		b = q.t("mul %%t%[2]d, %%t%d", q.typ(n, n.Operand.Type().Real().Type()), im, re2)
		i := q.t("add %%t%[2]d, %%t%d", q.typ(n, n.Operand.Type().Real().Type()), a, b)
		return r, i
	case cc.MultiplicativeExpressionDiv: // MultiplicativeExpression '/' CastExpression
		re, im := q.complexMultiplicativeExpression(n.MultiplicativeExpression)
		re, im = q.convertToComplex(n, re, im, n.MultiplicativeExpression.Operand.Type(), n.Operand.Type())
		re2, im2 := q.complexCastExpression(n.CastExpression)
		re2, im2 = q.convertToComplex(n, re2, im2, n.CastExpression.Operand.Type(), n.Operand.Type())
		// (re+im·i)/(re2+im2·i) = (re·re2+im·im2)/(re2^2+im2^2)+(im·re2-re·im2)/(re2^2+im2^2)·i
		c2 := q.t("mul %%t%[2]d, %%t%d\t\t# %v:", q.typ(n, n.Operand.Type().Real().Type()), re2, re2, q.pos(&n.Token))
		d2 := q.t("mul %%t%[2]d, %%t%d\t\t# %v:", q.typ(n, n.Operand.Type().Imag().Type()), im2, im2, q.pos(&n.Token))
		de := q.t("add %%t%[2]d, %%t%d", q.typ(n, n.Operand.Type().Real().Type()), c2, d2)
		ac := q.t("mul %%t%[2]d, %%t%d\t\t# %v:", q.typ(n, n.Operand.Type().Real().Type()), re, re2, q.pos(&n.Token))
		bd := q.t("mul %%t%[2]d, %%t%d\t\t# %v:", q.typ(n, n.Operand.Type().Real().Type()), im, im2, q.pos(&n.Token))
		no := q.t("add %%t%[2]d, %%t%d", q.typ(n, n.Operand.Type().Real().Type()), ac, bd)
		op := "div"
		if t := n.Operand.Type().Real().Type(); t.IsIntegerType() && !t.IsSignedType() {
			op = "udiv"
		}
		r := q.t("%[2]s %%t%d, %%t%d\t\t# %v:", q.typ(n, n.Operand.Type().Real().Type()), op, no, de, q.pos(&n.Token))
		bc := q.t("mul %%t%[2]d, %%t%d\t\t# %v:", q.typ(n, n.Operand.Type().Real().Type()), im, re2, q.pos(&n.Token))
		ad := q.t("mul %%t%[2]d, %%t%d\t\t# %v:", q.typ(n, n.Operand.Type().Real().Type()), re, im2, q.pos(&n.Token))
		no = q.t("sub %%t%[2]d, %%t%d", q.typ(n, n.Operand.Type().Real().Type()), bc, ad)
		i := q.t("%[2]s %%t%d, %%t%d\t\t# %v:", q.typ(n, n.Operand.Type().Real().Type()), op, no, de, q.pos(&n.Token))
		return r, i
	case cc.MultiplicativeExpressionMod: // MultiplicativeExpression '%' CastExpression
		todo(n)
	}
	panic(internalErrorf("%v", n.Case))
}

func (q *qbe) convertToComplex(n cc.Node, re, im int, from, to cc.Type) (int, int) {
	if from.IsComplexType() {
		if from.Kind() == to.Kind() {
			return re, im
		}

		re = q.convert(n, re, from.Real().Type(), to.Real().Type(), nil)
		im = q.convert(n, im, from.Imag().Type(), to.Imag().Type(), nil)
		return re, im
	}

	if from.IsIntegerType() {
		re = q.convert(n, re, from, to.Real().Type(), nil)
		im := q.t("copy 0\t# %[2]v", q.typ(n, to.Imag().Type()), q.pos(n))
		return re, im
	}

	switch from.Kind() {
	case cc.Double, cc.Float:
		re = q.convert(n, re, from, to.Real().Type(), nil)
		im := q.t("copy 0\t# %[2]v", q.typ(n, to.Imag().Type()), q.pos(n))
		return re, im
	}

	todo(n, re, im, from, to)
	panic("TODO")
}

func (q *qbe) complexCastExpression(n *cc.CastExpression) (int, int) {
	switch n.Case {
	case cc.CastExpressionUnary: // UnaryExpression
		return q.complexUnaryExpression(n.UnaryExpression)
	case cc.CastExpressionCast: // '(' TypeName ')' CastExpression
		switch from := n.CastExpression.Operand.Type(); {
		case from.IsComplexType():
			todo(n)
		default:
			switch to := n.TypeName.Type(); {
			case to.IsComplexType():
				re := q.castExpression(n.CastExpression, false) //TODO void
				return q.convertToComplex(n, re, 0, from, to)
			default:
				todo(n)
			}
		}
	}
	panic(internalErrorf("%v", n.Case))
}

func (q *qbe) complexUnaryExpression(n *cc.UnaryExpression) (int, int) {
	switch n.Case {
	case cc.UnaryExpressionPostfix: // PostfixExpression
		switch {
		case n.Operand.Type().IsComplexType():
			return q.complexPostfixExpression(n.PostfixExpression)
		default:
			return q.unaryExpression(n, false), 0 //TODO void
		}
		// case cc.UnaryExpressionInc: // "++" UnaryExpression
		// 	return q.preIncDec(n, "add")
		// case cc.UnaryExpressionDec: // "--" UnaryExpression
		// 	return q.preIncDec(n, "sub")
		// case cc.UnaryExpressionAddrof: // '&' CastExpression
		// 	//TODO check non nil op.Declarator()
		// 	return q.castExpressionAddrOf(n.CastExpression)
	case cc.UnaryExpressionDeref: // '*' CastExpression
		switch t := n.Operand.Type(); {
		case t.IsComplexType():
			p := q.castExpression(n.CastExpression, false) //TODO void
			re := q.t("add %%t%[2]d, %d", q.ptr, p, t.Real().Offset())
			re = q.load(n, re, nil, t.Real().Type())
			im := q.t("add %%t%[2]d, %d", q.ptr, p, t.Imag().Offset())
			im = q.load(n, im, nil, t.Imag().Type())
			return re, im
		default:
			todo(n)
		}
		// 	p := q.castExpression(n.CastExpression)
		// 	return q.load(n, p, n.Operand.Declarator(), n.Operand.Type())
		// case cc.UnaryExpressionPlus: // '+' CastExpression
		// 	t := q.castExpression(n.CastExpression)
		// 	return q.convert(n, t, n.CastExpression.Operand.Type(), n.Operand.Type())
	case cc.UnaryExpressionMinus: // '-' CastExpression
		switch {
		case n.Operand.Type().IsComplexType():
			todo(n)
		default:
			return q.unaryExpression(n, false), 0 //TODO void
		}
		// 	switch n.CastExpression.Operand.Type().Kind() {
		// 	case cc.Float:
		// 		return q.t("mul s_-1, %%t%[2]d\t\t# %v:", q.typ(n, n.Operand.Type()), t, q.pos(n))
		// 	case cc.Double, cc.LongDouble:
		// 		return q.t("mul d_-1, %%t%[2]d\t\t# %v:", q.typ(n, n.Operand.Type()), t, q.pos(n))
		// 	default:
		// 		return q.t("sub 0, %%t%[2]d\t\t# %v:", q.typ(n, n.Operand.Type()), t, q.pos(n))
		// 	}
	case cc.UnaryExpressionCpl: // '~' CastExpression
		switch {
		case n.Operand.Type().IsComplexType():
			re, im := q.complexCastExpression(n.CastExpression)
			re, im = q.convertToComplex(n, re, im, n.CastExpression.Operand.Type(), n.Operand.Type())
			switch t := n.Operand.Type().Imag().Type(); t.Kind() {
			case cc.Float:
				return re, q.t("mul s_-1, %%t%[2]d\t\t# %v:", q.typ(n, t), im, q.pos(n))
			case cc.Double:
				return re, q.t("mul d_-1, %%t%[2]d\t\t# %v:", q.typ(n, t), im, q.pos(n))
			case cc.LongDouble:
				todo(n)
			default:
				if t.IsIntegerType() {
					return re, q.t("mul -1, %%t%[2]d\t\t# %v:", q.typ(n, t), im, q.pos(n))
				}
				todo(n, re, im, t)
			}
		default:
			todo(n)
		}
		// 	t := q.castExpression(n.CastExpression)
		// 	t = q.convert(n, t, n.CastExpression.Operand.Type(), n.Operand.Type())
		// 	return q.t("xor %%t%[2]d, -1\t\t# %v:", q.typ(n, n.Operand.Type()), t, q.pos(n))
		// case cc.UnaryExpressionNot: // '!' CastExpression
		// 	// r = 0
		// 	// if expr1 != 0 goto z else goto a
		// 	// @a
		// 	// r = 1
		// 	// @z
		// 	a := q.label()
		// 	z := q.label()
		// 	r := q.t("copy 0\t\t# %[2]v:", q.typ(n, n.Operand.Type()), q.pos(n))
		// 	e := q.castExpression(n.CastExpression)
		// 	q.w("\tjnz %%t%d, @.%d, @.%d\n", e, z, a)
		// 	q.w("@.%d\n", a)
		// 	q.tn("copy %[2]d", r, q.typ(n, n.Operand.Type()), 1)
		// 	q.w("@.%d\n", z)
		// 	return r
		// case cc.UnaryExpressionSizeofExpr: // "sizeof" UnaryExpression
		// 	if t := n.UnaryExpression.Operand.Type(); t.Kind() == cc.Array && t.IsVLA() {
		// 		todo(n)
		// 		todo(n)
		// 	}

		// 	return q.t("copy %[2]v\t\t# %v:", q.typ(n, n.Operand.Type()), n.Operand.Value(), q.pos(n))
		// case cc.UnaryExpressionSizeofType: // "sizeof" '(' TypeName ')'
		// 	if v := n.Operand.Value(); v != nil {
		// 		return q.t("copy %[2]v\t\t# %v:", q.typ(n, n.Operand.Type()), v, q.pos(n))
		// 	}

		// 	todo(n)
		// case cc.UnaryExpressionLabelAddr: // "&&" IDENTIFIER
		// 	todo(n)
		// case cc.UnaryExpressionAlignofExpr: // "_Alignof" UnaryExpression
		// 	return q.t("copy %[2]d\t\t# %v:", q.typ(n, n.Operand.Type()), n.Operand.Value(), q.pos(n))
		// case cc.UnaryExpressionAlignofType: // "_Alignof" '(' TypeName ')'
		// 	return q.t("copy %[2]d\t\t# %v:", q.typ(n, n.Operand.Type()), n.Operand.Value(), q.pos(n))
		// case cc.UnaryExpressionImag: // "__imag__" UnaryExpression
		// 	todo(n)
		// case cc.UnaryExpressionReal: // "__real__" UnaryExpression
		// 	todo(n)
	}
	todo(n, n.Case)
	panic(internalErrorf("%v", n.Case))
}

func (q *qbe) complexPostfixExpression(n *cc.PostfixExpression) (int, int) {
	switch n.Case {
	case cc.PostfixExpressionPrimary: // PrimaryExpression
		return q.complexPrimaryExpression(n.PrimaryExpression)
		// case cc.PostfixExpressionIndex: // PostfixExpression '[' Expression ']'
		// 	//TODO check non-nil value
		// 	switch {
		// 	case n.Expression.Operand.Type().IsIntegerType():
		// 		e := q.expression(n.Expression)
		// 		e = q.convert(n, e, n.Expression.Operand.Type(), q.ptrType)
		// 		e = q.t("mul %%t%[2]d, %d\t\t# %v:", q.ptr, e, n.Operand.Type().Size(), q.pos(&n.Token))
		// 		p := q.postfixExpression(n.PostfixExpression)
		// 		p = q.t("add %%t%[2]d, %%t%d", q.ptr, p, e)
		// 		return q.load(&n.Token, p, n.Operand.Declarator(), n.Operand.Type())
		// 	default:
		// 		todo(n)
		// 	}
	case cc.PostfixExpressionCall: // PostfixExpression '(' ArgumentExpressionList ')'
		if r := q.constVal(n, n.Operand); r != 0 {
			todo(n)
		}

		p := q.call(n)
		switch t := n.Operand.Type(); {
		case t.IsComplexType():
			re := q.t("add %%t%[2]d, %d", q.ptr, p, t.Real().Offset())
			re = q.load(n, re, nil, t.Real().Type())
			im := q.t("add %%t%[2]d, %d", q.ptr, p, t.Imag().Offset())
			im = q.load(n, im, nil, t.Imag().Type())
			return re, im
		default:
			todo(n, p)
		}
	case cc.PostfixExpressionSelect: // PostfixExpression '.' IDENTIFIER
		p := q.postfixExpressionPtr(n.PostfixExpression)
		p = q.t("add %%t%[2]d, %d\t\t# %v: .%s", q.ptr, p, n.Field.Offset(), q.pos(&n.Token), n.Token2.Value)
		switch t := n.Operand.Type(); {
		case t.IsComplexType():
			re := q.t("add %%t%[2]d, %d", q.ptr, p, t.Real().Offset())
			re = q.load(n, re, nil, t.Real().Type())
			im := q.t("add %%t%[2]d, %d", q.ptr, p, t.Imag().Offset())
			im = q.load(n, im, nil, t.Imag().Type())
			return re, im
		default:
			todo(n)
		}
	// 	return q.load(&n.Token, p, n.Operand.Declarator(), n.Operand.Type())
	case cc.PostfixExpressionPSelect: // PostfixExpression "->" IDENTIFIER
		p := q.postfixExpression(n.PostfixExpression, false) //TODO void
		p = q.t("add %%t%[2]d, %d\t\t# %v:", q.ptr, p, n.Field.Offset(), q.pos(&n.Token))
		switch t := n.Operand.Type(); {
		case t.IsComplexType():
			re := q.t("add %%t%[2]d, %d", q.ptr, p, t.Real().Offset())
			re = q.load(n, re, nil, t.Real().Type())
			im := q.t("add %%t%[2]d, %d", q.ptr, p, t.Imag().Offset())
			im = q.load(n, im, nil, t.Imag().Type())
			return re, im
		default:
			todo(n)
		}
		// 	return q.load(&n.Token, p, n.Operand.Declarator(), n.Operand.Type())
		// case cc.PostfixExpressionInc: // PostfixExpression "++"
		// 	return q.postIncDec(n, "add")
		// case cc.PostfixExpressionDec: // PostfixExpression "--"
		// 	return q.postIncDec(n, "sub")
		// case cc.PostfixExpressionComplit: // '(' TypeName ')' '{' InitializerList ',' '}'
		// 	t := n.TypeName.Type()
		// 	if q.fn == nil {
		// 		todo(n)
		// 	}

		// 	switch kind(t) {
		// 	case cc.Array, cc.Struct, cc.Union:
		// 		return q.postfixExpressionPtr(n)
		// 	default:
		// 		todo(n)
		// 	}
		// case cc.PostfixExpressionTypeCmp: // "__builtin_types_compatible_p" '(' TypeName ',' TypeName ')'
		// 	todo(n)
	}
	todo(n, n.Case)
	panic(internalErrorf("%v", n.Case))
}

func (q *qbe) complexPrimaryExpression(n *cc.PrimaryExpression) (int, int) {
	switch n.Case {
	case cc.PrimaryExpressionIdent: // IDENTIFIER
		d := n.Operand.Declarator()
		t := d.Type()
		switch {
		case t.IsComplexType():
			p := q.declaratorPtr(n, n.Operand)
			pp := q.t("add %%t%[2]d, %d", q.ptr, p, t.Real().Offset())
			re := q.load(n, pp, nil, t.Real().Type())
			pp = q.t("add %%t%[2]d, %d", q.ptr, p, t.Imag().Offset())
			im := q.load(n, pp, nil, t.Imag().Type())
			return re, im
		default:
			return q.declarator(n, n.Operand), 0
		}
	case cc.PrimaryExpressionInt: // INTCONST
		return q.t("copy %[2]v\t\t# %v: %v", q.typ(n, n.Operand.Type()), n.Operand.Value(), q.pos(n), n.Operand.Type()), 0
	case cc.PrimaryExpressionFloat: // FLOATCONST
		//var v uint64
		switch x := n.Operand.Value().(type) {
		// case cc.Float32Value:
		// 	v = uint64(math.Float32bits(float32(x)))
		case cc.Float64Value:
			return q.t("copy %[2]v\t\t# %v: %v", q.typ(n, n.Operand.Type()), math.Float64bits(float64(x)), q.pos(n), n.Operand.Value()), 0
		case cc.Complex64Value:
			re := q.t("copy %[2]v\t\t# %v: %v", q.typ(n, n.Operand.Type().Real().Type()), math.Float32bits(real(x)), q.pos(n), n.Operand.Value())
			im := q.t("copy %[2]v", q.typ(n, n.Operand.Type().Imag().Type()), math.Float32bits(imag(x)))
			return re, im
		case cc.Complex128Value:
			re := q.t("copy %[2]v\t\t# %v: %v", q.typ(n, n.Operand.Type().Real().Type()), math.Float64bits(real(x)), q.pos(n), n.Operand.Value())
			im := q.t("copy %[2]v", q.typ(n, n.Operand.Type().Imag().Type()), math.Float64bits(imag(x)))
			return re, im
		default:
			todo(n, fmt.Sprintf("%T", x))
		}
		//return q.t("copy %[2]v\t\t# %v: %v", q.typ(n, n.Operand.Type()), v, q.pos(n), n.Operand.Value())
		// case cc.PrimaryExpressionEnum: // ENUMCONST
		// 	return q.t("copy %[2]v\t\t# %v:", q.typ(n, n.Operand.Type()), n.Operand.Value(), q.pos(n))
		// case cc.PrimaryExpressionChar: // CHARCONST
		// 	return q.t("copy %[2]v\t\t# %v:", q.typ(n, n.Operand.Type()), n.Operand.Value(), q.pos(n))
		// case cc.PrimaryExpressionLChar: // LONGCHARCONST
		// 	return q.t("copy %[2]v\t\t# %v:", q.typ(n, n.Operand.Type()), n.Operand.Value(), q.pos(n))
		// case cc.PrimaryExpressionString: // STRINGLITERAL
		// 	return q.t("copy $.%[2]d\t\t# %v: %s", q.ptr, q.string(n.Token.Value), q.pos(n), q.strComment(n.Token.Value))
		// case cc.PrimaryExpressionLString: // LONGSTRINGLITERAL
		// 	return q.t("copy $.%[2]d\t\t# %v: %s", q.ptr, q.string(-n.Token.Value), q.pos(n), q.strComment(n.Token.Value))
		// case cc.PrimaryExpressionExpr: // '(' Expression ')'
		// 	return q.expression(n.Expression)
		// case cc.PrimaryExpressionStmt: // '(' CompoundStatement ')'
		// 	return q.compoundStatementLeave(n.CompoundStatement)
	}
	todo(n, n.Case)
	panic(internalErrorf("%v", n.Case))
}

func (q *qbe) relationalExpression(n *cc.RelationalExpression, void bool) int {
	if void {
		if n.IsSideEffectsFree {
			return -1
		}

		if n.Case != cc.RelationalExpressionShift {
			q.relationalExpression(n.RelationalExpression, void)
			q.shiftExpression(n.ShiftExpression, void)
			return -1
		}
	}

	var op string
	switch n.Case {
	case cc.RelationalExpressionShift: // ShiftExpression
		return q.shiftExpression(n.ShiftExpression, void)
	case cc.RelationalExpressionLt: // RelationalExpression '<' ShiftExpression
		op = "lt"
	case cc.RelationalExpressionGt: // RelationalExpression '>' ShiftExpression
		op = "gt"
	case cc.RelationalExpressionLeq: // RelationalExpression "<=" ShiftExpression
		op = "le"
	case cc.RelationalExpressionGeq: // RelationalExpression ">=" ShiftExpression
		op = "ge"
	default:
		todo(n)
		panic("internal error") //TODOOK
	}

	lhs := q.relationalExpression(n.RelationalExpression, void)
	lhs = q.convert(n, lhs, n.RelationalExpression.Operand.Type(), n.Promote(), nil)
	rhs := q.shiftExpression(n.ShiftExpression, void)
	rhs = q.convert(n, rhs, n.ShiftExpression.Operand.Type(), n.Promote(), nil)
	switch {
	case n.Promote().IsIntegerType():
		switch {
		case n.Promote().IsSignedType():
			op = "s" + op
		default:
			op = "u" + op
		}
		return q.t("c%[2]s%s %%t%d, %%t%d\t\t# %v:", q.typ(n, n.Operand.Type()), op, q.typ(n, n.Promote()), lhs, rhs, q.pos(&n.Token))
	case n.Promote().Kind() == cc.Ptr:
		return q.t("cu%[2]s%s %%t%d, %%t%d\t\t# %v:", q.typ(n, n.Operand.Type()), op, q.typ(n, n.Promote()), lhs, rhs, q.pos(&n.Token))
	case n.Promote().Kind() == cc.Double, n.Promote().Kind() == cc.Float, n.Promote().Kind() == cc.LongDouble:
		return q.t("c%[2]s%s %%t%d, %%t%d\t\t# %v:", q.typ(n, n.Operand.Type()), op, q.typ(n, n.Promote()), lhs, rhs, q.pos(&n.Token))
	}
	todo(n, lhs, rhs, n.Promote(), op)
	panic("internal error") //TODOOK
}

func (q *qbe) shiftExpression(n *cc.ShiftExpression, void bool) (r int) {
	if void {
		if n.IsSideEffectsFree {
			return -1
		}

		if n.Case != cc.ShiftExpressionAdd {
			q.shiftExpression(n.ShiftExpression, void)
			q.additiveExpression(n.AdditiveExpression, void)
			return -1
		}
	}

	switch n.Case {
	case cc.ShiftExpressionAdd: // AdditiveExpression
		return q.additiveExpression(n.AdditiveExpression, void)
	case cc.ShiftExpressionLsh: // ShiftExpression "<<" AdditiveExpression
		lhs := q.shiftExpression(n.ShiftExpression, void)
		lhs = q.convert(n, lhs, n.ShiftExpression.Operand.Type(), n.Operand.Type(), nil)
		rhs := q.additiveExpression(n.AdditiveExpression, void)
		rhs = q.t("and %[2]d, %%t%d", q.typ(n, n.Promote()), q.shiftMask(n.ShiftExpression.Operand.Type()), rhs)
		r = q.t("shl %%t%[2]d, %%t%d", q.typ(n, n.ShiftExpression.Operand.Type()), lhs, rhs)
	case cc.ShiftExpressionRsh: // ShiftExpression ">>" AdditiveExpression
		lhs := q.shiftExpression(n.ShiftExpression, void)
		lhs = q.convert(n, lhs, n.ShiftExpression.Operand.Type(), n.Operand.Type(), nil)
		rhs := q.additiveExpression(n.AdditiveExpression, void)
		rhs = q.t("and %[2]d, %%t%d", q.typ(n, n.Promote()), q.shiftMask(n.ShiftExpression.Operand.Type()), rhs)
		if n.ShiftExpression.Operand.Type().IsSignedType() {
			return q.t("sar %%t%[2]d, %%t%d", q.typ(n, n.ShiftExpression.Operand.Type()), lhs, rhs)
		}

		r = q.t("shr %%t%[2]d, %%t%d", q.typ(n, n.ShiftExpression.Operand.Type()), lhs, rhs)
	}
	return q.reduce(r, n.ShiftExpression.Operand.Type(), n.ShiftExpression.Operand.Type(), nil)
}

func (q *qbe) additiveExpression(n *cc.AdditiveExpression, void bool) int {
	if void {
		if n.IsSideEffectsFree {
			return -1
		}

		if n.Case != cc.AdditiveExpressionMul {
			q.additiveExpression(n.AdditiveExpression, void)
			q.multiplicativeExpression(n.MultiplicativeExpression, void)
			return -1
		}
	}

	var mulL, mulR uintptr = 1, 1
	var op string
	switch n.Case {
	case cc.AdditiveExpressionMul: // MultiplicativeExpression
		return q.multiplicativeExpression(n.MultiplicativeExpression, void)
	case cc.AdditiveExpressionAdd: // AdditiveExpression '+' MultiplicativeExpression
		op = "add"
		if n.AdditiveExpression.Operand.Type().IsRealType() && n.MultiplicativeExpression.Operand.Type().IsRealType() {
			break
		}

		if t := n.AdditiveExpression.Operand.Type().Decay(); t.Kind() == cc.Ptr {
			mulR = t.Elem().Size()
			break
		}

		if t := n.MultiplicativeExpression.Operand.Type().Decay(); t.Kind() == cc.Ptr {
			mulL = t.Elem().Size()
			break
		}

		later(n, n.Operand.Type(), n.AdditiveExpression.Operand.Type(), n.MultiplicativeExpression.Operand.Type(), "__complex__")
	case cc.AdditiveExpressionSub: // AdditiveExpression '-' MultiplicativeExpression
		op = "sub"
		if n.AdditiveExpression.Operand.Type().IsRealType() && n.MultiplicativeExpression.Operand.Type().IsRealType() {
			//dbg("%v: %v", n.Token.Position(), n.Operand.Type())
			break
		}

		if t := n.AdditiveExpression.Operand.Type().Decay(); t.Kind() == cc.Ptr {
			if n.MultiplicativeExpression.Operand.Type().IsIntegerType() {
				mulR = t.Elem().Size()
				break
			}

			if n.MultiplicativeExpression.Operand.Type().Decay().Kind() == cc.Ptr {
				lhs := q.additiveExpression(n.AdditiveExpression, void)
				rhs := q.multiplicativeExpression(n.MultiplicativeExpression, void)
				r := q.t("sub %%t%[2]d, %%t%d\t\t# %v:", q.ptr, lhs, rhs, q.pos(&n.Token))
				return q.t("udiv %%t%[2]d, %d\t\t# %v:", q.ptr, r, n.AdditiveExpression.Operand.Type().Elem().Size(), q.pos(n.MultiplicativeExpression))
			}

			todo(n, n.MultiplicativeExpression.Operand.Type())
		}

		todo(n)
	default:
		panic("internal error") //TODOOK
	}

	if mulL == 0 || mulR == 0 {
		todo(n)
		panic("internal error") //TODOOK
	}

	lhs := q.additiveExpression(n.AdditiveExpression, void)
	lhs = q.convert(n, lhs, n.AdditiveExpression.Operand.Type(), n.Operand.Type(), nil)
	if mulL != 1 {
		lhs = q.t("mul %%t%[2]d, %d\t\t# %v:", q.ptr, lhs, mulL, q.pos(n.MultiplicativeExpression))
	}
	rhs := q.multiplicativeExpression(n.MultiplicativeExpression, void)
	rhs = q.convert(n, rhs, n.MultiplicativeExpression.Operand.Type(), n.Operand.Type(), nil)
	if mulR != 1 {
		rhs = q.t("mul %%t%[2]d, %d\t\t# %v:", q.ptr, rhs, mulR, q.pos(n.MultiplicativeExpression))
	}
	r := q.t("%[2]s %%t%d, %%t%d\t\t# %v:", q.typ(n, n.Operand.Type()), op, lhs, rhs, q.pos(&n.Token))
	return q.reduce(r, n.Operand.Type(), n.AdditiveExpression.Operand.Type(), n.MultiplicativeExpression.Operand.Type())
}

func (q *qbe) constVal(n cc.Node, o cc.Operand) int { //TODO-
	t := o.Type()
	switch x := o.Value().(type) {
	case cc.Int64Value:
		return q.t("copy %[2]v\t\t# %v: %v", q.typ(n, t), x, q.pos(n), t)
	case cc.Uint64Value:
		return q.t("copy %[2]v\t\t# %v: %v", q.typ(n, t), x, q.pos(n), t)
	case cc.StringValue:
		id := cc.StringID(x)
		switch t.Decay().Kind() {
		case cc.Ptr:
			return q.t("add $.%[2]d, %d\t\t# %v: %s", q.ptr, q.string(id), o.Offset(), q.pos(n), q.strComment(id))
		default:
			todo(n, n.Position(), t, t.Kind())
		}
	case cc.WideStringValue:
		id := cc.StringID(-x)
		switch t.Decay().Kind() {
		case cc.Ptr:
			return q.t("add $.%[2]d, %d\t\t# %v: %s", q.ptr, q.string(id), o.Offset(), q.pos(n), q.strComment(id))
		default:
			todo(n, n.Position(), t, t.Kind())
		}
	case cc.Float64Value:
		return q.t("copy %[2]v\t\t# %v: %v", q.typ(n, t), math.Float64bits(float64(x)), q.pos(n), x)
	case cc.Float32Value:
		return q.t("copy %[2]v\t\t# %v: %v", q.typ(n, t), math.Float32bits(float32(x)), q.pos(n), x)
	case *cc.InitializerValue, nil:
		return 0
	case *cc.Float128Value:
		switch {
		case x.NaN:
			todo(n)
		default:
			return q.t("copy ld_%[2]v\t\t# %v:", q.typ(n, t), x.N.Text('e', -1), q.pos(n))
		}
	default:
		todo(n, n.Position(), fmt.Sprintf("%T", x))
	}
	panic(internalErrorf("%v", n.Position()))
}

func (q *qbe) reduce(r int, t, lt, rt cc.Type) int {
	if !t.IsIntegerType() || t.IsSignedType() {
		return r
	}

	bits := 0
	if lt.IsBitFieldType() {
		bits = lt.BitField().BitFieldWidth()
	}
	switch {
	case rt == nil: // shift
		if bits != 0 && bits < 32 {
			bits = 32
		}
	default:
		if rt.IsBitFieldType() {
			if bits2 := rt.BitField().BitFieldWidth(); bits2 > bits {
				bits = bits2
			}
		}
	}
	if bits == 0 {
		return r
	}

	return q.t("and %%t%[2]d, %d\t\t# %#[3]x", q.typ(nil, t), r, uint64(1)<<uint(bits)-1)
}

func (q *qbe) multiplicativeExpression(n *cc.MultiplicativeExpression, void bool) int {
	if void {
		if n.IsSideEffectsFree {
			return -1
		}

		if n.Case != cc.MultiplicativeExpressionCast {
			q.multiplicativeExpression(n.MultiplicativeExpression, void)
			q.castExpression(n.CastExpression, void)
			return -1
		}
	}

	var op string
	switch n.Case {
	case cc.MultiplicativeExpressionCast: // CastExpression
		return q.castExpression(n.CastExpression, void)
	case cc.MultiplicativeExpressionMul: // MultiplicativeExpression '*' CastExpression
		op = "mul"
	case cc.MultiplicativeExpressionDiv: // MultiplicativeExpression '/' CastExpression
		op = "div"
	case cc.MultiplicativeExpressionMod: // MultiplicativeExpression '%' CastExpression
		op = "rem"
	default:
		panic("internal error") //TODOOK
	}
	if op != "mul" && n.Operand.Type().IsIntegerType() && !n.Operand.Type().IsSignedType() {
		op = "u" + op
	}
	lhs := q.multiplicativeExpression(n.MultiplicativeExpression, false)
	lhs = q.convert(n, lhs, n.MultiplicativeExpression.Operand.Type(), n.Operand.Type(), nil)
	rhs := q.castExpression(n.CastExpression, false)
	rhs = q.convert(n, rhs, n.CastExpression.Operand.Type(), n.Operand.Type(), nil)
	r := q.t("%[2]s %%t%d, %%t%d\t\t# %v:", q.typ(n, n.Operand.Type()), op, lhs, rhs, q.pos(&n.Token))
	return q.reduce(r, n.Operand.Type(), n.MultiplicativeExpression.Operand.Type(), n.CastExpression.Operand.Type())
}

func (q *qbe) castExpression(n *cc.CastExpression, void bool) int {
	var ok bool
	var args *cc.ArgumentExpressionList
	if n.Case == cc.CastExpressionCast {
		ok, args = q.isVaArg(n.CastExpression)
	}
	if !ok && void {
		if n.IsSideEffectsFree {
			return -1
		}

		if n.UnaryExpression != nil {
			q.unaryExpression(n.UnaryExpression, void)
		}
		if n.CastExpression != nil {
			q.castExpression(n.CastExpression, void)
		}
		return -1
	}

	switch n.Case {
	case cc.CastExpressionUnary: // UnaryExpression
		return q.unaryExpression(n.UnaryExpression, void)
	case cc.CastExpressionCast: // '(' TypeName ')' CastExpression
		if ok {
			t := n.TypeName.Type()
			switch kind(t) {
			case cc.Struct, cc.Union:
				report(n, "struct var arg") //TODO
			}

			//TODO- e := q.assignmentExpression(args.AssignmentExpression, false)
			//TODO- return q.t("vaarg %%t%[2]d\t\t# %v: %v", q.typ(n, n.TypeName.Type()), e, q.pos(n), n.TypeName.Type())
			d := args.AssignmentExpression.Operand.Declarator()
			if d == nil || args.AssignmentExpression.Operand.Offset() != 0 {
				todo(n)
			}
			return q.t("vaarg %[2]s\t\t# %v:", q.typ(n, n.TypeName.Type()), q.declName(n, d), q.pos(n))
		}

		e := q.castExpression(n.CastExpression, void)
		return q.convert(n, e, n.CastExpression.Operand.Type().Decay(), n.Operand.Type(), n.CastExpression.Operand.Value())
	}
	todo(n, n.Case)
	panic("internal error") //TODOOK
}

func (q *qbe) isVaArg(n *cc.CastExpression) (bool, *cc.ArgumentExpressionList) {
	if n.Case != cc.CastExpressionUnary {
		return false, nil
	}

	n2 := n.UnaryExpression
	if n2.Case != cc.UnaryExpressionPostfix {
		return false, nil
	}

	n3 := n2.PostfixExpression
	if n3.Case != cc.PostfixExpressionCall {
		return false, nil
	}

	n4 := n3.PostfixExpression
	if n4.Case != cc.PostfixExpressionPrimary {
		return false, nil
	}

	n5 := n4.PrimaryExpression
	if n5.Case != cc.PrimaryExpressionIdent {
		return false, nil
	}

	if n5.Token.Value.String() != "__gocc_va_arg" {
		return false, nil
	}

	return true, n3.ArgumentExpressionList
}

func (q *qbe) unaryExpression(n *cc.UnaryExpression, void bool) int {
	if void && n.IsSideEffectsFree {
		return -1
	}

	switch n.Case {
	case cc.UnaryExpressionPostfix: // PostfixExpression
		return q.postfixExpression(n.PostfixExpression, void)
	case cc.UnaryExpressionInc: // "++" UnaryExpression
		return q.preIncDec(n, "add", void)
	case cc.UnaryExpressionDec: // "--" UnaryExpression
		return q.preIncDec(n, "sub", void)
	case cc.UnaryExpressionAddrof: // '&' CastExpression
		if void {
			q.castExpression(n.CastExpression, void)
			return -1
		}

		//TODO check non nil op.Declarator()
		return q.castExpressionAddrOf(n.CastExpression)
	case cc.UnaryExpressionDeref: // '*' CastExpression
		if void {
			q.castExpression(n.CastExpression, void)
			return -1
		}

		p := q.castExpression(n.CastExpression, void)
		return q.load(n, p, n.Operand, n.Operand.Type())
	case cc.UnaryExpressionPlus: // '+' CastExpression
		if void {
			q.castExpression(n.CastExpression, void)
			return -1
		}

		t := q.castExpression(n.CastExpression, void)
		return q.convert(n, t, n.CastExpression.Operand.Type(), n.Operand.Type(), nil)
	case cc.UnaryExpressionMinus: // '-' CastExpression
		if void {
			q.castExpression(n.CastExpression, void)
			return -1
		}

		t := q.castExpression(n.CastExpression, void)
		t = q.convert(n, t, n.CastExpression.Operand.Type(), n.Operand.Type(), nil)
		switch n.CastExpression.Operand.Type().Kind() {
		case cc.Float:
			return q.t("mul s_-1, %%t%[2]d\t\t# %v:", q.typ(n, n.Operand.Type()), t, q.pos(n))
		case cc.Double:
			return q.t("mul d_-1, %%t%[2]d\t\t# %v:", q.typ(n, n.Operand.Type()), t, q.pos(n))
		case cc.LongDouble:
			return q.t("mul ld_-1, %%t%[2]d\t\t# %v:", q.typ(n, n.Operand.Type()), t, q.pos(n))
		default:
			return q.t("sub 0, %%t%[2]d\t\t# %v:", q.typ(n, n.Operand.Type()), t, q.pos(n))
		}
	case cc.UnaryExpressionCpl: // '~' CastExpression
		if void {
			q.castExpression(n.CastExpression, void)
			return -1
		}

		if void {
			q.castExpression(n.CastExpression, void)
			return -1
		}

		t := q.castExpression(n.CastExpression, void)
		t = q.convert(n, t, n.CastExpression.Operand.Type(), n.Operand.Type(), nil)
		return q.t("xor %%t%[2]d, -1\t\t# %v:", q.typ(n, n.Operand.Type()), t, q.pos(n))
	case cc.UnaryExpressionNot: // '!' CastExpression
		if void {
			q.castExpression(n.CastExpression, void)
			return -1
		}

		//	%e = expr
		// @a
		//	jnz %e, @b, @z
		// @b
		// @z
		//	r = phi @a 1, @b 0
		a := q.label()
		b := q.label()
		z := q.label()
		if n.CastExpression.Operand.Type().IsComplexType() {
			todo(n)
		}

		e := q.castExpression(n.CastExpression, false)
		e = q.nonZero(n, e, n.CastExpression.Operand)
		q.w("@.%d\n", a)
		q.w("\tjnz %%t%d, @.%d, @.%d\n", e, b, z)
		q.w("@.%d\n", b)
		q.w("@.%d\n", z)
		if void {
			return -1
		}

		return q.t("phi @.%[2]d 1, @.%d 0", q.typ(n, n.Operand.Type()), a, b)
	case cc.UnaryExpressionSizeofExpr: // "sizeof" UnaryExpression
		if t := n.UnaryExpression.Operand.Type(); t.Kind() == cc.Array && t.IsVLA() {
			todo(n)
			todo(n)
		}

		return q.t("copy %[2]v\t\t# %v:", q.typ(n, n.Operand.Type()), n.Operand.Value(), q.pos(n))
	case cc.UnaryExpressionSizeofType: // "sizeof" '(' TypeName ')'
		if v := n.Operand.Value(); v != nil {
			return q.t("copy %[2]v\t\t# %v:", q.typ(n, n.Operand.Type()), v, q.pos(n))
		}

		todo(n)
	case cc.UnaryExpressionLabelAddr: // "&&" IDENTIFIER
		todo(n)
	case cc.UnaryExpressionAlignofExpr: // "_Alignof" UnaryExpression
		return q.t("copy %[2]d\t\t# %v:", q.typ(n, n.Operand.Type()), n.Operand.Value(), q.pos(n))
	case cc.UnaryExpressionAlignofType: // "_Alignof" '(' TypeName ')'
		return q.t("copy %[2]d\t\t# %v:", q.typ(n, n.Operand.Type()), n.Operand.Value(), q.pos(n))
	case cc.UnaryExpressionImag: // "__imag__" UnaryExpression
		todo(n)
	case cc.UnaryExpressionReal: // "__real__" UnaryExpression
		todo(n)
	}
	todo(n, n.Case)
	panic("internal error") //TODOOK
}

func (q *qbe) nonZero(n cc.Node, e int, op cc.Operand) int {
	t := op.Type()
	if t.IsIntegerType() {
		switch t.Size() {
		case 1, 2, 4:
			return e
		case 8:
			return q.t("cnel %%t%[2]d, 0", "w", e)
		default:
			todo(n, t, t.Size())
		}
	}

	switch t := t.Decay(); t.Kind() {
	case cc.Ptr:
		if t.Size() == 4 {
			return e
		}

		return q.t("cnel %%t%[2]d, 0", "w", e)
	case cc.Double:
		return q.t("cned %%t%[2]d, 0", "w", e)
	case cc.LongDouble:
		todo(n)
	case cc.Float:
		return q.t("cnes %%t%[2]d, 0", "w", e)
	default:
		todo(n, t, t.Alias())
	}
	panic("unreachable")
}

func (q *qbe) castExpressionAddrOf(n *cc.CastExpression) int {
	switch n.Case {
	case cc.CastExpressionUnary: // UnaryExpression
		return q.unaryExpressionAddrOf(n.UnaryExpression)
	}
	todo(n, n.Case)
	panic("internal error") //TODOOK
}

func (q *qbe) unaryExpressionAddrOf(n *cc.UnaryExpression) int {
	switch n.Case {
	case cc.UnaryExpressionPostfix: // PostfixExpression
		return q.postfixExpressionAddrOf(n.PostfixExpression)
	case cc.UnaryExpressionDeref: // '*' CastExpression
		return q.castExpression(n.CastExpression, false) //TODO void
	}
	todo(n, n.Case)
	panic("internal error") //TODOOK
}

func (q *qbe) postfixExpressionAddrOf(n *cc.PostfixExpression) int {
	switch n.Case {
	case cc.PostfixExpressionPrimary: // PrimaryExpression
		return q.primaryExpressionAddrOf(n.PrimaryExpression)
	case cc.PostfixExpressionIndex: // PostfixExpression '[' Expression ']'
		switch {
		case n.Expression.Operand.Type().IsIntegerType():
			e := q.expression(n.Expression, false)
			e = q.convert(n, e, n.Expression.Operand.Type(), q.ptrType, nil)
			e = q.t("mul %%t%[2]d, %d\t\t# %v:", q.ptr, e, n.PostfixExpression.Operand.Type().Elem().Size(), q.pos(&n.Token))
			p := q.postfixExpression(n.PostfixExpression, false)
			return q.t("add %%t%[2]d, %%t%d", q.ptr, p, e)
		default:
			todo(n)
		}
	case cc.PostfixExpressionSelect: // PostfixExpression '.' IDENTIFIER
		r := q.postfixExpressionPtr(n.PostfixExpression)
		return q.t("add %%t%[2]d, %d\t\t# %v: .%s", q.ptr, r, n.Field.Offset(), q.pos(&n.Token), n.Token2.Value)
	case cc.PostfixExpressionPSelect: // PostfixExpression "->" IDENTIFIER
		r := q.postfixExpression(n.PostfixExpression, false)
		return q.t("add %%t%[2]d, %d\t\t# %v: .%s", q.ptr, r, n.Field.Offset(), q.pos(&n.Token), n.Token2.Value)
	case cc.PostfixExpressionComplit: // '(' TypeName ')' '{' InitializerList ',' '}'
		t := n.TypeName.Type()
		if q.fn == nil {
			todo(n)
		}

		r := q.compositeLiterals[n]
		if n.InitializerList.IsConst() {
			a := q.anonymousInitializer(nil, t, n, n.InitializerList.List())
			q.w("\tcall $memcpy(%s %%t%d, %s $.%d, %s %d)\t\t# %v:\n", q.ptr, r, q.ptr, a, q.ptr, t.Size(), q.pos(n))
			return r
		}

		in := newComputedInitializer(q, n, q.compositeLiterals[n], t)
		q.initializer(in, n.InitializerList.List(), 0)
		return r
	}
	todo(n, n.Case)
	panic("internal error") //TODOOK
}

func (q *qbe) primaryExpressionAddrOf(n *cc.PrimaryExpression) int {
	switch n.Case {
	case cc.PrimaryExpressionIdent: // IDENTIFIER
		return q.declaratorPtr(n, n.Operand)
	case cc.PrimaryExpressionExpr: // '(' Expression ')'
		return q.expressionAddrOf(n.Expression)
	}
	todo(n, n.Case)
	panic("internal error") //TODOOK
}

func (q *qbe) expressionAddrOf(n *cc.Expression) int {
	switch n.Case {
	case cc.ExpressionAssign: // AssignmentExpression
		return q.assignmentExpressionAddrOf(n.AssignmentExpression)
	}
	todo(n, n.Case)
	panic("internal error") //TODOOK
}

func (q *qbe) assignmentExpressionAddrOf(n *cc.AssignmentExpression) int {
	switch n.Case {
	case cc.AssignmentExpressionCond: // ConditionalExpression
		return q.conditionalExpressionAddrOf(n.ConditionalExpression)
	}
	todo(n, n.Case)
	panic("internal error") //TODOOK
}

func (q *qbe) conditionalExpressionAddrOf(n *cc.ConditionalExpression) int {
	switch n.Case {
	case cc.ConditionalExpressionLOr: // LogicalOrExpression
		return q.logicalOrExpressionAddrOf(n.LogicalOrExpression)
	}
	todo(n, n.Case)
	panic("internal error") //TODOOK
}

func (q *qbe) logicalOrExpressionAddrOf(n *cc.LogicalOrExpression) int {
	switch n.Case {
	case cc.LogicalOrExpressionLAnd: // LogicalAndExpression
		return q.logicalAndExpressionAddrOf(n.LogicalAndExpression)
	}
	todo(n, n.Case)
	panic("internal error") //TODOOK
}

func (q *qbe) logicalAndExpressionAddrOf(n *cc.LogicalAndExpression) int {
	switch n.Case {
	case cc.LogicalAndExpressionOr: // InclusiveOrExpression
		return q.inclusiveOrExpressionAddrOf(n.InclusiveOrExpression)
	}
	todo(n, n.Case)
	panic("internal error") //TODOOK
}

func (q *qbe) inclusiveOrExpressionAddrOf(n *cc.InclusiveOrExpression) int {
	switch n.Case {
	case cc.InclusiveOrExpressionXor: // ExclusiveOrExpression
		return q.exclusiveOrExpressionAddrOf(n.ExclusiveOrExpression)
	}
	todo(n, n.Case)
	panic("internal error") //TODOOK
}

func (q *qbe) exclusiveOrExpressionAddrOf(n *cc.ExclusiveOrExpression) int {
	switch n.Case {
	case cc.ExclusiveOrExpressionAnd: // AndExpression
		return q.andExpressionAddrOf(n.AndExpression)
	}
	todo(n, n.Case)
	panic("internal error") //TODOOK
}

func (q *qbe) andExpressionAddrOf(n *cc.AndExpression) int {
	switch n.Case {
	case cc.AndExpressionEq: // EqualityExpression
		return q.equalityExpressionAddrOf(n.EqualityExpression)
	}
	todo(n, n.Case)
	panic("internal error") //TODOOK
}

func (q *qbe) equalityExpressionAddrOf(n *cc.EqualityExpression) int {
	switch n.Case {
	case cc.EqualityExpressionRel: // RelationalExpression
		return q.relationalExpressionAddrOf(n.RelationalExpression)
	}
	todo(n, n.Case)
	panic("internal error") //TODOOK
}

func (q *qbe) relationalExpressionAddrOf(n *cc.RelationalExpression) int {
	switch n.Case {
	case cc.RelationalExpressionShift: // ShiftExpression
		return q.shiftExpressionAddrOf(n.ShiftExpression)
	}
	todo(n, n.Case)
	panic("internal error") //TODOOK
}

func (q *qbe) shiftExpressionAddrOf(n *cc.ShiftExpression) int {
	switch n.Case {
	case cc.ShiftExpressionAdd: // AdditiveExpression
		return q.additiveExpressionAddrOf(n.AdditiveExpression)
	}
	todo(n, n.Case)
	panic("internal error") //TODOOK
}

func (q *qbe) additiveExpressionAddrOf(n *cc.AdditiveExpression) int {
	switch n.Case {
	case cc.AdditiveExpressionMul: // MultiplicativeExpression
		return q.multiplicativeExpressionAddrOf(n.MultiplicativeExpression)
	}
	todo(n, n.Case)
	panic("internal error") //TODOOK
}

func (q *qbe) multiplicativeExpressionAddrOf(n *cc.MultiplicativeExpression) int {
	switch n.Case {
	case cc.MultiplicativeExpressionCast: // CastExpression
		return q.castExpressionAddrOf(n.CastExpression)
	}
	todo(n, n.Case)
	panic("internal error") //TODOOK
}

func (q *qbe) preIncDec(n *cc.UnaryExpression, operator string, void bool) int {
	operand := n.UnaryExpression.Operand
	t := operand.Type()
	delta := uintptr(1)
	switch kind(t) {
	case cc.Array:
		delta = t.Elem().Size()
	case cc.Struct, cc.Union:
		todo(n)
	case cc.Ptr:
		delta = t.Elem().Size()
	}

	switch d := n.UnaryExpression.Declarator(); {
	case q.values[d]:
		e := q.declarator(n, operand)
		e = q.t("%[2]s %%t%d, %s", q.typ(n, t), operator, e, q.int(n, uint64(delta), t))
		return q.stored(n, d, e, operand, void)
	default:
		p := q.unaryExpressionPtr(n.UnaryExpression)
		e := q.load(n, p, operand, t)
		e = q.t("%[2]s %%t%d, %s", q.typ(n, t), operator, e, q.int(n, uint64(delta), t))
		return q.store(&n.Token, p, e, operand, t, void)
	}
}

func (q *qbe) postfixExpression(n *cc.PostfixExpression, void bool) int {
	switch n.Case {
	case cc.PostfixExpressionPrimary: // PrimaryExpression
		return q.primaryExpression(n.PrimaryExpression, void)
	case cc.PostfixExpressionIndex: // PostfixExpression '[' Expression ']'
		//TODO check non-nil value
		switch {
		case n.Expression.Operand.Type().IsIntegerType():
			e := q.expression(n.Expression, false)
			e = q.convert(n, e, n.Expression.Operand.Type(), q.ptrType, nil)
			e = q.t("mul %%t%[2]d, %d\t\t# %v:", q.ptr, e, n.Operand.Type().Size(), q.pos(&n.Token))
			p := q.postfixExpression(n.PostfixExpression, false)
			p = q.t("add %%t%[2]d, %%t%d", q.ptr, p, e)
			if n.Operand.Type().Kind() == cc.Array {
				return p
			}

			return q.load(&n.Token, p, n.Operand, n.Operand.Type())
		default:
			todo(n)
		}
	case cc.PostfixExpressionCall: // PostfixExpression '(' ArgumentExpressionList ')'
		if r := q.constVal(n, n.Operand); r != 0 {
			return r
		}

		return q.call(n)
	case cc.PostfixExpressionSelect: // PostfixExpression '.' IDENTIFIER
		p := q.postfixExpressionPtr(n.PostfixExpression)
		p = q.t("add %%t%[2]d, %d\t\t# %v: .%s", q.ptr, p, n.Field.Offset(), q.pos(&n.Token), n.Token2.Value)
		return q.load(&n.Token, p, n.Operand, n.Operand.Type())
	case cc.PostfixExpressionPSelect: // PostfixExpression "->" IDENTIFIER
		p := q.postfixExpression(n.PostfixExpression, void)
		if void {
			return -1
		}

		p = q.t("add %%t%[2]d, %d\t\t# %v:", q.ptr, p, n.Field.Offset(), q.pos(&n.Token))
		return q.load(&n.Token, p, n.Operand, n.Operand.Type())
	case cc.PostfixExpressionInc: // PostfixExpression "++"
		return q.postIncDec(n, "add", void)
	case cc.PostfixExpressionDec: // PostfixExpression "--"
		return q.postIncDec(n, "sub", void)
	case cc.PostfixExpressionComplit: // '(' TypeName ')' '{' InitializerList ',' '}'
		t := n.TypeName.Type()
		if q.fn == nil {
			todo(n)
		}

		switch kind(t) {
		case cc.Array, cc.Struct, cc.Union:
			return q.postfixExpressionPtr(n)
		default:
			todo(n)
		}
	case cc.PostfixExpressionTypeCmp: // "__builtin_types_compatible_p" '(' TypeName ',' TypeName ')'
		todo(n)
	}
	todo(n, n.Case)
	panic("internal error") //TODOOK
}

func (q *qbe) postIncDec(n *cc.PostfixExpression, operator string, void bool) (r int) {
	operand := n.PostfixExpression.Operand
	t := operand.Type()
	delta := uintptr(1)
	switch kind(t) {
	case cc.Array:
		delta = t.Elem().Size()
	case cc.Struct, cc.Union:
		todo(n)
	case cc.Ptr:
		delta = t.Elem().Size()
	}

	switch d := n.PostfixExpression.Declarator(); {
	case q.values[d]:
		r = q.declarator(n, operand)
		e := q.t("%[2]s %%t%d, %s\t\t# %v:", q.typ(n, t), operator, r, q.int(n, uint64(delta), t), q.pos(&n.Token))
		q.stored(n, d, e, operand, true)
	default:
		p := q.postfixExpressionPtr(n.PostfixExpression)
		r = q.load(n, p, operand, t)
		e := q.t("%[2]s %%t%d, %s\t\t# %v:", q.typ(n, t), operator, r, q.int(n, uint64(delta), t), q.pos(&n.Token))
		q.store(&n.Token, p, e, operand, t, true)
	}
	if void || !t.IsBitFieldType() {
		return r
	}

	f := t.BitField()
	ft := f.Type()
	r = q.t("shl %%t%[2]d, %d", q.typ(n, ft), r, ft.Size()*8-uintptr(f.BitFieldWidth()))
	operator = "shr"
	if ft.IsSignedType() {
		operator = "sar"
	}
	r = q.t("%[2]s %%t%d, %d", q.typ(n, ft), operator, r, ft.Size()*8-uintptr(f.BitFieldWidth()))
	return q.convert(n, r, ft, t, nil)
}

func (q *qbe) int(n cc.Node, u uint64, t cc.Type) string {
	switch t.Kind() {
	case cc.Float:
		return fmt.Sprintf("s_%d", u)
	case cc.Double:
		return fmt.Sprintf("d_%d", u)
	case cc.LongDouble:
		return fmt.Sprintf("ld_%d", u)
	default:
		return fmt.Sprint(u)
	}
}

func (q *qbe) call(n *cc.PostfixExpression) int {
	if q.isAlloca(n) {
		e := q.assignmentExpression(n.ArgumentExpressionList.AssignmentExpression, false)
		e = q.convert(n, e, n.ArgumentExpressionList.AssignmentExpression.Operand.Type(), q.task.ast.SizeType, nil)
		return q.t("alloc16 %%t%[2]d\t\t# %v", q.ptr, e, q.pos(n))
	}

	if q.isVaStart(n) {
		d := n.ArgumentExpressionList.AssignmentExpression.Operand.Declarator()
		if d == nil || n.ArgumentExpressionList.AssignmentExpression.Operand.Offset() != 0 {
			todo(n)
		}
		q.w("\tvastart %s\t\t# %v:\n", q.declName(n, d), q.pos(n))
		return -1
	}

	if q.isVaEnd(n) {
		q.assignmentExpression(n.ArgumentExpressionList.AssignmentExpression, false)
		return -1
	}

	r := -1
	ft := n.PostfixExpression.Operand.Type()
	if ft.Kind() == cc.Ptr {
		ft = ft.Elem()
	}
	var args []int
	i := 0
	var ats []cc.Type
	for n := n.ArgumentExpressionList; n != nil; n = n.ArgumentExpressionList {
		var arg int
		e := n.AssignmentExpression
		et := n.AssignmentExpression.Operand.Type()
		at := n.AssignmentExpression.Promote()
		ats = append(ats, at)
		switch {
		case at.IsComplexType():
			switch d := n.AssignmentExpression.Operand.Declarator(); {
			case d != nil:
				arg = q.declarator(e, n.AssignmentExpression.Operand)
			default:
				arg = q.callSiteComplexExpr[e]
				re, im := q.complexAssignmentExpression(e, false)
				re, im = q.convertToComplex(e, re, im, et, at)
				p := q.t("copy %%t%[2]d", q.ptr, arg)
				t := q.t("add %%t%[2]d, %d", q.ptr, p, at.Real().Offset())
				q.store(e, t, re, nil, at.Real().Type(), true)
				t = q.t("add %%t%[2]d, %d", q.ptr, p, at.Imag().Offset())
				q.store(e, t, im, nil, at.Imag().Type(), true)
			}
		default:
			arg = q.assignmentExpression(e, false)
			arg = q.convert(n, arg, et, at, nil)
		}
		args = append(args, arg)
		i++
	}
	pref := "\t"
	if ft.Result().Kind() != cc.Void {
		r = q.temp()
		pref += fmt.Sprintf("%%t%d =%s ", r, q.abiType(n, ft.Result()))
	}
	op := n.PostfixExpression.Operand
	var d *cc.Declarator
	switch d = op.Declarator(); {
	case d != nil && op.Offset() == 0:
		//TODO-? if q.escaped[d] {
		//TODO-? 	todo(n)
		//TODO-? }

		if q.values[d] || d.Type().Kind() == cc.Ptr {
			fp := q.declarator(n, op)
			if ft.IsVariadic() {
				q.w("\t#qbe:fixArgs %d\n", len(ft.Parameters()))
			}
			q.w("%scall %%t%d(", pref, fp)
			break
		}

		switch d.Linkage {
		case cc.External, cc.Internal:
			switch nm := q.renames[d]; {
			case nm != 0:
				if ft.IsVariadic() {
					q.w("\t#qbe:fixArgs %d\n", len(ft.Parameters()))
				}
				q.w("%scall $.%d(", pref, nm)
			default:
				if ft.IsVariadic() {
					q.w("\t#qbe:fixArgs %d\n", len(ft.Parameters()))
				}
				q.w("%scall $%q(", pref, d.Name())
			}
		case cc.None:
			fp := q.declarator(n, op)
			if ft.IsVariadic() {
				q.w("\t#qbe:fixArgs %d\n", len(ft.Parameters()))
			}
			q.w("%scall %%t%d(", pref, fp)
		default:
			todo(n, d.Linkage)
		}
	default:
		switch t := n.PostfixExpression.Operand.Type(); t.Kind() {
		case cc.Function:
			fn := q.postfixExpression(n.PostfixExpression, false)
			if ft.IsVariadic() {
				q.w("\t#qbe:fixArgs %d\n", len(ft.Parameters()))
			}
			q.w("%scall %%t%d(", pref, fn)
		case cc.Ptr:
			switch t := t.Elem(); t.Kind() {
			case cc.Function:
				fn := q.postfixExpression(n.PostfixExpression, false)
				if ft.IsVariadic() {
					q.w("\t#qbe:fixArgs %d\n", len(ft.Parameters()))
				}
				q.w("%scall %%t%d(", pref, fn)
			default:
				todo(n, n.PostfixExpression.Operand.Type())
			}
		default:
			todo(n, n.PostfixExpression.Operand.Type())
		}
	}

	for i, v := range args {
		q.w("%s %%t%d", q.abiType(nil, ats[i]), v)
		if i < len(args)-1 {
			q.w(", ")
		}
	}
	if ft.IsVariadic() {
		q.w(", ...")
	}
	q.w(")\t\t# %v: %v", q.pos(n), op.Type())
	if d != nil {
		q.w(", %v:", q.pos(d))
	}
	q.w("\n")
	return r
}

func (q *qbe) complexAssignmentExpression(n *cc.AssignmentExpression, void bool) (int, int) {
	switch n.Case {
	case cc.AssignmentExpressionCond: // ConditionalExpression
		return q.complexConditionalExpression(n.ConditionalExpression)
	case cc.AssignmentExpressionAssign: // UnaryExpression '=' AssignmentExpression
		if !n.Operand.Type().IsComplexType() {
			todo(n)
		}
		// 	if n.UnaryExpression.Operand.Type().Kind() == cc.Array && n.AssignmentExpression.Operand.Type().Kind() == cc.Array {
		// 		// TCC extension
		// 		lsz := n.UnaryExpression.Operand.Type().Size()
		// 		rsz := n.AssignmentExpression.Operand.Type().Size()
		// 		if lsz != rsz {
		// 			todo(n)
		// 		}

		// 		rhs := q.declarator(n, n.AssignmentExpression.Operand.Declarator())
		// 		lhs := q.declarator(n, n.UnaryExpression.Operand.Declarator())
		// 		q.w("\tcall $memcpy(%s %%t%d, %s %%t%d, %s %d)\t\t# %v: 930\n", q.ptr, lhs, q.ptr, rhs, q.ptr, lsz, q.pos(&n.Token))
		// 		return -1 //TODO a=b=c;
		// 	}

		// 	switch kind(n.AssignmentExpression.Operand.Type()) {
		// 	case cc.Struct, cc.Union:
		// 		rhs := q.assignmentExpression(n.AssignmentExpression)
		// 		lhs := q.unaryExpressionPtr(n.UnaryExpression)
		// 		q.w("\tcall $memcpy(%s %%t%d, %s %%t%d, %s %d)\t\t# %v:\n", q.ptr, lhs, q.ptr, rhs, q.ptr, n.UnaryExpression.Operand.Type().Size(), q.pos(&n.Token))
		// 		return rhs
		// 	}

		// 	rhs := q.assignmentExpression(n.AssignmentExpression)
		// 	rhs = q.convert(n, rhs, n.AssignmentExpression.Operand.Type(), n.UnaryExpression.Operand.Type())
		// 	lhs := q.unaryExpressionPtr(n.UnaryExpression)
		// 	return q.store(&n.Token, lhs, rhs, n.UnaryExpression.Operand.Declarator(), n.UnaryExpression.Operand.Type())
		re, im := q.complexAssignmentExpression(n.AssignmentExpression, void)
		re, im = q.convertToComplex(n, re, im, n.AssignmentExpression.Operand.Type(), n.UnaryExpression.Operand.Type())
		lhs := q.unaryExpressionPtr(n.UnaryExpression)
		p := q.t("add %%t%[2]d, %d", q.ptr, lhs, n.UnaryExpression.Operand.Type().Real().Offset())
		q.store(&n.Token, p, re, nil, n.UnaryExpression.Operand.Type().Real().Type(), true)
		p = q.t("add %%t%[2]d, %d", q.ptr, lhs, n.UnaryExpression.Operand.Type().Imag().Offset())
		q.store(&n.Token, p, im, nil, n.UnaryExpression.Operand.Type().Imag().Type(), true)
		return re, im
		// case cc.AssignmentExpressionMul: // UnaryExpression "*=" AssignmentExpression
		// 	return q.assop(n, "mul")
		// case cc.AssignmentExpressionDiv: // UnaryExpression "/=" AssignmentExpression
		// 	return q.assop(n, "div")
		// case cc.AssignmentExpressionMod: // UnaryExpression "%=" AssignmentExpression
		// 	return q.assop(n, "rem")
	case cc.AssignmentExpressionAdd: // UnaryExpression "+=" AssignmentExpression
		return q.complexAssop(n, "add", void)
		// case cc.AssignmentExpressionSub: // UnaryExpression "-=" AssignmentExpression
		// 	return q.assop(n, "sub")
		// case cc.AssignmentExpressionLsh: // UnaryExpression "<<=" AssignmentExpression
		// 	return q.shiftop(n, "shl")
		// case cc.AssignmentExpressionRsh: // UnaryExpression ">>=" AssignmentExpression
		// 	return q.shiftop(n, "shr")
		// case cc.AssignmentExpressionAnd: // UnaryExpression "&=" AssignmentExpression
		// 	return q.assop(n, "and")
		// case cc.AssignmentExpressionXor: // UnaryExpression "^=" AssignmentExpression
		// 	return q.assop(n, "xor")
		// case cc.AssignmentExpressionOr: // UnaryExpression "|=" AssignmentExpression
		// 	return q.assop(n, "or")
	}
	todo(n, n.Case)
	panic(internalErrorf("%v", n.Case))
}

func (q *qbe) complexAssop(n *cc.AssignmentExpression, op string, void bool) (int, int) {
	//TODO- var mulL, mulR uintptr = 1, 1
	//TODO- if t := n.UnaryExpression.Operand.Type().Decay(); t.Kind() == cc.Ptr {
	//TODO- 	mulR = t.Elem().Size()
	//TODO- } else if t := n.AssignmentExpression.Operand.Type().Decay(); t.Kind() == cc.Ptr {
	//TODO- 	todo(n, t)
	//TODO- }

	//TODO- if mulL == 0 || mulR == 0 {
	//TODO- 	panic("internal error") //TODOOK
	//TODO- }

	//TODO- if (op == "div" || op == "rem") && n.Promote().IsIntegerType() && !n.Promote().IsSignedType() {
	//TODO- 	op = "u" + op
	//TODO- }
	//TODO- rhs := q.assignmentExpression(n.AssignmentExpression, false)
	//TODO- rhs = q.convert(n, rhs, n.AssignmentExpression.Operand.Type(), n.Promote())
	//TODO- if mulR != 1 {
	//TODO- 	rhs = q.t("mul %%t%[2]d, %d", q.ptr, rhs, mulR)
	//TODO- }
	//TODO- lhs := q.unaryExpressionPtr(n.UnaryExpression)
	//TODO- t := q.load(n, lhs, n.UnaryExpression.Operand.Declarator(), n.UnaryExpression.Operand.Type())
	//TODO- t = q.convert(n, t, n.UnaryExpression.Operand.Type(), n.Promote())
	//TODO- if mulL != 1 {
	//TODO- 	todo(n, mulL)
	//TODO- }
	//TODO- rhs = q.t("%[2]s %%t%d, %%t%d\t\t# %v:", q.typ(n, n.Promote()), op, t, rhs, q.pos(&n.Token))
	//TODO- rhs = q.convert(n, rhs, n.Promote(), n.UnaryExpression.Operand.Type())
	//TODO- return q.store(&n.Token, lhs, rhs, n.UnaryExpression.Operand.Declarator(), n.UnaryExpression.Operand.Type())
	re, im := q.complexAssignmentExpression(n.AssignmentExpression, void)
	re, im = q.convertToComplex(n, re, im, n.AssignmentExpression.Operand.Type(), n.Promote())
	p := q.unaryExpressionPtr(n.UnaryExpression)
	t := n.UnaryExpression.Operand.Type()
	switch op {
	case "add":
		pre := q.t("add %%t%[2]d, %d", q.ptr, p, t.Real().Offset())
		re2 := q.load(n, pre, nil, t.Real().Type())
		re2 = q.convert(n, re2, t.Real().Type(), n.Promote().Real().Type(), nil)
		re2 = q.t("%[2]s %%t%d, %%t%d\t\t# %v:", q.typ(n, n.Promote().Real().Type()), op, re2, re, q.pos(&n.Token))
		re2 = q.convert(n, re2, n.Promote().Real().Type(), t.Real().Type(), nil)
		re2 = q.store(n, pre, re2, nil, t.Real().Type(), void)

		pim := q.t("add %%t%[2]d, %d", q.ptr, p, t.Imag().Offset())
		im2 := q.load(n, pim, nil, t.Imag().Type())
		im2 = q.convert(n, im2, t.Imag().Type(), n.Promote().Imag().Type(), nil)
		im2 = q.t("%[2]s %%t%d, %%t%d\t\t# %v:", q.typ(n, n.Promote().Imag().Type()), op, im2, im, q.pos(&n.Token))
		im2 = q.convert(n, im2, n.Promote().Imag().Type(), t.Imag().Type(), nil)
		im2 = q.store(n, pim, im2, nil, t.Imag().Type(), void)
		return re2, im2
	default:
		todo(n, p, re, im, op)
	}
	panic("TODO")
}

func (q *qbe) complexConditionalExpression(n *cc.ConditionalExpression) (int, int) {
	switch n.Case {
	case cc.ConditionalExpressionLOr: // LogicalOrExpression
		return q.complexLogicalOrExpression(n.LogicalOrExpression)
		// case cc.ConditionalExpressionCond: // LogicalOrExpression '?' Expression ':' ConditionalExpression
		// 	// if expr != 0 goto a else goto b
		// 	// @a
		// 	// r = expression2
		// 	// goto z
		// 	// @b
		// 	// r = expression3
		// 	// @z
		// 	a := q.label()
		// 	b := q.label()
		// 	z := q.label()
		// 	e := q.logicalOrExpression(n.LogicalOrExpression)
		// 	q.w("\tjnz %%t%d, @.%d, @.%d\t\t# %v:\n", e, a, b, q.pos(n))
		// 	q.w("@.%d\n", a)
		// 	var r int
		// 	var t cc.Type
		// 	switch {
		// 	case n.Expression == nil:
		// 		r = q.logicalOrExpression(n.LogicalOrExpression)
		// 		t = n.LogicalOrExpression.Operand.Type()
		// 	default:
		// 		r = q.expression(n.Expression)
		// 		t = n.Expression.Operand.Type()
		// 	}
		// 	if r > 0 {
		// 		r = q.convert(n, r, t, n.Operand.Type())
		// 	}
		// 	q.w("\tjmp @.%d\n", z)
		// 	q.w("@.%d\n", b)
		// 	r2 := q.conditionalExpression(n.ConditionalExpression)
		// 	if r2 > 0 {
		// 		r2 = q.convert(n, r2, n.ConditionalExpression.Operand.Type(), n.Operand.Type())
		// 	tn("copy %%t%[2]d", r, q.typ(n, n.Operand.Type()), r2)
		// 	}
		// 	q.w("@.%d\n", z)
		// 	return r
	}
	todo(n, n.Case)
	panic(internalErrorf("%v", n.Case))
}

func (q *qbe) complexLogicalOrExpression(n *cc.LogicalOrExpression) (int, int) {
	switch n.Case {
	case cc.LogicalOrExpressionLAnd: // LogicalAndExpression
		return q.complexLogicalAndExpression(n.LogicalAndExpression)
		// case cc.LogicalOrExpressionLOr: // LogicalOrExpression "||" LogicalAndExpression
		// 	// r = 0
		// 	// if expr1 != 0 goto y else goto a
		// 	// @a
		// 	// if expr2 != 0 goto y else goto z
		// 	// @y
		// 	// r++
		// 	// @z
		// 	a := q.label()
		// 	y := q.label()
		// 	z := q.label()
		// 	r := q.t("copy 0\t\t# %[2]v:", q.typ(n, n.Operand.Type()), q.pos(n))
		// 	e1 := q.logicalOrExpression(n.LogicalOrExpression)
		// 	q.w("\tjnz %%t%d, @.%d, @.%d\n", e1, y, a)
		// 	q.w("@.%d\n", a)
		// 	e2 := q.logicalAndExpression(n.LogicalAndExpression)
		// 	q.w("\tjnz %%t%d, @.%d, @.%d\n", e2, y, z)
		// 	q.w("@.%d\n", y)
		// 	q.tn("copy %[2]d", r, q.typ(n, n.Operand.Type()), 1)
		// 	q.w("@.%d\n", z)
		// 	return r
	}
	todo(n, n.Case)
	panic(internalErrorf("%v", n.Case))
}

func (q *qbe) complexLogicalAndExpression(n *cc.LogicalAndExpression) (int, int) {
	switch n.Case {
	case cc.LogicalAndExpressionOr: // InclusiveOrExpression
		return q.complexInclusiveOrExpression(n.InclusiveOrExpression)
		// case cc.LogicalAndExpressionLAnd: // LogicalAndExpression "&&" InclusiveOrExpression
		// 	// r = 1
		// 	// if expr1 != 0 goto a else goto y
		// 	// @a
		// 	// if expr2 != 0 goto z else goto y
		// 	// @y
		// 	// r--
		// 	// @z
		// 	a := q.label()
		// 	y := q.label()
		// 	z := q.label()
		// 	r := q.t("copy 1\t\t# %[2]v:", q.typ(n, n.Operand.Type()), q.pos(n))
		// 	e1 := q.logicalAndExpression(n.LogicalAndExpression)
		// 	q.w("\tjnz %%t%d, @.%d, @.%d\n", e1, a, y)
		// 	q.w("@.%d\n", a)
		// 	e2 := q.inclusiveOrExpression(n.InclusiveOrExpression)
		// 	q.w("\tjnz %%t%d, @.%d, @.%d\n", e2, z, y)
		// 	q.w("@.%d\n", y)
		// 	q.tn("copy %[2]d", r, q.typ(n, n.Operand.Type()), 0)
		// 	q.w("@.%d\n", z)
		// 	return r
	}
	todo(n, n.Case)
	panic(internalErrorf("%v", n.Case))
}

func (q *qbe) complexInclusiveOrExpression(n *cc.InclusiveOrExpression) (int, int) {
	switch n.Case {
	case cc.InclusiveOrExpressionXor: // ExclusiveOrExpression
		return q.complexExclusiveOrExpression(n.ExclusiveOrExpression)
		// case cc.InclusiveOrExpressionOr: // InclusiveOrExpression '|' ExclusiveOrExpression
		// 	lhs := q.inclusiveOrExpression(n.InclusiveOrExpression)
		// 	lhs = q.convert(n, lhs, n.InclusiveOrExpression.Operand.Type(), n.Operand.Type())
		// 	rhs := q.exclusiveOrExpression(n.ExclusiveOrExpression)
		// 	rhs = q.convert(n, rhs, n.ExclusiveOrExpression.Operand.Type(), n.Operand.Type())
		// 	return q.t("or %%t%[2]d, %%t%d\t\t# %v:", q.typ(n, n.Operand.Type()), lhs, rhs, q.pos(&n.Token))
	}
	todo(n, n.Case)
	panic(internalErrorf("%v", n.Case))
}

func (q *qbe) complexExclusiveOrExpression(n *cc.ExclusiveOrExpression) (int, int) {
	switch n.Case {
	case cc.ExclusiveOrExpressionAnd: // AndExpression
		return q.complexAndExpression(n.AndExpression)
		// case cc.ExclusiveOrExpressionXor: // ExclusiveOrExpression '^' AndExpression
		// 	lhs := q.exclusiveOrExpression(n.ExclusiveOrExpression)
		// 	lhs = q.convert(n, lhs, n.ExclusiveOrExpression.Operand.Type(), n.Operand.Type())
		// 	rhs := q.andExpression(n.AndExpression)
		// 	rhs = q.convert(n, rhs, n.AndExpression.Operand.Type(), n.Operand.Type())
		// 	return q.t("xor %%t%[2]d, %%t%d\t\t# %v:", q.typ(n, n.Operand.Type()), lhs, rhs, q.pos(&n.Token))
	}
	todo(n, n.Case)
	panic(internalErrorf("%v", n.Case))
}

func (q *qbe) complexAndExpression(n *cc.AndExpression) (int, int) {
	switch n.Case {
	case cc.AndExpressionEq: // EqualityExpression
		return q.complexEqualityExpression(n.EqualityExpression)
		// case cc.AndExpressionAnd: // AndExpression '&' EqualityExpression
		// 	lhs := q.andExpression(n.AndExpression)
		// 	lhs = q.convert(n, lhs, n.AndExpression.Operand.Type(), n.Operand.Type())
		// 	rhs := q.equalityExpression(n.EqualityExpression)
		// 	rhs = q.convert(n, rhs, n.EqualityExpression.Operand.Type(), n.Operand.Type())
		// 	return q.t("and %%t%[2]d, %%t%d\t\t# %v:", q.typ(n, n.Operand.Type()), lhs, rhs, q.pos(&n.Token))
	}
	todo(n, n.Case)
	panic(internalErrorf("%v", n.Case))
}

func (q *qbe) isAlloca(n *cc.PostfixExpression) bool  { return q.is(n, "__gocc_alloca") }
func (q *qbe) isVaStart(n *cc.PostfixExpression) bool { return q.is(n, "__gocc_va_start") }
func (q *qbe) isVaEnd(n *cc.PostfixExpression) bool   { return q.is(n, "__gocc_va_end") }

func (q *qbe) is(n *cc.PostfixExpression, s string) bool {
	if n.Case != cc.PostfixExpressionCall {
		return false
	}

	n2 := n.PostfixExpression
	if n2.Case != cc.PostfixExpressionPrimary {
		return false
	}

	n3 := n2.PrimaryExpression
	if n3.Case != cc.PrimaryExpressionIdent {
		return false
	}

	return n3.Token.Value.String() == s

}

func (q *qbe) primaryExpression(n *cc.PrimaryExpression, void bool) int {
	switch n.Case {
	case cc.PrimaryExpressionIdent: // IDENTIFIER
		return q.declarator(n, n.Operand)
	case cc.PrimaryExpressionInt: // INTCONST
		return q.t("copy %[2]v\t\t# %v: %v", q.typ(n, n.Operand.Type()), n.Operand.Value(), q.pos(n), n.Operand.Type())
	case cc.PrimaryExpressionFloat: // FLOATCONST
		var v uint64
		switch x := n.Operand.Value().(type) {
		case cc.Float32Value:
			v = uint64(math.Float32bits(float32(x)))
		case cc.Float64Value:
			v = math.Float64bits(float64(x))
		case *cc.Float128Value:
			return q.float128(n, x, n.Operand.Type())
		default:
			todo(n, x)
		}
		return q.t("copy %[2]v\t\t# %v: %v", q.typ(n, n.Operand.Type()), v, q.pos(n), n.Operand.Value())
	case cc.PrimaryExpressionEnum: // ENUMCONST
		return q.t("copy %[2]v\t\t# %v:", q.typ(n, n.Operand.Type()), n.Operand.Value(), q.pos(n))
	case cc.PrimaryExpressionChar: // CHARCONST
		return q.t("copy %[2]v\t\t# %v:", q.typ(n, n.Operand.Type()), n.Operand.Value(), q.pos(n))
	case cc.PrimaryExpressionLChar: // LONGCHARCONST
		return q.t("copy %[2]v\t\t# %v:", q.typ(n, n.Operand.Type()), n.Operand.Value(), q.pos(n))
	case cc.PrimaryExpressionString: // STRINGLITERAL
		return q.t("copy $.%[2]d\t\t# %v: %s", q.ptr, q.string(n.Token.Value), q.pos(n), q.strComment(n.Token.Value))
	case cc.PrimaryExpressionLString: // LONGSTRINGLITERAL
		return q.t("copy $.%[2]d\t\t# %v: %s", q.ptr, q.string(-n.Token.Value), q.pos(n), q.strComment(n.Token.Value))
	case cc.PrimaryExpressionExpr: // '(' Expression ')'
		return q.expression(n.Expression, void)
	case cc.PrimaryExpressionStmt: // '(' CompoundStatement ')'
		return q.compoundStatementLeave(n.CompoundStatement, false)
	}
	todo(n, n.Case)
	panic("internal error") //TODOOK
}

func (q *qbe) float128(n cc.Node, x *cc.Float128Value, t cc.Type) int {
	switch {
	case x.NaN:
		todo(n)
	default:
		return q.t("copy ld_%[2]v\t\t# %v:", q.typ(n, t), x.N.Text('e', -1), q.pos(n))
	}

	panic("unreachable")
}

func (q *qbe) declarator(n cc.Node, op cc.Operand) int {
	d := op.Declarator()
	if op.Offset() != 0 {
		todo(n)
	}

	switch d.Linkage {
	case cc.None:
		if d.IsStatic() {
			switch kind(d.Type()) {
			case cc.Function:
				todo(n)
			case cc.Array, cc.Struct, cc.Union:
				return q.t("copy $.%[2]d\t\t# %v: %s", q.typ(n, d.Type()), q.localStatic[d], q.pos(d), d.Name())
			default:
				if q.isVolatile(d) {
					q.w("\t#qbe:volatile\n")
				}
				return q.t("load%s $.%d\t\t# %v: %s", q.typ(n, d.Type()), q.localStatic[d], q.pos(d), d.Name())
			}
		}

		switch kind(d.Type()) {
		case cc.Function:
			if q.escaped[d] {
				todo(n, d.Name())
			}

			return q.t("copy %%v%[2]d\t\t# %v: %s", q.typ(n, d.Type()), q.localVars[d], q.pos(d), d.Name())
		case cc.Array:
			if q.escaped[d] {
				return q.t("load%[2]s %%v%d\t\t# %v: %s", q.typ(n, d.Type()), q.loadType(d.Type()), q.localVars[d], q.pos(d), d.Name())
			}

			return q.t("copy %%v%[2]d\t\t# %v: %s", q.ptr, q.localVars[d], q.pos(d), d.Name())
		case cc.Struct, cc.Union:
			if q.escaped[d] {
				todo(n, d.Name())
			}

			return q.t("copy %%v%[2]d\t\t# %v: %s", q.ptr, q.localVars[d], q.pos(d), d.Name())
		default:
			if q.values[d] {
				return q.t("copy %%v%[2]d\t\t# %v: %s", q.typ(n, d.Type()), q.localVars[d], q.pos(d), d.Name())
			}

			if q.isVolatile(d) {
				q.w("\t#qbe:volatile\n")
			}
			return q.t("load%[2]s %%v%d\t\t# %v: %s", q.typ(n, d.Type()), q.loadType(d.Type()), q.localVars[d], q.pos(d), d.Name())
		}
	case cc.Internal, cc.External:
		switch kind(d.Type()) {
		case cc.Array, cc.Function, cc.Struct, cc.Union:
			return q.t("copy $%[2]q\t\t# %v:", q.ptr, d.Name(), q.pos(d))
		}

		if q.isVolatile(d) {
			q.w("\t#qbe:volatile\n")
		}
		return q.t("load%[2]s $%q", q.typ(n, d.Type()), q.loadType(d.Type()), d.Name())
	}
	todo(n, d.Linkage)
	panic("internal error") //TODOOK
}

func (q *qbe) strComment(id cc.StringID) string {
	const max = 32
	s := id.String()
	if len(s) < max {
		return fmt.Sprintf("%q", s)
	}

	return fmt.Sprintf("%q...", s[:max])
}

func (q *qbe) string(id cc.StringID) int {
	if r, ok := q.strings[id]; ok {
		return r
	}

	r := q.anon()
	q.strings[id] = r
	q.stringQueue = append(q.stringQueue, id)
	return r
}

func (q *qbe) anon() int { q.anons++; return q.anons }

func (q *qbe) unaryExpressionPtr(n *cc.UnaryExpression) int {
	switch n.Case {
	case cc.UnaryExpressionPostfix: // PostfixExpression
		return q.postfixExpressionPtr(n.PostfixExpression)
	case cc.UnaryExpressionDeref: // '*' CastExpression
		return q.castExpression(n.CastExpression, false) //TODO void
	}
	todo(n, n.Case)
	panic("internal error") //TODOOK
}

func (q *qbe) postfixExpressionPtr(n *cc.PostfixExpression) int {
	switch n.Case {
	case cc.PostfixExpressionPrimary: // PrimaryExpression
		return q.primaryExpressionPtr(n.PrimaryExpression)
	case cc.PostfixExpressionIndex: // PostfixExpression '[' Expression ']'
		switch {
		case n.Expression.Operand.Type().IsIntegerType():
			e := q.expression(n.Expression, false)
			e = q.convert(n, e, n.Expression.Operand.Type(), q.ptrType, nil)
			e = q.t("mul %%t%[2]d, %d\t\t# %v:", q.ptr, e, n.PostfixExpression.Operand.Type().Elem().Size(), q.pos(&n.Token))
			p := q.postfixExpression(n.PostfixExpression, false)
			return q.t("add %%t%[2]d, %%t%d", q.ptr, p, e)
		default:
			todo(n)
		}
	case cc.PostfixExpressionCall: // PostfixExpression '(' ArgumentExpressionList ')'
		switch kind(n.Operand.Type()) {
		case cc.Struct, cc.Union:
			return q.postfixExpression(n, false)
		default:
			todo(n)
		}
	case cc.PostfixExpressionSelect: // PostfixExpression '.' IDENTIFIER
		r := q.postfixExpressionPtr(n.PostfixExpression)
		return q.t("add %%t%[2]d, %d\t\t# %v: .%s", q.ptr, r, n.Field.Offset(), q.pos(&n.Token), n.Token2.Value)
	case cc.PostfixExpressionPSelect: // PostfixExpression "->" IDENTIFIER
		r := q.postfixExpression(n.PostfixExpression, false)
		return q.t("add %%t%[2]d, %d\t\t# %v: .%s", q.ptr, r, n.Field.Offset(), q.pos(&n.Token), n.Token2.Value)
	case cc.PostfixExpressionComplit: // '(' TypeName ')' '{' InitializerList ',' '}'
		t := n.TypeName.Type()
		if q.fn == nil {
			todo(n)
		}

		r := q.compositeLiterals[n]
		if n.InitializerList.IsConst() {
			a := q.anonymousInitializer(nil, t, n, n.InitializerList.List())
			q.w("\tcall $memcpy(%s %%t%d, %s $.%d, %s %d)\t\t# %v:\n", q.ptr, r, q.ptr, a, q.ptr, t.Size(), q.pos(n))
			return r
		}

		in := newComputedInitializer(q, n, q.compositeLiterals[n], t)
		q.initializer(in, n.InitializerList.List(), 0)
		return r
	}
	todo(n, n.Case)
	panic("internal error") //TODOOK
}

func (q *qbe) primaryExpressionPtr(n *cc.PrimaryExpression) int {
	switch n.Case {
	case cc.PrimaryExpressionIdent: // IDENTIFIER
		return q.declaratorPtr(n, n.Operand)
	case cc.PrimaryExpressionExpr: // '(' Expression ')'
		return q.expressionPtr(n.Expression)
	case cc.PrimaryExpressionStmt: // '(' CompoundStatement ')'
		later(n, "ptr of PrimaryExpressionStmt")
	}
	todo(n, n.Case)
	panic("internal error") //TODOOK
}

func (q *qbe) expressionPtr(n *cc.Expression) int {
	switch n.Case {
	case cc.ExpressionAssign: // AssignmentExpression
		return q.assignmentExpressionPtr(n.AssignmentExpression)
	}
	todo(n, n.Case)
	panic("internal error") //TODOOK
}

func (q *qbe) assignmentExpressionPtr(n *cc.AssignmentExpression) int {
	switch n.Case {
	case cc.AssignmentExpressionCond: // ConditionalExpression
		return q.conditionalExpressionPtr(n.ConditionalExpression)
	}
	todo(n, n.Case)
	panic("internal error") //TODOOK
}

func (q *qbe) conditionalExpressionPtr(n *cc.ConditionalExpression) int {
	switch n.Case {
	case cc.ConditionalExpressionLOr: // LogicalOrExpression
		return q.logicalOrExpressionPtr(n.LogicalOrExpression)
	}
	todo(n, n.Case)
	panic("internal error") //TODOOK
}

func (q *qbe) logicalOrExpressionPtr(n *cc.LogicalOrExpression) int {
	switch n.Case {
	case cc.LogicalOrExpressionLAnd: // LogicalAndExpression
		return q.logicalAndExpressionPtr(n.LogicalAndExpression)
	}
	todo(n, n.Case)
	panic("internal error") //TODOOK
}

func (q *qbe) logicalAndExpressionPtr(n *cc.LogicalAndExpression) int {
	switch n.Case {
	case cc.LogicalAndExpressionOr: // InclusiveOrExpression
		return q.inclusiveOrExpressionPtr(n.InclusiveOrExpression)
	}
	todo(n, n.Case)
	panic("internal error") //TODOOK
}

func (q *qbe) inclusiveOrExpressionPtr(n *cc.InclusiveOrExpression) int {
	switch n.Case {
	case cc.InclusiveOrExpressionXor: // ExclusiveOrExpression
		return q.exclusiveOrExpressionPtr(n.ExclusiveOrExpression)
	}
	todo(n, n.Case)
	panic("internal error") //TODOOK
}

func (q *qbe) exclusiveOrExpressionPtr(n *cc.ExclusiveOrExpression) int {
	switch n.Case {
	case cc.ExclusiveOrExpressionAnd: // AndExpression
		return q.andExpressionPtr(n.AndExpression)
	}
	todo(n, n.Case)
	panic("internal error") //TODOOK
}

func (q *qbe) andExpressionPtr(n *cc.AndExpression) int {
	switch n.Case {
	case cc.AndExpressionEq: // EqualityExpression
		return q.equalityExpressionPtr(n.EqualityExpression)
	}
	todo(n, n.Case)
	panic("internal error") //TODOOK
}

func (q *qbe) equalityExpressionPtr(n *cc.EqualityExpression) int {
	switch n.Case {
	case cc.EqualityExpressionRel: // RelationalExpression
		return q.relationalExpressionPtr(n.RelationalExpression)
	}
	todo(n, n.Case)
	panic("internal error") //TODOOK
}

func (q *qbe) relationalExpressionPtr(n *cc.RelationalExpression) int {
	switch n.Case {
	case cc.RelationalExpressionShift: // ShiftExpression
		return q.shiftExpressionPtr(n.ShiftExpression)
	}
	todo(n, n.Case)
	panic("internal error") //TODOOK
}

func (q *qbe) shiftExpressionPtr(n *cc.ShiftExpression) int {
	switch n.Case {
	case cc.ShiftExpressionAdd: // AdditiveExpression
		return q.additiveExpressionPtr(n.AdditiveExpression)
	}
	todo(n, n.Case)
	panic("internal error") //TODOOK
}

func (q *qbe) additiveExpressionPtr(n *cc.AdditiveExpression) int {
	switch n.Case {
	case cc.AdditiveExpressionMul: // MultiplicativeExpression
		return q.multiplicativeExpressionPtr(n.MultiplicativeExpression)
	}
	todo(n, n.Case)
	panic("internal error") //TODOOK
}

func (q *qbe) multiplicativeExpressionPtr(n *cc.MultiplicativeExpression) int {
	switch n.Case {
	case cc.MultiplicativeExpressionCast: // CastExpression
		return q.castExpressionPtr(n.CastExpression)
	}
	todo(n, n.Case)
	panic("internal error") //TODOOK
}

func (q *qbe) castExpressionPtr(n *cc.CastExpression) int {
	switch n.Case {
	case cc.CastExpressionUnary: // UnaryExpression
		return q.unaryExpressionPtr(n.UnaryExpression)
	}
	todo(n, n.Case)
	panic("internal error") //TODOOK
}

func (q *qbe) declaratorPtr(n cc.Node, op cc.Operand) int {
	return q.declaratorPtr2(n, op.Declarator(), op.Offset())
}

func (q *qbe) declaratorPtr2(n cc.Node, d *cc.Declarator, off uintptr) int {
	switch d.Linkage {
	case cc.None:
		if d.IsStatic() {
			return q.t("add $.%[2]d, %d\t\t# %v: %s", q.ptr, q.localStatic[d], off, q.pos(d), d.Name())
		}

		switch kind(d.Type()) {
		case cc.Function:
			if q.escaped[d] {
				todo(n, d.Name())
			}

			todo(n, d.Name())
		case cc.Array:
			if q.escaped[d] { //TODO- not special
				return q.t("add %%v%[2]d, %d\t\t# %v: %s", q.ptr, q.localVars[d], off, q.pos(d), d.Name())
			}

			return q.t("add %%v%[2]d, %d\t\t# %v: %s", q.ptr, q.localVars[d], off, q.pos(d), d.Name()) //TODO- not special
		case cc.Struct, cc.Union:
			if q.escaped[d] {
				todo(n, d.Name())
			}

			return q.t("add %%v%[2]d, %d\t\t# %v: %s", q.ptr, q.localVars[d], off, q.pos(d), d.Name())
		default:
			if q.values[d] {
				panic(fmt.Errorf("internal error\n%s", debug.Stack()))
			}

			return q.t("add %%v%[2]d, %d\t\t# %v: %s", q.ptr, q.localVars[d], off, q.pos(d), d.Name())
		}
	case cc.Internal:
		return q.t("add $%[2]q, %d\t\t# %v:", q.ptr, d.Name(), off, q.pos(d))
	case cc.External:
		return q.t("add $%[2]q, %d\t\t# %v:", q.ptr, d.Name(), off, q.pos(d))
	}
	todo(n, d.Linkage)
	panic("internal error") //TODOOK
}

func (q *qbe) temp() int {
	r := q.tempVars + 1
	q.tempVars++
	return r
}

// BASETY := 'w' | 'l' | 's' | 'd'  # Base types
func (q *qbe) typ(n cc.Node, t cc.Type) string {
	switch kind(t) {
	case cc.Float:
		return "s"
	case cc.Double:
		return "d"
	case cc.LongDouble:
		return "ld"
	case cc.Function, cc.Array, cc.Struct, cc.Union:
		return q.ptr
	}

	switch t.Size() {
	case 1, 2, 4:
		return "w"
	case 8:
		return "l"
	}
	todo(n, t, t.Kind(), t.Size())
	panic("internal error") //TODOOK
}

// EXTTY  := BASETY    | 'b' | 'h'  # Extended types
func (q *qbe) extType(t cc.Type) string {
	switch t.Kind() {
	case cc.Float:
		return "s"
	case cc.Double:
		return "d"
	case cc.LongDouble:
		return "ld"
	case cc.Function, cc.Array, cc.Struct, cc.Union:
		return q.ptr
	}

	switch t.Size() {
	case 1:
		return "b"
	case 2:
		return "h"
	case 4:
		return "w"
	case 8:
		return "l"
	}
	todo(nil, t, t.Size())
	panic("internal error") //TODOOK
}

// Type tags for the extend instructions.
//
//	extsw, extuw -- l(w)
//	extsh, extuh -- I(ww)
//	extsb, extub -- I(ww)
//	exts -- d(s)
func (q *qbe) extendType(t cc.Type) string {
	switch kind(t) {
	case cc.Float:
		todo(nil, t)
	case cc.Double:
		todo(nil, t)
	case cc.LongDouble:
		todo(nil, t)
	case cc.Function:
		todo(nil, t)
	case cc.Array:
		todo(nil, t)
	case cc.Struct, cc.Union:
		todo(nil, t)
	}

	switch t.Size() {
	case 1:
		if t.IsSignedType() {
			return "sb"
		}

		return "ub"
	case 2:
		if t.IsSignedType() {
			return "sh"
		}

		return "uh"
	case 4:
		if t.IsSignedType() {
			return "sw"
		}

		return "uw"
	}
	todo(nil, t, t.Size())
	panic("internal error") //TODOOK
}

// Type tags for memory loads
//
//	loadd -- d(m)
//	loads -- s(m)
//	loadl -- l(m)
//	loadsw, loaduw -- I(mm)
//	loadsh, loaduh -- I(mm)
//	loadsb, loadub -- I(mm)
func (q *qbe) loadType(t cc.Type) string {
	switch kind(t) {
	case cc.Float:
		return "s"
	case cc.Double:
		return "d"
	case cc.LongDouble:
		return "ld"
	case cc.Function, cc.Array, cc.Struct, cc.Union:
		return q.ptr
	}

	switch t.Size() {
	case 1:
		if t.IsSignedType() {
			return "sb"
		}

		return "ub"
	case 2:
		if t.IsSignedType() {
			return "sh"
		}

		return "uh"
	case 4:
		return "w"
	case 8:
		return "l"
	}
	todo(nil, t, t.Size())
	panic("internal error") //TODOOK
}

// ABITY := BASETY | :IDENT
func (q *qbe) abiType(n cc.Node, t cc.Type) string {
	switch kind(t) {
	case cc.Float:
		return "s"
	case cc.Double:
		return "d"
	case cc.LongDouble:
		return "ld"
	case cc.Function, cc.Array:
		return q.ptr
	case cc.Struct, cc.Union:
		ord := q.registerStruct(n, t)
		return fmt.Sprintf(":.%d", ord)
	}

	switch t.Size() {
	case 1, 2, 4:
		return "w"
	case 8:
		return "l"
	}
	later(n, t, t.Size())
	panic("internal error") //TODOOK
}

// DeclarationSpecifiers InitDeclaratorList ';'
func (q *qbe) declaration(n *cc.Declaration) {
	for n := n.InitDeclaratorList; n != nil; n = n.InitDeclaratorList {
		if n.AttributeSpecifierList != nil {
			todo(n)
		}
		q.initDeclarator(n.InitDeclarator, false)
	}
}

func (q *qbe) initDeclarator(n *cc.InitDeclarator, predeclare bool) {
	if n.AttributeSpecifierList != nil {
		todo(n)
	}

	d := n.Declarator
	t := d.Type()
	if t.Kind() == cc.Function && d.LexicalScope().Parent() == nil {
		nm := d.Name()
		defs := q.ast.Scope[nm]
		defined := false
		if len(defs) != 0 {
			_, defined = defs[0].(*cc.FunctionDefinition)
		}
		if !defined {
			if _, ok := q.prototypes[nm]; !ok {
				q.prototypes[nm] = struct{}{}
				q.prototype(n, d)
			}
		}
	}

	if d.Linkage != cc.None {
		if _, ok := q.ast.TLD[d]; !ok {
			return
		}
	}

	switch n.Case {
	case cc.InitDeclaratorDecl: // Declarator AttributeSpecifierList
		if t.Kind() == cc.Function { // prototype
			return
		}

		if d.IsTypedefName {
			return
		}

		if d.IsExtern() {
			return
		}

		q.initDeclaratorDeclare(n, d, t, predeclare)
	case cc.InitDeclaratorInit: // Declarator AttributeSpecifierList '=' Initializer
		switch d.Linkage {
		case cc.None:
			if d.IsStatic() {
				if q.localStatic[d] != 0 {
					return
				}

				if !n.Initializer.IsConst() {
					later(n, "const initializer determined not constant")
				}

				if q.escaped[d] {
					todo(n, d.Name())
				}

				q.anonymousInitializer(d, t, n, n.Initializer.List())
				return
			}

			if predeclare {
				q.initDeclaratorDeclare(n, d, t, predeclare)
				return
			}

			switch kind(d.Type()) {
			case cc.Array:
				if q.escaped[d] {
					todo(n, d.Name())
				}

				r := q.initDeclaratorDeclare(n, d, t, false)
				if n.Initializer.IsConst() {
					a := q.anonymousInitializer(d, t, n, n.Initializer.List())
					q.w("\tcall $memcpy(%s %s, %s $.%d, %s %d)\t\t# %v: %v %v\n", q.ptr, r, q.ptr, a, q.ptr, d.Type().Size(), q.pos(n), d.Name(), t)
					return
				}

				in := newComputedInitializer(q, n, q.declaratorPtr2(n, d, 0), t)
				q.initializer(in, n.Initializer.List(), 0)
				return
			case cc.Struct, cc.Union:
				if q.escaped[d] {
					todo(n, d.Name())
				}

				if n.Initializer.Case == cc.InitializerExpr {
					q.initDeclaratorDeclare(n, d, t, false)
					switch {
					case n.Initializer.AssignmentExpression.Operand.Type().IsComplexType():
						later(n, "complex initializer expression")
					default:
						e := q.assignmentExpression(n.Initializer.AssignmentExpression, false)
						p := q.declaratorPtr2(n, d, 0)
						q.w("\tcall $memcpy(%s %%t%d, %s %%t%d, %s %d)\t\t# %v:\n", q.ptr, p, q.ptr, e, q.ptr, d.Type().Size(), q.pos(n.Initializer.AssignmentExpression))
					}
					return
				}

				r := q.initDeclaratorDeclare(n, d, t, false)
				if n.Initializer.IsConst() {
					a := q.anonymousInitializer(d, t, n, n.Initializer.List())
					q.w("\tcall $memcpy(%s %s, %s $.%d, %s %d)\t\t# %v: %v %v\n", q.ptr, r, q.ptr, a, q.ptr, d.Type().Size(), q.pos(n), d.Name(), t)
					return
				}

				in := newComputedInitializer(q, n, q.declaratorPtr2(n, d, 0), t)
				q.initializer(in, n.Initializer.List(), 0)
				return
			}

			switch n.Initializer.Case {
			case cc.InitializerExpr: // AssignmentExpression
				q.initDeclaratorDeclare(n, d, t, false)
				switch {
				case n.Initializer.AssignmentExpression.Operand.Type().IsComplexType():
					later(n, "complex initializer expression")
				default:
					e := q.assignmentExpression(n.Initializer.AssignmentExpression, false)
					e = q.convert(n, e, n.Initializer.AssignmentExpression.Operand.Type(), t, nil)
					switch {
					case q.values[d]:
						q.w("\t%s =%s copy %%t%d\t\t# %v: %s %s\n", q.declName(n, d), q.typ(n, t), e, q.pos(d), d.Name(), t)
					default:
						q.w("\tstore%s %%t%d, %s\t\t# %v:\n", q.extType(t), e, q.declName(n, d), q.pos(&n.Token))
					}
				}
			default:
				todo(n, n.Initializer.Case)
			}
		case cc.Internal:
			if !n.Initializer.IsConst() {
				todo(n, "const initializer determined not constant")
			}

			in := newConstInitializer(q, t)
			q.initializer(in, n.Initializer.List(), 0)
			q.w("\ndata $%q = align %d {", d.Name(), d.Type().Align())
			in.emit(q)
			q.w("}\t\t# %v: %s, size %v\n", q.pos(d), d.Type(), d.Type().Size())
			q.flushStrings()
		case cc.External:
			if !n.Initializer.IsConst() {
				later(n, "const initializer determined not constant")
			}

			in := newConstInitializer(q, t)
			q.initializer(in, n.Initializer.List(), 0)
			q.w("\nexport data $%q = align %d {", d.Name(), d.Type().Align())
			in.emit(q)
			q.w("}\t\t# %v: %s, size %v\n", q.pos(d), d.Type(), d.Type().Size())
			q.flushStrings()
		default:
			todo(n, d.Linkage)
			panic("internal error") //TODOOK
		}
	default:
		panic("internal error") //TODOOK
	}
}

func (q *qbe) prototype(n cc.Node, d *cc.Declarator) {
	if d.Read == 0 {
		return
	}

	q.w("\n")
	if !d.IsStatic() {
		q.w("export ")
	}
	q.w("function ")
	t := d.Type()
	switch r := t.Result(); r.Kind() {
	case cc.Void:
		// nop
	default:
		q.w("%s ", q.abiType(d, r))
	}
	q.w("$%q(", "__qbe_prototype_"+d.Name().String())
	params := t.Parameters()
	if len(params) == 1 && params[0].Type().Kind() == cc.Void {
		params = nil
	}
	for i, v := range params {
		if v.Type().Kind() == cc.Array && v.Type().IsVLA() {
			later(d, "VLA param")
		}

		q.w("%s ", q.abiType(v.Declarator(), v.Type()))
		q.w("%%v%d", i)
		if i < len(params)-1 {
			q.w(", ")
		}
	}
	if t.IsVariadic() {
		q.w(", ...")
	}
	q.w(") { @.1 @.2 call $abort() jmp @.2}")
	q.w("\n")
}

func (q *qbe) anonymousInitializer(localStatic *cc.Declarator, t cc.Type, n cc.Node, list []*cc.Initializer) int {
	a := q.anon()
	if localStatic != nil {
		q.localStatic[localStatic] = a
	}
	in := newConstInitializer(q, t)
	q.initializer(in, list, 0)
	q.initializerQueue = append(q.initializerQueue, queuedInitializer{
		name:        a,
		typ:         t,
		initializer: in,
		comment:     fmt.Sprintf("\t\t# %v: %s", q.pos(n), t),
		d:           localStatic,
	})
	return a
}

func (q *qbe) initDeclaratorDeclare(n *cc.InitDeclarator, d *cc.Declarator, t cc.Type, predeclare bool) (r string) { //TODO- result
	switch d.Linkage {
	case cc.None:
		if d.IsStatic() {
			a := q.anon()
			q.localStatic[d] = a
			q.initializerQueue = append(q.initializerQueue, queuedInitializer{
				name:    a,
				typ:     t,
				comment: fmt.Sprintf("\t\t# %v: %s %s", q.pos(d), d.Name(), t),
				d:       d,
			})
			return fmt.Sprintf("$.%d", a)
		}

		vla := t.Kind() == cc.Array && t.IsVLA()
		if !vla && !predeclare {
			if i := q.localVars[d]; i != 0 {
				return fmt.Sprintf("%%v%d", i)
			}
		}

		ord := len(q.localVars) + 1
		switch {
		case vla:
			ord = q.vlas[d]
		default:
			q.localVars[d] = ord
		}
		r = fmt.Sprintf("%%v%d", ord)
		switch t.Kind() {
		case cc.Array:
			if t.IsVLA() {
				q.scopeVLAs = append(q.scopeVLAs, d)
				p := q.t("load%[2]s %%v%d", q.ptr, q.loadType(q.ptrType), q.vlas[d])
				// jnz p, @b, @a
				// @a:
				// alloc
				// @b:
				a := q.label()
				b := q.label()
				q.w("\tjnz %%t%d, @.%d, @.%d\n", p, b, a)
				q.w("@.%d\n", a)
				expr := t.LenExpr()
				e := q.assignmentExpression(expr, false)
				e = q.convert(d, e, expr.Operand.Type(), q.uintptr, nil)
				e = q.t("mul %%t%[2]d, %d\t\t# %v:", q.typ(n, q.uintptr), e, t.Elem().Size(), q.pos(d))
				p = q.t("call $malloc(%[2]s %%t%d)", q.ptr, q.abiType(nil, q.uintptr), e)
				q.w("\tstore%s %%t%d, %%v%d\n", q.loadType(q.ptrType), p, q.vlas[d])
				q.w("@.%d\n", b)
				break
			}

			fallthrough
		default:
			if q.canValue(d) {
				q.values[d] = true
				q.w("\t%s =%s copy 0\t\t# %v: %s %s\n", r, q.typ(n, t), q.pos(d), d.Name(), t)
				break
			}

			if q.opts.goccAllocatedDeclarators {
				q.reportAllocatedDeclarator(d)
			}
			switch t.Align() {
			case 1, 2, 4:
				q.w("\t%s =%s alloc4 %d\t\t# %v: %s %s\n", r, q.ptr, t.Size(), q.pos(d), d.Name(), t)
			case 8:
				q.w("\t%s =%s alloc8 %d\t\t# %v: %s %s\n", r, q.ptr, t.Size(), q.pos(d), d.Name(), t)
			case 16:
				q.w("\t%s =%s alloc16 %d\t\t# %v: %s %s\n", r, q.ptr, t.Size(), q.pos(d), d.Name(), t)
			default:
				todo(n, t, t.Size(), t.Align(), t.IsIncomplete())
			}
		}
	case cc.Internal:
		r = fmt.Sprintf("$%q", d.Name())
		q.w("data %s = align %d { z %d }\t\t# %v: %s\n", r, t.Align(), t.Size(), q.pos(d), t)
	case cc.External:
		r = fmt.Sprintf("$%q", d.Name())
		q.w("\nexport data %s = align %d { z %d }\t\t# %v: %s\n", r, t.Align(), t.Size(), q.pos(d), t)
	default:
		panic("internal error") //TODOOK
	}
	return r
}

func (q *qbe) pos(n cc.Node) string {
	if n == nil {
		return ""
	}

	p := n.Position()
	if p.Filename != "" {
		p.Filename = filepath.Base(p.Filename)
	}
	return p.String()
}

func (q *qbe) canValue(d *cc.Declarator) bool {
	t := d.Type()
	k := t.Kind()
	if !t.IsIntegerType() {
		switch k {
		case cc.Ptr, cc.Float, cc.Double:
			// ok
		case cc.Array:
			if !d.IsParameter {
				return false
			}
		default:
			return false
		}
	}

	return !d.AddressTaken && !t.IsBitFieldType()
}
